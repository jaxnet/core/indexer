package biggest_first

import (
	"gitlab.com/jaxnet/core/indexer/clients/utxos/abstract"
)

type UTXOProvider struct {
	*abstract.APIClient
}

func New(hostAndPort string, useHTTPS bool, sigFunc abstract.SignatureFunc) (c *UTXOProvider, err error) {
	client, err := abstract.NewAPIClient(hostAndPort, abstract.StrategyBiggestFirst, useHTTPS, sigFunc)
	if err != nil {
		return
	}

	c = &UTXOProvider{
		APIClient: client,
	}

	return
}
