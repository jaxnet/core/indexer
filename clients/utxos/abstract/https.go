package abstract

import "fmt"

type APIClient struct {
	*UTXOClient
}

func NewAPIClient(hostAndPort string, strategy string, useHTTPS bool, sigFunc SignatureFunc) (c *APIClient, err error) {
	var baseURL string
	if useHTTPS {
		baseURL = fmt.Sprint("https://", hostAndPort, "/api/v1/indexes/utxos/non-spent/")
	} else {
		baseURL = fmt.Sprint("http://", hostAndPort, "/api/v1/indexes/utxos/non-spent/")
	}

	client, err := NewAbstractUTXOClient(hostAndPort, baseURL, strategy, sigFunc)
	if err != nil {
		return
	}

	c = &APIClient{
		UTXOClient: client,
	}

	return
}
