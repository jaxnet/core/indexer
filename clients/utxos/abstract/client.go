package abstract

import (
	"encoding/json"
	"fmt"
	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/core/indexer/clients/utxos/ec"
	"gitlab.com/jaxnet/core/indexer/indexes/utxo/core/database"
	"gitlab.com/jaxnet/core/indexer/indexes/utxo/core/interfaces"
	"gitlab.com/jaxnet/jaxnetd/jaxutil/txmodels"
)

const (
	StrategySmallestFirst = "smallest-first"
	StrategyBiggestFirst  = "biggest-first"
)

type SignatureFunc func(address string) (pubKey, signature string, err error)

type UTXOClient struct {
	client        *fasthttp.HostClient
	baseURL       string
	strategy      string
	signatureFunc SignatureFunc
}

func NewAbstractUTXOClient(hostAndPort string, baseURL string, strategy string, sigFunc SignatureFunc) (c *UTXOClient, err error) {
	if strategy != StrategySmallestFirst && strategy != StrategyBiggestFirst {
		err = ec.ErrInvalidStrategy
		return
	}

	c = &UTXOClient{
		strategy: strategy,
		baseURL:  baseURL,
		client: &fasthttp.HostClient{
			Addr:                          hostAndPort,
			DisableHeaderNamesNormalizing: true,
			DisablePathNormalizing:        true,
		},
		signatureFunc: sigFunc,
	}

	return
}

// SelectForAmount returns all found UTXOs for required `amount`, `shardID` and list of addresses.
func (c *UTXOClient) SelectForAmount(amount int64, shardID uint32, addresses ...string) (utxos txmodels.UTXORows, err error) {
	if len(addresses) == 0 {
		err = ec.ErrInvalidAddressList
		return
	}

	var fetchedUTXOs txmodels.UTXORows
	for _, address := range addresses {
		fetchedUTXOs, err = c.fetchUTXOsForAddress(amount, shardID, address, true)
		if err != nil {
			return
		}

		utxos = append(utxos, fetchedUTXOs...)
	}

	return
}

// SelectForAmount returns all found UTXOs for required `amount`, `shardID` and list of addresses.
func (c *UTXOClient) SelectForAmountWithoutLock(amount int64, shardID uint32, addresses ...string) (utxos txmodels.UTXORows, err error) {
	if len(addresses) == 0 {
		err = ec.ErrInvalidAddressList
		return
	}

	var fetchedUTXOs txmodels.UTXORows
	for _, address := range addresses {
		fetchedUTXOs, err = c.fetchUTXOsForAddress(amount, shardID, address, false)
		if err != nil {
			return
		}

		utxos = append(utxos, fetchedUTXOs...)
	}

	return
}

// GetForAmount returns at most one UTXO for required `amount`, `shardID` and list of addresses.
// The list of UTXOs returned is sorted from the smallest one to the biggest one.
func (c *UTXOClient) GetForAmount(amount int64, shardID uint32, addresses ...string) (utxos *txmodels.UTXO, err error) {
	if len(addresses) == 0 {
		err = ec.ErrInvalidAddressList
		return
	}

	var fetchedUTXOs txmodels.UTXORows
	for _, address := range addresses {
		fetchedUTXOs, err = c.fetchUTXOsForAddress(amount, shardID, address, true)
		if err != nil {
			return
		}

		if len(fetchedUTXOs) > 0 {
			return &fetchedUTXOs[0], nil
		}
	}

	return
}

func (c *UTXOClient) NonSpentUTXOsCnt(address string) (count uint64, err error) {
	query := fmt.Sprint(c.baseURL, "count/", "?address=", address)

	var (
		statusCode int
		body       []byte
	)
	statusCode, body, err = c.client.Get(nil, query)

	if statusCode != fasthttp.StatusOK {
		err = fmt.Errorf("%s, %w", err, ec.ErrInvalidIndexResponse)
		return
	}
	if err != nil {
		return
	}

	var receivedUTXOsCnt interfaces.CountResponse

	err = json.Unmarshal(body, &receivedUTXOsCnt)
	if err != nil {
		return
	}

	count = receivedUTXOsCnt.Count
	return
}

func (c *UTXOClient) fetchUTXOsForAddress(amount int64, shardID uint32, address string, useLock bool) (
	utxos txmodels.UTXORows, err error) {
	// Note: no need to check arguments validity.
	// This validation is delegated to the API itself.
	// In worst case - error would be returned.

	query := fmt.Sprint(c.baseURL, "?strategy=", c.strategy, "&address=", address, "&amount=", amount)

	if useLock {
		pubKeyStr, sigStr, err := c.signatureFunc(address)
		if err != nil {
			return nil, err
		}

		query = fmt.Sprint(query, "&pub_key=", pubKeyStr, "&signature=", sigStr)
	}

	var (
		statusCode int
		body       []byte
	)
	statusCode, body, err = c.client.Get(nil, query)

	if statusCode != fasthttp.StatusOK {
		err = fmt.Errorf("%s, %w", err, ec.ErrInvalidIndexResponse)
		return
	}
	if err != nil {
		return
	}

	var receivedUTXOs []database.UTXO
	err = json.Unmarshal(body, &receivedUTXOs)
	if err != nil {
		return
	}

	for _, receivedUTXO := range receivedUTXOs {
		transformedUTXO := txmodels.UTXO{
			ShardID:    shardID,
			Address:    address,
			TxHash:     receivedUTXO.TxHash,
			OutIndex:   uint32(receivedUTXO.TxOutput),
			Value:      receivedUTXO.Amount,
			Used:       false,
			PKScript:   receivedUTXO.PKScript,
			ScriptType: "", //"pubkeyhash",
		}

		utxos = append(utxos, transformedUTXO)
	}

	return
}
