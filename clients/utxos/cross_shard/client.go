package cross_shard

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/core/indexer/clients/utxos/ec"
	"gitlab.com/jaxnet/core/indexer/indexes/utxo/core/database"
	"gitlab.com/jaxnet/jaxnetd/jaxutil/txmodels"
)

type SignatureFunc func(address string) (pubKey, signature string, err error)

type UTXOClient struct {
	clients       map[uint32]*fasthttp.HostClient
	useHTTPS      bool
	signatureFunc SignatureFunc
}

func New(hostsAndPorts map[uint32]string, useHTTPS bool, sigFunc SignatureFunc) (c *UTXOClient, err error) {

	hostClients := make(map[uint32]*fasthttp.HostClient)
	for shardID, hostAndPort := range hostsAndPorts {
		hostClients[shardID] = &fasthttp.HostClient{
			Addr:                          hostAndPort,
			DisableHeaderNamesNormalizing: true,
			DisablePathNormalizing:        true,
		}
	}
	c = &UTXOClient{
		clients:       hostClients,
		useHTTPS:      useHTTPS,
		signatureFunc: sigFunc,
	}

	return
}

// SelectForAmount returns all found UTXOs for required `amount`, `shardID` and list of addresses.
// The list of UTXOs returned is sorted from the smallest one to the biggest one.
func (c *UTXOClient) SelectForAmount(amount int64, shardID uint32, addresses ...string) (utxos txmodels.UTXORows, err error) {
	if len(addresses) == 0 {
		err = ec.ErrInvalidAddressList
		return
	}

	var fetchedUTXOs txmodels.UTXORows
	for _, address := range addresses {
		fetchedUTXOs, err = c.fetchUTXOsForAddress(amount, shardID, address)
		if err != nil {
			return
		}

		utxos = append(utxos, fetchedUTXOs...)
	}

	return
}

// GetForAmount returns at most one UTXO for required `amount`, `shardID` and list of addresses.
// The list of UTXOs returned is sorted from the smallest one to the biggest one.
func (c *UTXOClient) GetForAmount(amount int64, shardID uint32, addresses ...string) (utxos *txmodels.UTXO, err error) {
	if len(addresses) == 0 {
		err = ec.ErrInvalidAddressList
		return
	}

	var fetchedUTXOs txmodels.UTXORows
	for _, address := range addresses {
		fetchedUTXOs, err = c.fetchUTXOsForAddress(amount, shardID, address)
		if err != nil {
			return
		}

		if len(fetchedUTXOs) > 0 {
			return &fetchedUTXOs[0], nil
		}
	}

	return
}

func (c *UTXOClient) GetByHashes(shardID uint32, address string, hashes ...string) (utxos txmodels.UTXORows, err error) {
	if len(hashes) == 0 {
		err = ec.ErrInvalidHashesList
		return
	}

	client, ok := c.clients[shardID]
	if !ok {
		err = errors.New("Not supported shard ID")
		return
	}

	var url string
	if c.useHTTPS {
		url = fmt.Sprint("https://", client.Addr, "/api/v1/indexes/utxos/non-spent/by-hash/")
	} else {
		url = fmt.Sprint("http://", client.Addr, "/api/v1/indexes/utxos/non-spent/by-hash/")
	}

	query := fmt.Sprint(url, "?address=", address)
	for _, hash := range hashes {
		query = fmt.Sprint(query, "&tx_hash=", hash)
	}

	var (
		statusCode int
		body       []byte
	)

	statusCode, body, err = client.Get(nil, query)

	if statusCode != fasthttp.StatusOK {
		err = fmt.Errorf("%s, %w", err, ec.ErrInvalidIndexResponse)
		return
	}
	if err != nil {
		return
	}

	var receivedUTXOs []database.UTXO
	err = json.Unmarshal(body, &receivedUTXOs)
	if err != nil {
		return
	}

	for _, receivedUTXO := range receivedUTXOs {
		transformedUTXO := txmodels.UTXO{
			ShardID:    shardID,
			Address:    address,
			TxHash:     receivedUTXO.TxHash,
			OutIndex:   uint32(receivedUTXO.TxOutput),
			Value:      receivedUTXO.Amount,
			Used:       false,
			PKScript:   receivedUTXO.PKScript,
			ScriptType: "", //"pubkeyhash",
		}

		utxos = append(utxos, transformedUTXO)
	}

	return
}

func (c *UTXOClient) fetchUTXOsForAddress(amount int64, shardID uint32, address string) (utxos txmodels.UTXORows, err error) {
	// Note: no need to check arguments validity.
	// This validation is delegated to the API itself.
	// In worst case - error would be returned.

	pubKeyStr, sigStr, err := c.signatureFunc(address)
	if err != nil {
		return
	}

	client, ok := c.clients[shardID]
	if !ok {
		err = errors.New("Not supported shard ID")
		return
	}

	var url string
	if c.useHTTPS {
		url = fmt.Sprint("https://", client.Addr, "/api/v1/indexes/utxos/non-spent/one/")
	} else {
		url = fmt.Sprint("http://", client.Addr, "/api/v1/indexes/utxos/non-spent/one/")
	}

	query := fmt.Sprint(url, "?address=", address, "&amount=", amount, "&pub_key=", pubKeyStr, "&signature=", sigStr)

	var (
		statusCode int
		body       []byte
	)

	statusCode, body, err = client.Get(nil, query)

	if statusCode != fasthttp.StatusOK {
		err = fmt.Errorf("%s, %w", err, ec.ErrInvalidIndexResponse)
		return
	}
	if err != nil {
		return
	}

	var receivedUTXOs []database.UTXO
	err = json.Unmarshal(body, &receivedUTXOs)
	if err != nil {
		return
	}

	for _, receivedUTXO := range receivedUTXOs {
		transformedUTXO := txmodels.UTXO{
			ShardID:    shardID,
			Address:    address,
			TxHash:     receivedUTXO.TxHash,
			OutIndex:   uint32(receivedUTXO.TxOutput),
			Value:      receivedUTXO.Amount,
			Used:       false,
			PKScript:   receivedUTXO.PKScript,
			ScriptType: "", //"pubkeyhash",
		}

		utxos = append(utxos, transformedUTXO)
	}

	return
}
