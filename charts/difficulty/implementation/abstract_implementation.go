package implementation

import (
	"fmt"
	"github.com/fasthttp/router"
	"gitlab.com/jaxnet/core/indexer/charts/difficulty/core/interfaces"
	"gitlab.com/jaxnet/core/indexer/core/clickhouse"
	"gitlab.com/jaxnet/core/indexer/core/settings"
	"time"
)

// SharDifficultyChart once per block fetches current hash rate of the shard
// and records it in Click House in daily, hourly and blockly formats.
type SharDifficultyChart struct{}

func (i SharDifficultyChart) SetHttpHandlers(group *router.Group) {
	interfaces.InitAPIHandlers(group)
}

func (i SharDifficultyChart) DefaultFirstBlockForProcessing() (blockNumber int64) {
	return 0
}

func (i SharDifficultyChart) PrepareDatabase() error {
	return nil
}

func (i SharDifficultyChart) writeHashRateInDifferentResolutions(difficulty uint32, t time.Time) (err error) {
	err = i.writeDailyResolution(difficulty, t)
	if err != nil {
		return
	}

	err = i.writeHourlyResolution(difficulty, t)
	if err != nil {
		return
	}

	err = i.writeBlocklyResolution(difficulty, t)
	return
}

func (i SharDifficultyChart) writeDailyResolution(difficulty uint32, t time.Time) (err error) {
	query := fmt.Sprintf("INSERT INTO difficulty_daily__buffer "+
		"SELECT %d AS shardID, toDateTime('%s') AS timeStamp, maxState(toUInt32(%d)) AS maxDifficulty;",
		settings.Conf.Blockchain.ID, clickhouse.DropTZInfo(dailyResolution(t).UTC()), difficulty)

	return execQuery(query)
}

func (i SharDifficultyChart) writeHourlyResolution(difficulty uint32, t time.Time) (err error) {
	query := fmt.Sprintf("INSERT INTO difficulty_hourly__buffer "+
		"SELECT %d AS shardID, toDateTime('%s') AS timeStamp, maxState(toUInt32(%d)) AS maxDifficulty;",
		settings.Conf.Blockchain.ID, clickhouse.DropTZInfo(hourlyResolution(t).UTC()), difficulty)

	return execQuery(query)
}

func (i SharDifficultyChart) writeBlocklyResolution(difficulty uint32, t time.Time) (err error) {
	query := fmt.Sprintf("INSERT INTO difficulty_blockly__buffer "+
		"SELECT %d AS shardID, toDateTime('%s') AS timeStamp, maxState(toUInt32(%d)) AS maxDifficulty;",
		settings.Conf.Blockchain.ID, clickhouse.DropTZInfo(t.UTC()), difficulty)

	return execQuery(query)
}

func hourlyResolution(t time.Time) time.Time {
	return time.Date(t.Year(), t.Month(), t.Day(), t.Hour(), 0, 0, 0, t.Location())
}

func dailyResolution(t time.Time) time.Time {
	return time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, t.Location())
}

func execQuery(query string) (err error) {
	tx, err := clickhouse.BeginOperation()
	if err != nil {
		return
	}

	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return
	}

	err = tx.Commit()
	return
}
