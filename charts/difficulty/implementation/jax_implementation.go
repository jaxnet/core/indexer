package implementation

import (
	"github.com/jackc/pgx/v4"
	"gitlab.com/jaxnet/core/indexer/core"
	"gitlab.com/jaxnet/core/indexer/core/blocks_cache"
)

type JaxSharDifficultyChart struct {
	SharDifficultyChart
}

func (i JaxSharDifficultyChart) InitRPCClient() (*core.AbstractRPCClient, error) {
	return core.InitJaxRPCClient()
}

func (i JaxSharDifficultyChart) FetchNextBlockFromNode(offset int64, checkPrevBlock bool,
	rpcClient *core.AbstractRPCClient, cacheHandler *blocks_cache.CacheHandler) (
	block *core.AbstractMsgBlock, prevBlockNumber, blockHeight int64, rebuildingChainBlockNumber int64, err error) {
	return core.FetchNextBlockFromJaxNode(offset, checkPrevBlock, rpcClient, cacheHandler)
}

func (i JaxSharDifficultyChart) ConnectBlock(tx pgx.Tx, block *core.AbstractMsgBlock, _ int64, _ *core.AbstractRPCClient) error {
	// WARN: `tx` is a PostgreSQL's transaction, that can not be used in this context.
	// 		 As a result - there is no atomicity here!
	// 		 In case of any error on upper levels - the write operation into the clickhouse would not be rolled back,
	//		 but it should be OK for most cases (charts should not be 100% correct).
	jaxBlock, err := block.JaxMsgBlock()
	if err != nil {
		return err
	}
	return i.writeHashRateInDifferentResolutions(jaxBlock.Header.Bits(), jaxBlock.Header.Timestamp())
}

func (i JaxSharDifficultyChart) DisconnectBlock(tx pgx.Tx, _ *core.AbstractMsgBlock, _ int64, _ *core.AbstractRPCClient) error {
	// No need to discard hash rate from the accumulated stats.
	// Even if block is marked as orphan - it's hash rate still should be calculated in a global total.
	return nil
}
