package interfaces

import (
	"errors"
	"fmt"
	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/core/indexer/core/clickhouse"
	"gitlab.com/jaxnet/core/indexer/core/interfaces/http"
	"gitlab.com/jaxnet/core/indexer/core/settings"
	"gitlab.com/jaxnet/core/indexer/core/utils"
	"time"
)

const maxPointsPerRequest = 512

type DifficultyPoint struct {
	Timestamp  time.Time `json:"timestamp"`
	Difficulty uint32    `json:"difficulty"`
}

type DifficultyResponse struct {
	Data []DifficultyPoint `json:"data"`
}

func handleGetDifficultiesRequest(ctx *fasthttp.RequestCtx) {
	var (
		err     error
		userErr error

		from       = utils.YesterdayBegin()
		to         = utils.TodayEnd()
		resolution = "blockly"
		response   = DifficultyResponse{}
	)
	defer http.EstablishHandlerWideErrorsProcessing(&err, ctx)
	defer http.EstablishUserVisibleErrorsProcessing(&userErr, ctx)

	argFrom := string(ctx.QueryArgs().Peek("from"))
	if argFrom != "" {
		from, userErr = time.Parse(time.RFC3339, argFrom)
		if userErr != nil {
			return
		}
	}

	argTo := string(ctx.QueryArgs().Peek("to"))
	if argTo != "" {
		to, userErr = time.Parse(time.RFC3339, argTo)
		if userErr != nil {
			return
		}
	}

	argResolution := string(ctx.QueryArgs().Peek("resolution"))
	if argResolution != "" {
		resolution = argResolution
	}

	response.Data, err, userErr = fetchMetrics(from, to, resolution)
	if err != nil || userErr != nil {
		return
	}

	err = http.WriteJSONResponse(response, ctx)
}

func fetchMetrics(from, to time.Time, resolution string) (metrics []DifficultyPoint, err error, userErr error) {
	switch resolution {
	case "daily":
		metrics, err = fetchDailyMetrics(from, to)
		return

	case "hourly":
		metrics, err = fetchHourlyMetrics(from, to)
		return

	case "blockly":
		metrics, err = fetchBlocklyMetrics(from, to)
		return

	default:
		userErr = errors.New("unexpected resolution, available options are: daily, hourly, blockly")
		return
	}
}

func fetchDailyMetrics(from, to time.Time) (metrics []DifficultyPoint, err error) {
	query := fmt.Sprintf(
		"SELECT timeStamp, maxMerge(maxDifficulty) "+
			"FROM difficulty_daily__buffer "+
			"WHERE shardID = %d AND timeStamp >= toDateTime('%s') AND timeStamp < toDateTime('%s') "+
			"GROUP BY timeStamp "+
			"ORDER BY timeStamp ASC "+
			"LIMIT %d",
		settings.Conf.Blockchain.ID,
		clickhouse.DropTZInfo(from.UTC()),
		clickhouse.DropTZInfo(to.UTC()),
		maxPointsPerRequest)

	return fetch(query)
}

func fetchHourlyMetrics(from, to time.Time) (metrics []DifficultyPoint, err error) {
	query := fmt.Sprintf(
		"SELECT timeStamp, maxMerge(maxDifficulty) "+
			"FROM difficulty_hourly__buffer "+
			"WHERE shardID = %d AND timeStamp >= toDateTime('%s') AND timeStamp < toDateTime('%s') "+
			"GROUP BY timeStamp "+
			"ORDER BY timeStamp ASC "+
			"LIMIT %d",
		settings.Conf.Blockchain.ID,
		clickhouse.DropTZInfo(from.UTC()),
		clickhouse.DropTZInfo(to.UTC()),
		maxPointsPerRequest)

	return fetch(query)
}

func fetchBlocklyMetrics(from, to time.Time) (metrics []DifficultyPoint, err error) {
	query := fmt.Sprintf(
		"SELECT timeStamp, maxMerge(maxDifficulty) "+
			"FROM difficulty_blockly__buffer "+
			"WHERE shardID = %d AND timeStamp >= toDateTime('%s') AND timeStamp < toDateTime('%s') "+
			"GROUP BY timeStamp "+
			"ORDER BY timeStamp ASC "+
			"LIMIT %d",
		settings.Conf.Blockchain.ID,
		clickhouse.DropTZInfo(from.UTC()),
		clickhouse.DropTZInfo(to.UTC()),
		maxPointsPerRequest)

	return fetch(query)
}

func fetch(query string) (metrics []DifficultyPoint, err error) {
	tx, err := clickhouse.BeginOperation()
	if err != nil {
		return
	}

	rows, err := tx.Query(query)
	if err != nil {
		return
	}

	point := DifficultyPoint{}
	metrics = make([]DifficultyPoint, 0, maxPointsPerRequest)
	for rows.Next() {
		err := rows.Scan(&point.Timestamp, &point.Difficulty)
		if err != nil {
			return nil, err
		}

		metrics = append(metrics, point)
	}

	return
}
