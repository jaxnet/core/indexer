package interfaces

import (
	"github.com/fasthttp/router"
)

func InitAPIHandlers(root *router.Group) {
	root.GET("difficulty/", handleGetDifficultiesRequest)
}
