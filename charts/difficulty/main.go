package main

import (
	"gitlab.com/jaxnet/core/indexer/charts/difficulty/implementation"
	"gitlab.com/jaxnet/core/indexer/core/fake"
)

func main() {
	fake.Main(&implementation.JaxSharDifficultyChart{}, &implementation.BtcSharDifficultyChart{})
}
