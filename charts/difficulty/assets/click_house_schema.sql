CREATE TABLE IF NOT EXISTS {{ index_data_table }}_daily
(
    shardID UInt32,
    timeStamp DateTime,
    maxDifficulty AggregateFunction(max, UInt32)
) ENGINE = AggregatingMergeTree()
    PRIMARY KEY timeStamp
    PARTITION BY shardID
    ORDER BY timeStamp;

-- SEPARATOR

CREATE TABLE IF NOT EXISTS {{ index_data_table }}_daily__buffer AS {{ index_data_table }}_daily
    ENGINE = Buffer('', {{ index_data_table }}_daily, 4, 15, 150, 1, 40, 1, 100000000);

-- SEPARATOR

CREATE TABLE IF NOT EXISTS {{ index_data_table }}_hourly
(
    shardID UInt32,
    timeStamp DateTime,
    maxDifficulty AggregateFunction(max, UInt32)
) ENGINE = AggregatingMergeTree()
    PRIMARY KEY timeStamp
    PARTITION BY shardID
    ORDER BY timeStamp;

-- SEPARATOR

CREATE TABLE IF NOT EXISTS {{ index_data_table }}_hourly__buffer AS {{ index_data_table }}_hourly
    ENGINE = Buffer('', {{ index_data_table }}_hourly, 4, 15, 150, 1, 200, 1, 100000000);

-- SEPARATOR

CREATE TABLE IF NOT EXISTS {{ index_data_table }}_blockly
(
    shardID UInt32,
    timeStamp DateTime,
    maxDifficulty AggregateFunction(max, UInt32)
) ENGINE = AggregatingMergeTree()
    PRIMARY KEY timeStamp
    PARTITION BY shardID
    ORDER BY timeStamp;

-- SEPARATOR

CREATE TABLE IF NOT EXISTS {{ index_data_table }}_blockly__buffer AS {{ index_data_table }}_blockly
    ENGINE = Buffer('', {{ index_data_table }}_blockly, 4, 15, 150, 1, 1000, 1, 100000000);