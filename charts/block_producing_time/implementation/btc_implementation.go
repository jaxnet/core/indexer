package implementation

import (
	"github.com/jackc/pgx/v4"
	"gitlab.com/jaxnet/core/indexer/core"
	"gitlab.com/jaxnet/core/indexer/core/blocks_cache"
	"gitlab.com/jaxnet/core/indexer/core/logger"
)

type BtcBlockProducingTimeChart struct {
	BlockProducingTimeChart
}

func (i BtcBlockProducingTimeChart) InitRPCClient() (*core.AbstractRPCClient, error) {
	return core.InitBTCRPCClient()
}

func (i BtcBlockProducingTimeChart) FetchNextBlockFromNode(offset int64, checkPrevBlock bool,
	rpcClient *core.AbstractRPCClient, cacheHandler *blocks_cache.CacheHandler) (
	block *core.AbstractMsgBlock, prevBlockNumber, blockHeight int64, rebuildingChainBlockNumber int64, err error) {
	return core.FetchNextBlockFromBtcNode(offset, checkPrevBlock, rpcClient, cacheHandler)
}

func (i BtcBlockProducingTimeChart) ConnectBlock(tx pgx.Tx, block *core.AbstractMsgBlock, _ int64, rpcClient *core.AbstractRPCClient) error {
	// WARN: `tx` is a PostgreSQL's transaction, that can not be used in this context.
	// 		 As a result - there is no atomicity here!
	// 		 In case of any error on upper levels - the write operation into the clickhouse would not be rolled back,
	//		 but it should be OK for most cases (charts should not be 100% correct).
	btcBlock, err := block.BTCMsgBlock()
	if err != nil {
		return err
	}
	btcClient, err := rpcClient.BTCRPCClient()
	if err != nil {
		return err
	}
	prevBlockHash := btcBlock.Header.PrevBlock
	prevBlockHeader, err := btcClient.GetBlockHeader(&prevBlockHash)
	if err != nil {
		return err
	}
	blockProducing := uint32(btcBlock.Header.Timestamp.Sub(prevBlockHeader.Timestamp).Seconds())
	logger.Log.Debug().Uint32("block-producing-time-sec", blockProducing).Dur(
		"block-producing-time", btcBlock.Header.Timestamp.Sub(prevBlockHeader.Timestamp)).Msg("Connect block")
	return i.writeHashRateInDifferentResolutions(blockProducing, btcBlock.Header.Timestamp)
}

func (i BtcBlockProducingTimeChart) DisconnectBlock(tx pgx.Tx, _ *core.AbstractMsgBlock, _ int64, _ *core.AbstractRPCClient) error {
	// No need to discard block producing time from the accumulated data.
	// Even if block is marked as orphan - it's hash rate still should be calculated in a global total.
	return nil
}
