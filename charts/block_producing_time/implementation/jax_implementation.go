package implementation

import (
	"github.com/jackc/pgx/v4"
	"gitlab.com/jaxnet/core/indexer/core"
	"gitlab.com/jaxnet/core/indexer/core/blocks_cache"
	"gitlab.com/jaxnet/core/indexer/core/logger"
	"gitlab.com/jaxnet/core/indexer/core/settings"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

type JaxBlockProducingTimeChart struct {
	BlockProducingTimeChart
}

func (i JaxBlockProducingTimeChart) InitRPCClient() (*core.AbstractRPCClient, error) {
	return core.InitJaxRPCClient()
}

func (i JaxBlockProducingTimeChart) FetchNextBlockFromNode(offset int64, checkPrevBlock bool,
	rpcClient *core.AbstractRPCClient, cacheHandler *blocks_cache.CacheHandler) (
	block *core.AbstractMsgBlock, prevBlockNumber, blockHeight int64, rebuildingChainBlockNumber int64, err error) {
	return core.FetchNextBlockFromJaxNode(offset, checkPrevBlock, rpcClient, cacheHandler)
}

func (i JaxBlockProducingTimeChart) ConnectBlock(tx pgx.Tx, block *core.AbstractMsgBlock, _ int64, rpcClient *core.AbstractRPCClient) error {
	// WARN: `tx` is a PostgreSQL's transaction, that can not be used in this context.
	// 		 As a result - there is no atomicity here!
	// 		 In case of any error on upper levels - the write operation into the clickhouse would not be rolled back,
	//		 but it should be OK for most cases (charts should not be 100% correct).
	jaxBlock, err := block.JaxMsgBlock()
	if err != nil {
		return err
	}
	jaxClient, err := rpcClient.JaxRPCClient()
	if err != nil {
		return err
	}
	prevBlockHash := jaxBlock.Header.PrevBlockHash()
	logger.Log.Debug().Str("prev-block-hash", prevBlockHash.String()).Msg("")
	var prevBlockHeader wire.BlockHeader
	if settings.Conf.Blockchain.ID == 0 {
		prevBlockHeader, err = jaxClient.GetBeaconBlockHeader(&prevBlockHash)
		if err != nil {
			logger.Log.Debug().Msg("can't get prev beacon block header")
			return err
		}
	} else {
		prevBlockHeader, err = jaxClient.GetShardBlockHeader(&prevBlockHash)
		if err != nil {
			return err
		}
	}
	blockProducing := uint32(jaxBlock.Header.Timestamp().Sub(prevBlockHeader.Timestamp()).Seconds())
	logger.Log.Debug().Uint32("block-producing-time-sec", blockProducing).Dur(
		"block-producing-time", jaxBlock.Header.Timestamp().Sub(prevBlockHeader.Timestamp())).Msg("Connect block")
	return i.writeHashRateInDifferentResolutions(blockProducing, jaxBlock.Header.Timestamp())
}

func (i JaxBlockProducingTimeChart) DisconnectBlock(tx pgx.Tx, _ *core.AbstractMsgBlock, _ int64, _ *core.AbstractRPCClient) error {
	// No need to discard block producing time from the accumulated data.
	// Even if block is marked as orphan - it's producing time still should be calculated in a global total.
	return nil
}
