package implementation

import (
	"fmt"
	"github.com/fasthttp/router"
	"gitlab.com/jaxnet/core/indexer/charts/block_producing_time/core/interfaces"
	"gitlab.com/jaxnet/core/indexer/core/clickhouse"
	"gitlab.com/jaxnet/core/indexer/core/settings"
	"time"
)

// BlockProducingTimeChart calculates time of block producing
// and records it in Click House in daily, hourly and blockly formats.
type BlockProducingTimeChart struct{}

func (i BlockProducingTimeChart) SetHttpHandlers(group *router.Group) {
	interfaces.InitAPIHandlers(group)
}

func (i BlockProducingTimeChart) DefaultFirstBlockForProcessing() (blockNumber int64) {
	return 2
}

func (i BlockProducingTimeChart) PrepareDatabase() error {
	return nil
}

func (i BlockProducingTimeChart) writeHashRateInDifferentResolutions(difficulty uint32, t time.Time) (err error) {
	err = i.writeDailyResolution(difficulty, t)
	if err != nil {
		return
	}

	err = i.writeHourlyResolution(difficulty, t)
	if err != nil {
		return
	}

	err = i.writeBlocklyResolution(difficulty, t)
	return
}

func (i BlockProducingTimeChart) writeDailyResolution(blockProducingTimeInSec uint32, t time.Time) (err error) {
	query := fmt.Sprintf("INSERT INTO block_producing_time_daily__buffer "+
		"SELECT %d AS shardID, toDateTime('%s') AS timeStamp, maxState(toUInt32(%d)) AS maxBlockProducingTimeInSec;",
		settings.Conf.Blockchain.ID, clickhouse.DropTZInfo(dailyResolution(t).UTC()), blockProducingTimeInSec)

	return execQuery(query)
}

func (i BlockProducingTimeChart) writeHourlyResolution(blockProducingTimeInSec uint32, t time.Time) (err error) {
	query := fmt.Sprintf("INSERT INTO block_producing_time_hourly__buffer "+
		"SELECT %d AS shardID, toDateTime('%s') AS timeStamp, maxState(toUInt32(%d)) AS maxBlockProducingTimeInSec;",
		settings.Conf.Blockchain.ID, clickhouse.DropTZInfo(hourlyResolution(t).UTC()), blockProducingTimeInSec)

	return execQuery(query)
}

func (i BlockProducingTimeChart) writeBlocklyResolution(blockProducingTimeInSec uint32, t time.Time) (err error) {
	query := fmt.Sprintf("INSERT INTO block_producing_time_blockly__buffer "+
		"SELECT %d AS shardID, toDateTime('%s') AS timeStamp, maxState(toUInt32(%d)) AS maxBlockProducingTimeInSec;",
		settings.Conf.Blockchain.ID, clickhouse.DropTZInfo(t.UTC()), blockProducingTimeInSec)

	return execQuery(query)
}

func hourlyResolution(t time.Time) time.Time {
	return time.Date(t.Year(), t.Month(), t.Day(), t.Hour(), 0, 0, 0, t.Location())
}

func dailyResolution(t time.Time) time.Time {
	return time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, t.Location())
}

func execQuery(query string) (err error) {
	tx, err := clickhouse.BeginOperation()
	if err != nil {
		return
	}

	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return
	}

	err = tx.Commit()
	return
}
