package interfaces

import (
	"github.com/fasthttp/router"
)

func InitAPIHandlers(root *router.Group) {
	root.GET("block-producing-time/", handleGetBlockProducingTimesRequest)
}
