package main

import (
	"gitlab.com/jaxnet/core/indexer/charts/block_producing_time/implementation"
	"gitlab.com/jaxnet/core/indexer/core/fake"
)

func main() {
	fake.Main(&implementation.JaxBlockProducingTimeChart{}, &implementation.BtcBlockProducingTimeChart{})
}
