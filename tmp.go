/*
 * Copyright (c) 2021 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */
package main

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/jaxnet/jaxnetd/jaxutil/txutils"
	"gitlab.com/jaxnet/jaxnetd/network/rpcclient"
	"gitlab.com/jaxnet/jaxnetd/txscript"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
)

func interruptOnError(e error) {
	if e != nil {
		log.Fatal(e)
	}
}
func prettyPrint(val interface{}) {
	data, _ := json.MarshalIndent(val, "", "  ")
	fmt.Println(string(data))
}

func _main() {
	connCfg := &rpcclient.ConnConfig{
		Params: "testnet",
		// Host: "127.0.0.1:18333",
		Host:         "198.199.125.197:18333",
		User:         "jaxnetrpc",
		Pass:         "AUL6VBjoQnhP3bfFzl",
		HTTPPostMode: true,
		DisableTLS:   true,
	}

	rpcClient, err := rpcclient.New(connCfg, nil)
	interruptOnError(err)
	rpcClient = rpcClient
	v, err := rpcClient.Version()
	interruptOnError(err)
	prettyPrint(connCfg.Host + " version=" + v.Node.Version)
	const shard = 2
	for _, i := range []string{} {
		prettyPrint("")
		prettyPrint(i)
		hash, _ := chainhash.NewHashFromStr(i)
		res1, err := rpcClient.ForShard(shard).GetTx(hash, false, false)
		prettyPrint(err)
		prettyPrint("GetTx:")
		prettyPrint(res1)
		prettyPrint("")

		res3, err := rpcClient.ForShard(shard).GetRawTransaction(hash, false)
		prettyPrint(err)
		prettyPrint("GetRawTransaction:")
		prettyPrint(res3)
		prettyPrint("")

		res4, err := rpcClient.ForShard(shard).GetRawTransactionVerbose(hash, false)
		prettyPrint(err)
		prettyPrint("GetRawTransactionVerbose:")
		prettyPrint(res4)
		prettyPrint("")

		res5, err := rpcClient.ForShard(shard).GetTxDetails(hash, false)
		prettyPrint(err)
		prettyPrint("GetTxDetails:")
		prettyPrint(res5)
		prettyPrint("")

		prettyPrint("GetTxOut:")

		for ind := 0; ind < 5; ind++ {
			println("tx out #", ind)
			res5, err := rpcClient.ForShard(shard).GetTxOut(hash, uint32(ind), false, false)
			prettyPrint(err)
			prettyPrint(res5)
		}

		prettyPrint("")
	}

}

func inspectTx(rawTx string) {
	connCfg := &rpcclient.ConnConfig{
		Params: "testnet",
		// Host: "127.0.0.1:18333",
		Host:         "198.199.125.197:18333",
		User:         "jaxnetrpc",
		Pass:         "AUL6VBjoQnhP3bfFzl",
		HTTPPostMode: true,
		DisableTLS:   true,
	}

	rpcClient, err := rpcclient.New(connCfg, nil)
	interruptOnError(err)
	rpcClient = rpcClient
	v, err := rpcClient.Version()
	interruptOnError(err)
	prettyPrint(connCfg.Host + " version=" + v.Node.Version)

	msgTx, err := txutils.DecodeTx(rawTx)
	interruptOnError(err)

	// log := zerolog.New(zerolog.ConsoleWriter{Out: os.Stderr, NoColor: false}).Level(zerolog.TraceLevel)
	// zerolog.SetGlobalLevel(zerolog.TraceLevel)
	// txscript.UseLogger(log)

	for i, in := range msgTx.TxIn {
		println("")
		inShard := 1
		parent, err := rpcClient.ForShard(1).GetTx(&in.PreviousOutPoint.Hash, false, false)
		if err != nil {
			inShard = 2
			parent, err = rpcClient.ForShard(2).GetTx(&in.PreviousOutPoint.Hash, false, false)
		}

		if parent == nil {
			prettyPrint("Err: parent not found for input " + strconv.Itoa(i))
			continue
		}

		res, _ := txscript.DisasmString(in.SignatureScript)
		prettyPrint("SIGNATURE SCRIPT: " + res)
		chunks := strings.Split(res, " ")
		redeem, _ := hex.DecodeString(chunks[len(chunks)-1])
		res, _ = txscript.DisasmString(redeem)
		prettyPrint("REDEEM SCRIPT: " + res)
		prettyPrint("PARENT TX: " + parent.RawTx)

		parentTx, err := txutils.DecodeTx(parent.RawTx)
		interruptOnError(err)

		fmt.Printf("Parent of input %v found in shard %v; parent tx(%v, %v), tx_version=%v\n",
			i, inShard, parentTx.TxHash().String(), in.PreviousOutPoint.Index, parentTx.Version)

		parentOut := parentTx.TxOut[in.PreviousOutPoint.Index]

		vm, err := txscript.NewEngine(parentOut.PkScript, msgTx, i,
			txscript.StandardVerifyFlags, nil, nil, parentOut.Value)
		if err != nil {
			prettyPrint(errors.Wrap(err, "unable to init txScript engine "+strconv.Itoa(i)).Error())
			continue
		}

		if err = vm.Execute(); err != nil {
			prettyPrint(errors.Wrap(err, "tx script exec failed "+strconv.Itoa(i)).Error())
			continue
		}
		prettyPrint("Input verified " + strconv.Itoa(i))
	}
}

func main() {
	txs := []string{
		"0100010004dd4a00c6b2bb3f718657e568009c0c858f5c06a82e0a3982944a139203079637010000004900473044022038914eef9821532c9c11542659a668e807e77af7ef39682c058023c8f343570a02204f605cff0a255a9252225e00fce413ba7416cafc6c9b074063034270782aa9fc01ffffffff9b91f5f9c555d77e891794f906e58b7fbc52977448243fe78ae918ff0cf1f293010000008b483045022100973793a1543eee82aff50851b9860c9eaf7ed60712e966faae76046be223dea00220541875a2c1041f31a7d57c996536dac9131feeddaf5e53cef38babffa9d1ce770141043fc5c0b936bdb1595ca4807ebab7dfc5418abb6c030e58ea6b4270e524d3da9c9ff6ad1798097b14571fe21b2325c55a685ab5846848a7e4de8dc34f3231c88fffffffff17844dc3240d5240c72aee539661c1c54a47259ac40ccfa343d6fb3218bc7997000000004a00483045022100cdf8c4db6763984fafefd951c6aa5fa4a5610b4774fbb7b08301137525c3360502206afeb8033493df8900bdd2361db5da569e67082c14c80aac3c5cecb006bb0f0a01ffffffff17844dc3240d5240c72aee539661c1c54a47259ac40ccfa343d6fb3218bc7997010000008b4830450221009164cdcaab6ee71290da78360da980ceb22bcedd0b99b59371802c5574c3280d02205c2d303dd5b28544be9c0fe8596057efdddff39d9528150ddee46be1359b23700141043fc5c0b936bdb1595ca4807ebab7dfc5418abb6c030e58ea6b4270e524d3da9c9ff6ad1798097b14571fe21b2325c55a685ab5846848a7e4de8dc34f3231c88fffffffff04f00a0000000000001976a9145496c151b3a251c77b5544b1d3ac6242587215e888aca0410100000000001976a9145496c151b3a251c77b5544b1d3ac6242587215e888acd40a0000000000001976a9149bd8681d7f972de68b1ce72e534da3d2f85782f288ace5050000000000001976a9145496c151b3a251c77b5544b1d3ac6242587215e888ac00000000",
		"0100010004dd4a00c6b2bb3f718657e568009c0c858f5c06a82e0a3982944a13920307963701000000fd1e0100473044022063e36113c733308dbf89d387256ddc38f151284fb9373902663ca3b42de95c650220582f70760bfa533fdc05a3a11e42f029389d783d3293d7c75333c04768c75a3d014cd3bc02f0009f635241043a465d4b86a2161421a52ca9bdfb9cafaac8c8dd284e361331cca7e8fd678925471c687ccb18cf04e2ab2c12a74e51c272e5aad2c1b2529ac0d061b949498e5841043fc5c0b936bdb1595ca4807ebab7dfc5418abb6c030e58ea6b4270e524d3da9c9ff6ad1798097b14571fe21b2325c55a685ab5846848a7e4de8dc34f3231c88f52ae6741043a465d4b86a2161421a52ca9bdfb9cafaac8c8dd284e361331cca7e8fd678925471c687ccb18cf04e2ab2c12a74e51c272e5aad2c1b2529ac0d061b949498e58ac7768ffffffff9b91f5f9c555d77e891794f906e58b7fbc52977448243fe78ae918ff0cf1f293010000008b483045022100973793a1543eee82aff50851b9860c9eaf7ed60712e966faae76046be223dea00220541875a2c1041f31a7d57c996536dac9131feeddaf5e53cef38babffa9d1ce770141043fc5c0b936bdb1595ca4807ebab7dfc5418abb6c030e58ea6b4270e524d3da9c9ff6ad1798097b14571fe21b2325c55a685ab5846848a7e4de8dc34f3231c88fffffffff17844dc3240d5240c72aee539661c1c54a47259ac40ccfa343d6fb3218bc799700000000fd1e0100483045022100d0892772a43fd2dd616300a320f3f5f85f0c170d8d788da1c04c59c53175667d02200d4e12ca4f01303b767e197c985741d56c7bd27824948dfaa0beb62a7927bb78014cd2bc01789f635241043a465d4b86a2161421a52ca9bdfb9cafaac8c8dd284e361331cca7e8fd678925471c687ccb18cf04e2ab2c12a74e51c272e5aad2c1b2529ac0d061b949498e5841043fc5c0b936bdb1595ca4807ebab7dfc5418abb6c030e58ea6b4270e524d3da9c9ff6ad1798097b14571fe21b2325c55a685ab5846848a7e4de8dc34f3231c88f52ae6741043fc5c0b936bdb1595ca4807ebab7dfc5418abb6c030e58ea6b4270e524d3da9c9ff6ad1798097b14571fe21b2325c55a685ab5846848a7e4de8dc34f3231c88fac7768ffffffff17844dc3240d5240c72aee539661c1c54a47259ac40ccfa343d6fb3218bc7997010000008b4830450221009164cdcaab6ee71290da78360da980ceb22bcedd0b99b59371802c5574c3280d02205c2d303dd5b28544be9c0fe8596057efdddff39d9528150ddee46be1359b23700141043fc5c0b936bdb1595ca4807ebab7dfc5418abb6c030e58ea6b4270e524d3da9c9ff6ad1798097b14571fe21b2325c55a685ab5846848a7e4de8dc34f3231c88fffffffff04f00a0000000000001976a9145496c151b3a251c77b5544b1d3ac6242587215e888aca0410100000000001976a9145496c151b3a251c77b5544b1d3ac6242587215e888acd40a0000000000001976a9149bd8681d7f972de68b1ce72e534da3d2f85782f288ace5050000000000001976a9145496c151b3a251c77b5544b1d3ac6242587215e888ac00000000",
	}

	for _, rawTx := range txs {
		inspectTx(rawTx)
		println("--------------------------")
	}
}
