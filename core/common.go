package core

import (
	"errors"
	btcclient "github.com/btcsuite/btcd/rpcclient"
	"gitlab.com/jaxnet/core/indexer/core/blocks_cache"
	"gitlab.com/jaxnet/core/indexer/core/ec"
	"gitlab.com/jaxnet/core/indexer/core/logger"
	"gitlab.com/jaxnet/core/indexer/core/settings"
	jaxclient "gitlab.com/jaxnet/jaxnetd/network/rpcclient"
	"time"
)

func InitJaxRPCClient() (*AbstractRPCClient, error) {
	connCfg := &jaxclient.ConnConfig{
		Host:         settings.Conf.Blockchain.RPC.Net.Interface(),
		User:         settings.Conf.Blockchain.RPC.Credentials.User,
		Pass:         settings.Conf.Blockchain.RPC.Credentials.Pass,
		HTTPPostMode: true, // Bitcoin core only supports HTTP POST mode
		DisableTLS:   true, // Bitcoin core does not provide TLS by default
		ShardID:      settings.Conf.Blockchain.ID,
	}
	connCfg.Params = settings.Conf.Blockchain.NetworkName

	client, err := jaxclient.New(connCfg, nil)
	if err != nil {
		return nil, err
	}

	if settings.Conf.Blockchain.ID == 0 {
		client = client.ForBeacon()
	} else {
		client = client.ForShard(settings.Conf.Blockchain.ID)
	}

	return Init(client), err
}

func InitBTCRPCClient() (*AbstractRPCClient, error) {
	connCfg := &btcclient.ConnConfig{
		Host:         settings.Conf.Blockchain.RPC.Net.Interface(),
		User:         settings.Conf.Blockchain.RPC.Credentials.User,
		Pass:         settings.Conf.Blockchain.RPC.Credentials.Pass,
		HTTPPostMode: true, // Bitcoin core only supports HTTP POST mode
		DisableTLS:   true, // Bitcoin core does not provide TLS by default
	}
	connCfg.Params = settings.Conf.Blockchain.NetworkName

	client, err := btcclient.New(connCfg, nil)
	if err != nil {
		return nil, err
	}

	return Init(client), err
}

func FetchNextBlockFromJaxNode(offset int64, checkPrevBlock bool, rpcClient *AbstractRPCClient, cacheHandler *blocks_cache.CacheHandler) (
	block *AbstractMsgBlock, prevBlockNum, blockHeight int64, rebuildingChainBlockNumber int64, err error) {
	jaxRpcClient, err := rpcClient.JaxRPCClient()
	if err != nil {
		return
	}
	const maxAttempts = 600
	for attempt := 0; attempt < maxAttempts; attempt++ {

		var blockResult *jaxclient.BlockResult
		if cacheHandler == nil {
			if settings.Conf.Blockchain.ID == 0 {
				blockResult, err = jaxRpcClient.GetBeaconBlockBySerialNumber(offset)
			} else {
				blockResult, err = jaxRpcClient.GetShardBlockBySerialNumber(offset)
			}
		} else {
			blockResult, err = getJaxBlockFromCacheOrNode(offset, cacheHandler, jaxRpcClient)
		}
		if err != nil {
			if err.Error() == ec.JaxBlockNotFoundError || err.Error() == ec.BtcBlockNotFoundError ||
				err.Error() == ec.JaxChainNotFoundError {
				time.Sleep(time.Duration(settings.Conf.Blockchain.NewBlockFetchingPeriodInSec) * time.Second)
			} else {
				time.Sleep(time.Duration(settings.Conf.Blockchain.BlocksFetchingPeriodInSec) * time.Second)
			}
			continue
		}

		if checkPrevBlock {
			if blockResult.SerialID-blockResult.PrevSerialID != 1 {
				rebuildingChainBlockNumber = blockResult.PrevSerialID
				return nil, blockResult.PrevSerialID, int64(blockResult.Height), rebuildingChainBlockNumber, nil
			}
		}

		return InitBlock(blockResult.Block), blockResult.PrevSerialID, int64(blockResult.Height), 0, nil
	}
	return
}

func getJaxBlockFromCacheOrNode(blockNum int64, cacheHandler *blocks_cache.CacheHandler, rpcClient *jaxclient.Client) (
	blockResult *jaxclient.BlockResult, err error) {
	blockResult, err = cacheHandler.GetJaxBlock(blockNum)
	var blockResultArr []*jaxclient.BlockResult
	if err != nil {
		if settings.Conf.Blockchain.ID == 0 {
			blockResultArr, err = rpcClient.ListBeaconBlocksBySerialNumber(blockNum, cacheHandler.CountBlocksPerRequest)
		} else {
			blockResultArr, err = rpcClient.ListShardBlocksBySerialNumber(blockNum, cacheHandler.CountBlocksPerRequest)
		}
		if err != nil {
			return nil, err
		}
		if len(blockResultArr) == 0 {
			err = errors.New("empty list of blocks from node")
			return
		}
		for _, el := range blockResultArr {
			cacheHandler.AddJaxBlock(el.SerialID, el)
		}
		blockResult = blockResultArr[0]
	}
	return
}

func FetchNextBlockFromBtcNode(offset int64, checkPrevBlock bool, rpcClient *AbstractRPCClient, cacheHandler *blocks_cache.CacheHandler) (
	block *AbstractMsgBlock, prevBlockNum, blockHeight int64, rebuildingChainBlockNumber int64, err error) {
	btcRpcClient, err := rpcClient.BTCRPCClient()
	if err != nil {
		return
	}
	const maxAttempts = 600
	logger.Log.Debug().Msg("Try to get block from BTC node")
	for attempt := 0; attempt < maxAttempts; attempt++ {

		var blockResult *btcclient.BlockResult
		var errGettingBlock error
		if cacheHandler == nil {
			startTime := time.Now()
			blockResult, errGettingBlock = btcRpcClient.GetBlockBySerialNumber(offset)
			logger.Log.Debug().Str("duration", time.Now().Sub(startTime).String()).Msg("Getting block time")
		} else {
			blockResult, errGettingBlock = getBtcBlockFromCacheOrNode(offset, cacheHandler, btcRpcClient)
		}
		if errGettingBlock != nil {
			err = errGettingBlock
			logger.Log.Debug().Err(errGettingBlock).Msg("Can't get block from node")
			time.Sleep(time.Duration(settings.Conf.Blockchain.BlocksFetchingPeriodInSec) * time.Second)
			continue
		}

		if checkPrevBlock {
			if blockResult.SerialID-blockResult.PrevSerialID != 1 {
				rebuildingChainBlockNumber = blockResult.PrevSerialID
				return nil, blockResult.PrevSerialID, int64(blockResult.Height), rebuildingChainBlockNumber, nil
			}
		}

		return InitBlock(blockResult.Block), blockResult.PrevSerialID, int64(blockResult.Height), 0, nil
	}

	return
}

func getBtcBlockFromCacheOrNode(blockNum int64, cacheHandler *blocks_cache.CacheHandler, rpcClient *btcclient.Client) (
	blockResult *btcclient.BlockResult, err error) {
	blockResult, err = cacheHandler.GetBtcBlock(blockNum)
	var blockResultArr []*btcclient.BlockResult
	if err != nil {
		blockResultArr, err = rpcClient.ListBlocksBySerialNumber(blockNum, cacheHandler.CountBlocksPerRequest)
		if err != nil {
			return nil, err
		}
		if len(blockResultArr) == 0 {
			err = errors.New("empty list of blocks from node")
			return
		}
		for _, el := range blockResultArr {
			cacheHandler.AddBtcBlock(el.SerialID, el)
		}
		blockResult = blockResultArr[0]
	}
	return
}
