package logger

import "github.com/rs/zerolog"

// Error prints err message to the log in error scope.
func Error(err error) {
	Log.Err(err).Msg("")
}

// Err returns logger event configured to enrich scope with additional context.
func Err(err error) *zerolog.Event {
	return Log.Err(err)
}

func Info() *zerolog.Event {
	return Log.Info()
}
