package database

import (
	"regexp"
	"strings"
)

func MinimizeSQLScript(query string) string {
	space := regexp.MustCompile(`\s+`)

	s := space.ReplaceAllString(query, " ")
	s = strings.Replace(s, ") )", "))", -1)
	s = strings.Replace(s, " )", ")", -1)
	s = strings.Replace(s, ") ", ")", -1)
	s = strings.Replace(s, " (", "(", -1)
	s = strings.Replace(s, "( ", "(", -1)
	return s
}
