package database

import (
	"context"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/jaxnet/core/indexer/core/ec"
	"gitlab.com/jaxnet/core/indexer/core/settings"
	"time"
)

var (
	DB *pgxpool.Pool
)

func Init() (err error) {

	// Before setting up new pool — previous one should be dropped.
	// Database reinitialisation could happen relatively often,
	// which leads to memory leaks if processed incorrectly.
	//
	// See func Exec(tx pgx.Tx, sql string) (tag pgconn.CommandTag, err error)
	// for the case when DB is reinitialised.
	Free()

	DB, err = pgxpool.Connect(context.Background(), settings.Conf.DB.PSQLConnectionCredentials())
	if err != nil {
		return
	}

	err = EnsureSchema()
	return
}

func Free() {
	if DB != nil {
		DB.Close()
	}
}

func BeginTransaction() (tx *pgx.Tx, err error) {
	const maxAttempts = 10

	for attempt := 0; attempt < maxAttempts; attempt++ {
		txInstance, err := DB.Begin(context.Background())
		if err == nil {
			return &txInstance, err

		} else {
			_ = txInstance.Rollback(context.Background())
		}

		// If error - sleep a bit and try once more.
		time.Sleep(time.Second * 1)
	}

	err = ec.ErrNoTxCanBeCreated
	return
}
