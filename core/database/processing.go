package database

import (
	"context"
	"database/sql/driver"
	"errors"
	"github.com/jackc/pgconn"
	"github.com/jackc/pgx/v4"
	"gitlab.com/jaxnet/core/indexer/core"
	"gitlab.com/jaxnet/core/indexer/core/ec"
	"gitlab.com/jaxnet/core/indexer/core/settings"
	"strconv"
	"strings"
)

func IndexNamePrefix() string {
	return settings.Conf.Name
}

func DataTableName() string {
	return settings.Conf.Name + "_data"
}

func BlocksForProcessingTableName() string {
	return settings.Conf.Name + "_blocks_for_processing"
}

func ProcessedBlocksTableName() string {
	return settings.Conf.Name + "_processed_blocks"
}

func BlockConnectingStateTableName() string {
	return settings.Conf.Name + "_blocks_connecting_state"
}

func AdditionalDataTableName(dataTablePostfix string) string {
	return DataTableName() + "_" + dataTablePostfix
}

// InitIndexBlockOffset tries to initialize index's block offset number before usage.
// In case if this index already has the offset number - no error would be emitted (muted on database level).
// Returns current offset that has been last stored.
func InitIndexBlockOffset() (currentOffset int64, err error) {
	query :=
		"INSERT INTO indexes_offsets (index_name, index_offset) " +
			"VALUES ('{{ index_name }}', 0) " +
			"ON CONFLICT (\"index_name\") " +
			"DO UPDATE SET index_offset = indexes_offsets.index_offset " +
			"RETURNING index_offset"
	query = strings.Replace(query, "{{ index_name }}", settings.Conf.Name, 1)

	tx, err := BeginTransaction()
	if err != nil {
		return
	}
	defer (*tx).Rollback(context.Background())

	rows, err := Query(*tx, query)
	if err != nil {
		return
	}
	defer rows.Close()

	if !rows.Next() {
		err = errors.New("unexpected response from the database")
		return
	}

	values, err := rows.Values()
	if err != nil {
		return
	}

	offset, ok := values[0].(int64)
	if !ok {
		err = errors.New("unexpected response from the database")
		return
	}
	currentOffset = offset

	// This must be set here.
	// Even despite previous defer.
	rows.Close()

	err = Commit(*tx)
	return
}

// IncrementIndexOffset increments block offset of the index in database,
// using the same TX context, as the rest of index mechanics.
// Returns current offset of the index, that is fetched in the same query,
// to save one round trip to the database.
func IncrementIndexOffset(tx pgx.Tx) (currentOffset int64, err error) {
	query :=
		"UPDATE indexes_offsets " +
			"SET index_offset = index_offset + 1 " +
			"WHERE index_name = '{{ index_name }}' " +
			"RETURNING index_offset"

	query = strings.Replace(query, "{{ index_name }}", settings.Conf.Name, 1)
	rows, err := Query(tx, query)
	if err != nil {
		return
	}
	defer rows.Close()

	if !rows.Next() {
		err = errors.New("unexpected response from the database")
		return
	}

	values, err := rows.Values()
	if err != nil {
		return
	}

	offset, ok := values[0].(int64)
	if !ok {
		err = errors.New("unexpected response from the database")
		return
	}

	return offset, nil
}

// MarkBlockProcessed adds latest_blocks hash to the processed latest_blocks table.
// Returns error if this block is already processed.
func MarkBlockProcessed(tx pgx.Tx, hash string) (err error) {
	query := "INSERT INTO hashes (hash) VALUES ( '{{ hash }}' ) ON CONFLICT DO NOTHING"
	query = strings.Replace(query, "{{ hash }}", hash, 1)
	_, err = Exec(tx, query)
	if err != nil {
		return
	}

	query =
		"INSERT INTO {{ processed_blocks_table }} (hash) " +
			"SELECT id AS hash FROM hashes WHERE hash = '{{ hash }}'"

	query = strings.Replace(query, "{{ processed_blocks_table }}", ProcessedBlocksTableName(), 1)
	query = strings.Replace(query, "{{ hash }}", hash, 1)
	_, err = Exec(tx, query)
	if err != nil {
		return
	}

	return
}

// BlockIsAlreadyProcessed returns `true` if block with such hash has been marked as processed earlier.
// Otherwise (including error cases) returns `false`.
func BlockIsAlreadyProcessed(tx pgx.Tx, hash string) (processed bool, err error) {
	query :=
		"SELECT count(hash) " +
			"FROM {{ processed_blocks_table }} " +
			"WHERE hash IN " +
			"(SELECT id AS hash FROM hashes WHERE hash = '{{ hash }}')"
	query = strings.Replace(query, "{{ hash }}", hash, 1)
	query = strings.Replace(query, "{{ processed_blocks_table }}", ProcessedBlocksTableName(), 1)

	rows, err := Query(tx, query)
	if err != nil {
		return
	}

	defer rows.Close()
	if !rows.Next() {
		err = errors.New("unexpected response from the database")
		return
	}

	values, err := rows.Values()
	if err != nil {
		return
	}

	count, ok := values[0].(int64)
	if !ok {
		err = errors.New("unexpected response from the database")
		return
	}

	return count > 0, nil
}

// GetNextBlockNumberForProcessing returns block number, that is stored in the database as next to be processed.
// `tx` defines the postgres's transaction into which the data should be fetched.
// `impl` is used for the cases when blocks processing should begin in a non-standard manner (e.g. not from zero index).
func GetNextBlockNumberForProcessing(tx pgx.Tx, impl core.IndexImplementation) (blockNumber int64, forward bool, checkPrevBlock bool, err error) {
	query :=
		"SELECT block_number, forward, check_prev_block " +
			"FROM {{ blocks_for_processing_table }} " +
			"ORDER BY id LIMIT 1"
	query = strings.Replace(query, "{{ blocks_for_processing_table }}", BlocksForProcessingTableName(), 1)

	rows, err := Query(tx, query)
	if err != nil {
		return
	}
	defer rows.Close()

	if !rows.Next() {
		blockNumber = impl.DefaultFirstBlockForProcessing()
		forward = true
		checkPrevBlock = false
		return
	}

	values, err := rows.Values()
	if err != nil {
		return
	}

	blockNumber, ok := values[0].(int64)
	if !ok {
		err = errors.New("unexpected response from the database")
	}

	forward, ok = values[1].(bool)
	if !ok {
		err = errors.New("unexpected response from the database")
	}

	checkPrevBlock, ok = values[2].(bool)
	if !ok {
		err = errors.New("unexpected response from the database")
	}

	return
}

func AddNextBlockNumberForProcessing(tx pgx.Tx, blockNumber int64, forward bool, checkPrevBlock bool) (err error) {
	query := "INSERT INTO {{ blocks_for_processing_table }} (block_number, forward, check_prev_block) " +
		"VALUES ( {{ block_number }}, {{ forward }}, {{ check_prev_block }} ) "
	query = strings.Replace(query, "{{ blocks_for_processing_table }}", BlocksForProcessingTableName(), 1)
	query = strings.Replace(query, "{{ block_number }}", strconv.FormatInt(blockNumber, 10), 1)
	query = strings.Replace(query, "{{ forward }}", strconv.FormatBool(forward), 1)
	query = strings.Replace(query, "{{ check_prev_block }}", strconv.FormatBool(checkPrevBlock), 1)
	_, err = Exec(tx, query)
	if err != nil {
		return
	}

	return
}

func RemoveNextBlockNumberForProcessing(tx pgx.Tx, blockNumber int64, forward bool) (err error) {
	query := "DELETE FROM {{ blocks_for_processing_table }} WHERE block_number = {{ block_number }} AND forward = {{ forward }} "
	query = strings.Replace(query, "{{ blocks_for_processing_table }}", BlocksForProcessingTableName(), 1)
	query = strings.Replace(query, "{{ block_number }}", strconv.FormatInt(blockNumber, 10), 1)
	query = strings.Replace(query, "{{ forward }}", strconv.FormatBool(forward), 1)
	_, err = Exec(tx, query)
	if err != nil {
		return
	}

	return
}

func InsertBlockConnectingInfo(tx pgx.Tx, blockNumber, prevBlockNumber int64, state uint8) (err error) {
	query := "INSERT INTO {{ blocks_connecting_state_table }} (serial_id, prev_serial_id, state) " +
		"VALUES ( {{ block_number }}, {{ prev_block_number }}, {{ state }} ) "
	query = strings.Replace(query, "{{ blocks_connecting_state_table }}", BlockConnectingStateTableName(), 1)
	query = strings.Replace(query, "{{ block_number }}", strconv.FormatInt(blockNumber, 10), 1)
	query = strings.Replace(query, "{{ prev_block_number }}", strconv.FormatInt(prevBlockNumber, 10), 1)
	query = strings.Replace(query, "{{ state }}", strconv.FormatUint(uint64(state), 10), 1)
	_, err = Exec(tx, query)
	if err != nil {
		return
	}

	return
}

func GetPrevBlockNumber(tx pgx.Tx, blockNumber int64) (prevBlockNumber int64, err error) {
	query :=
		"SELECT prev_serial_id " +
			"FROM {{ blocks_connecting_state_table }} " +
			"WHERE serial_id = {{ block_num }}" +
			"ORDER BY serial_id DESC LIMIT 1"
	query = strings.Replace(query, "{{ blocks_connecting_state_table }}", BlockConnectingStateTableName(), 1)
	query = strings.Replace(query, "{{ block_num }}", strconv.FormatInt(blockNumber, 10), 1)

	rows, err := Query(tx, query)
	if err != nil {
		return
	}
	defer rows.Close()

	if !rows.Next() {
		err = ec.ErrNoRecordsInDB
		return
	}

	values, err := rows.Values()
	if err != nil {
		return
	}

	prevBlockNumber, ok := values[0].(int64)
	if !ok {
		err = errors.New("unexpected response from the database")
	}

	return
}

func GetBlockConnectingState(tx pgx.Tx, blockNumber int64) (state uint8, err error) {
	query :=
		"SELECT state " +
			"FROM {{ blocks_connecting_state_table }} " +
			"WHERE serial_id = {{ block_num }} " +
			"LIMIT 1"
	query = strings.Replace(query, "{{ blocks_connecting_state_table }}", BlockConnectingStateTableName(), 1)
	query = strings.Replace(query, "{{ block_num }}", strconv.FormatInt(blockNumber, 10), 1)

	rows, err := Query(tx, query)
	if err != nil {
		return
	}
	defer rows.Close()

	if !rows.Next() {
		err = ec.ErrNoRecordsInDB
		return
	}

	values, err := rows.Values()
	if err != nil {
		return
	}

	stateFromDB, ok := values[0].(int16)
	if !ok {
		err = errors.New("unexpected response from the database")
		return
	}
	state = uint8(stateFromDB)

	return
}

func UpdateBlockConnectingState(tx pgx.Tx, blockNumber int64, newState uint8) (err error) {
	query :=
		"UPDATE {{ blocks_connecting_state_table }} " +
			"SET state = {{ new_state }} " +
			"WHERE serial_id = '{{ block_num }}' "

	query = strings.Replace(query, "{{ blocks_connecting_state_table }}", BlockConnectingStateTableName(), 1)
	query = strings.Replace(query, "{{ new_state }}", strconv.FormatUint(uint64(newState), 10), 1)
	query = strings.Replace(query, "{{ block_num }}", strconv.FormatInt(blockNumber, 10), 1)
	_, err = Exec(tx, query)
	if err != nil {
		return
	}
	return
}

func DeleteOutdatedConnectingStateRecords(tx pgx.Tx, blockNumber int64) (err error) {
	query := "DELETE FROM {{ blocks_connecting_state_table }} WHERE serial_id <= {{ block_number }} "
	query = strings.Replace(query, "{{ blocks_connecting_state_table }}", BlockConnectingStateTableName(), 1)
	query = strings.Replace(query, "{{ block_number }}", strconv.FormatInt(blockNumber, 10), 1)
	_, err = Exec(tx, query)
	if err != nil {
		return
	}
	return
}

func Exec(tx pgx.Tx, sql string) (tag pgconn.CommandTag, err error) {
	tag, err = tx.Exec(context.Background(), sql)
	if err != nil {
		if errors.Is(err, driver.ErrBadConn) {
			// Postgresql most probably has been restarted.
			// In this case — all connections should be reinitialised.

			err = Init()
			if err != nil {
				return
			}

			return tx.Exec(context.Background(), sql)
		}
	}

	return
}

func Query(tx pgx.Tx, sql string) (rows pgx.Rows, err error) {
	rows, err = tx.Query(context.Background(), sql)

	if err != nil {
		if errors.Is(err, driver.ErrBadConn) {
			// Postgresql most probably has been restarted.
			// In this case — all connections should be reinitialised.

			err = Init()
			if err != nil {
				return
			}

			return tx.Query(context.Background(), sql)
		}
	}

	return
}

func QueryTxFree(sql string, args ...interface{}) (rows pgx.Rows, err error) {
	rows, err = DB.Query(context.Background(), sql, args...)

	if err != nil {
		if errors.Is(err, driver.ErrBadConn) {
			// Postgresql most probably has been restarted.
			// In this case — all connections should be reinitialised.

			err = Init()
			if err != nil {
				return
			}

			return DB.Query(context.Background(), sql, args...)
		}
	}

	return
}

func ExecTxFree(sql string) (tag pgconn.CommandTag, err error) {
	tag, err = DB.Exec(context.Background(), sql)

	if err != nil {
		if errors.Is(err, driver.ErrBadConn) {
			// Postgresql most probably has been restarted.
			// In this case — all connections should be reinitialised.

			err = Init()
			if err != nil {
				return
			}

			return DB.Exec(context.Background(), sql)
		}
	}

	return
}

func Commit(tx pgx.Tx) (err error) {
	return tx.Commit(context.Background())
}

func Rollback(tx pgx.Tx) (err error) {
	return tx.Rollback(context.Background())
}
