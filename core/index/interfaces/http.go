package interfaces

import (
	"github.com/fasthttp/router"
	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/core/indexer/core"
	"gitlab.com/jaxnet/core/indexer/core/logger"
	"gitlab.com/jaxnet/core/indexer/core/settings"
)

func HandleHttpInterface(impl core.IndexImplementation) (err error) {
	r := router.New()
	impl.SetHttpHandlers(r.Group(settings.Conf.Http.ApiPrefix))

	logRouterEntries(r)
	return fasthttp.ListenAndServe(settings.Conf.Http.Interface(), r.Handler)
}

func logRouterEntries(r *router.Router) {
	for method, handlers := range r.List() {
		for _, handler := range handlers {
			logger.Log.Info().Str(
				"method", method).Str(
				"handler", handler).Msg(
				"Route set")
		}
	}
}
