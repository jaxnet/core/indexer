package index

import (
	"github.com/jackc/pgx/v4"
	"gitlab.com/jaxnet/core/indexer/core"
	"gitlab.com/jaxnet/core/indexer/core/database"
	"gitlab.com/jaxnet/core/indexer/core/logger"
	"time"
)

// waitUntilNextBlockIsAvailable tries to fetch next block from the node,
// pauses the execution if there is no next block available yet.
// Automatically retries block fetching in case of several errors in a row.
//
// Reports error only in case if communication to the node can't be restored any more
// (this functionality is supposed to deal with occasional communication errors and automatic node reboots,
// but would not help if node would be offline for more than several minutes)
func (index *Index) waitUntilNextBlockIsAvailable(tx pgx.Tx, offset int64, checkPrevBlock bool, impl core.IndexImplementation) (
	block *core.AbstractMsgBlock, prevBlockNumber, blockHeight int64, rebuildingChainBlockNumber int64, err error) {

	for {
		block, prevBlockNumber, blockHeight, rebuildingChainBlockNumber, err = impl.FetchNextBlockFromNode(
			offset, checkPrevBlock, index.RPCClient, index.CacheHandler)
		if err != nil {
			logger.Log.Debug().Err(err).Msg("Can't fetch next block from node")
			return
		}
		if block == nil {
			logger.Log.Debug().Msg("Block is empty. Start rebuilding chain")
			return
		}

		if checkPrevBlock {
			var blockIsProcessed bool
			blockIsProcessed, err = database.BlockIsAlreadyProcessed(tx, block.BlockHash())
			if err != nil {
				return
			}

			if blockIsProcessed {
				logger.Log.Trace().Msg("LOOP!!!")
				time.Sleep(time.Second)
				continue
			}
		}

		return
	}
}
