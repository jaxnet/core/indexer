package index

import (
	"errors"
	"github.com/jackc/pgx/v4"
	"gitlab.com/jaxnet/core/indexer/core"
	"gitlab.com/jaxnet/core/indexer/core/database"
	"gitlab.com/jaxnet/core/indexer/core/ec"
	"gitlab.com/jaxnet/core/indexer/core/logger"
	"gitlab.com/jaxnet/core/indexer/core/metrics"
	"time"
)

// processNextBlockConnecting tries to index fetched block.
// In case of error - retries the attempt up to several times.
// For each attempt uses newly created transaction to prevent data duplication.
// Previous transaction is dropped (rolled back) before next one would be used.
func (index *Index) processNextBlockConnecting(tx *pgx.Tx, block *core.AbstractMsgBlock,
	blockHeight, blockNumber, prevBlockNumber int64,
	impl core.IndexImplementation) (txUpd *pgx.Tx, err error) {

	const maxAttempts = 3
	var processingStartTime time.Time
	var processingEndTime time.Time
	var timeDelta time.Duration
	var blockConnectingState uint8
	var gettingStateErr error
	for attempt := 0; attempt < maxAttempts; attempt++ {

		blockConnectingState, gettingStateErr = database.GetBlockConnectingState(*tx, blockNumber)
		if gettingStateErr != nil && gettingStateErr != ec.ErrNoRecordsInDB {
			err = gettingStateErr
			goto nextAttempt
		}

		if gettingStateErr == nil && blockConnectingState == ec.BlockStateConnected {
			err = errors.New("protocol error: trying to connect connected block")
			return
		}

		processingStartTime = time.Now()
		err = impl.ConnectBlock(*tx, block, blockHeight, index.RPCClient)
		if err != nil {
			goto nextAttempt
		}
		processingEndTime = time.Now()
		timeDelta = processingEndTime.Sub(processingStartTime)
		logger.Log.Debug().Str("duration", timeDelta.String()).Msg("Block connecting time")

		if gettingStateErr != nil {
			// firstly processed block
			err = database.MarkBlockProcessed(*tx, block.BlockHash())
			if err != nil {
				goto nextAttempt
			}
			err = database.RemoveNextBlockNumberForProcessing(*tx, blockNumber, true)
			if err != nil {
				goto nextAttempt
			}
			err = database.AddNextBlockNumberForProcessing(*tx, blockNumber+1, true, true)
			if err != nil {
				goto nextAttempt
			}
			err = database.InsertBlockConnectingInfo(*tx, blockNumber, prevBlockNumber, ec.BlockStateConnected)
			if err != nil {
				goto nextAttempt
			}
		} else {
			// rebuilding chain sequence
			err = database.RemoveNextBlockNumberForProcessing(*tx, blockNumber, true)
			if err != nil {
				goto nextAttempt
			}
			err = database.UpdateBlockConnectingState(*tx, blockNumber, ec.BlockStateConnected)
			if err != nil {
				goto nextAttempt
			}
		}

		err = database.DeleteOutdatedConnectingStateRecords(*tx, blockNumber-ec.BlockConnectingStateRecordsCnt)
		if err != nil {
			goto nextAttempt
		}

		metrics.GaugeSet(ec.MetricLastBlockConnected, float64(blockHeight))
		metrics.HistogramObserve(ec.MetricBlockConnectingTime, timeDelta.Seconds())
		return tx, err

	nextAttempt:
		// Previous transaction must be rolled back to free transaction instance on database level.
		rollbackErr := database.Rollback(*tx)
		if rollbackErr != nil {
			logger.Log.Debug().Err(rollbackErr).Msg("Can't rollback during block connection")
			return
		}

		// No reason to prepare next attempt if this one is the last one.
		if attempt < maxAttempts-1 {
			// Preparing one more attempt.
			// Next attempt must be processed in it's own transaction.
			tx, err = database.BeginTransaction()
			if err != nil {
				// `BeginTransaction` tries to handle errors internally.
				// If error is here then communication to the database can't be restored any more.
				return tx, err
			}
		}
	}

	return tx, err
}

// processNextBlockDisconnecting tries to index fetched block.
// In case of error - retries the attempt up to several times.
// For each attempt uses newly created transaction to prevent data duplication.
// Previous transaction is dropped (rolled back) before next one would be used.
func (index *Index) processNextBlockDisconnecting(tx *pgx.Tx, block *core.AbstractMsgBlock,
	blockHeight, blockNumber int64, impl core.IndexImplementation) (
	txUpd *pgx.Tx, err error) {

	const maxAttempts = 3
	for attempt := 0; attempt < maxAttempts; attempt++ {

		err = impl.DisconnectBlock(*tx, block, blockHeight, index.RPCClient)
		if err != nil {
			goto nextAttempt
		}

		err = database.RemoveNextBlockNumberForProcessing(*tx, blockNumber, false)
		if err != nil {
			goto nextAttempt
		}

		return tx, err

	nextAttempt:
		// Previous transaction must be rolled back to free transaction instance on database level.
		rollbackErr := database.Rollback(*tx)
		if rollbackErr != nil {
			logger.Log.Debug().Err(rollbackErr).Msg("Can't rollback during block disconnection")
			return
		}

		// No reason to prepare next attempt if this one is the last one.
		if attempt < maxAttempts-1 {
			// Preparing one more attempt.
			// Next attempt must be processed via its own transaction.
			tx, err = database.BeginTransaction()
			if err != nil {
				// `BeginTransaction` tries to handle errors internally.
				// If error is here then communication to the database can't be restored anymore.
				return tx, err
			}
		}
	}

	return tx, err
}

func (index *Index) rebuildChainSequence(tx pgx.Tx, currentBlockNumber, rebuildChainBlockNumber int64) (err error) {
	err = database.RemoveNextBlockNumberForProcessing(tx, currentBlockNumber, true)
	if err != nil {
		return err
	}
	for idx := currentBlockNumber - 1; idx > int64(rebuildChainBlockNumber); idx-- {
		err = database.AddNextBlockNumberForProcessing(tx, idx, false, false)
		if err != nil {
			return err
		}
	}
	err = database.AddNextBlockNumberForProcessing(tx, currentBlockNumber, true, false)
	return
}

func (index *Index) processOrphanBlock(tx *pgx.Tx, blockNumber, prevBlockNumber int64) (err error) {
	err = database.RemoveNextBlockNumberForProcessing(*tx, blockNumber, true)
	if err != nil {
		return
	}
	err = database.AddNextBlockNumberForProcessing(*tx, blockNumber+1, true, true)
	if err != nil {
		return
	}
	err = database.InsertBlockConnectingInfo(*tx, blockNumber, prevBlockNumber, ec.BlockStateDisconnected)
	return
}

func (index *Index) rebuildChainSequenceFromDisconnected(tx pgx.Tx, blockNumber, prevBlockNumber int64) (err error) {
	err = database.RemoveNextBlockNumberForProcessing(tx, blockNumber, true)
	if err != nil {
		return
	}
	err = index.moveToPrevBlockForChainSequenceRebuilding(tx, prevBlockNumber)
	if err != nil {
		return
	}
	err = database.AddNextBlockNumberForProcessing(tx, blockNumber, true, false)
	return
}

func (index *Index) moveToPrevBlockForChainSequenceRebuilding(tx pgx.Tx, blockNumber int64) (err error) {
	prevBlockNumber, err := database.GetPrevBlockNumber(tx, blockNumber)
	if err != nil {
		return
	}
	if prevBlockNumber < 0 {
		err = errors.New("invalid value of block number")
		return
	}
	prevBlockConnectingState, err := database.GetBlockConnectingState(tx, prevBlockNumber)
	if err != nil {
		return
	}
	if prevBlockConnectingState == ec.BlockStateDisconnected {
		err = index.moveToPrevBlockForChainSequenceRebuilding(tx, prevBlockNumber)
		if err != nil {
			return
		}
	}
	err = database.AddNextBlockNumberForProcessing(tx, blockNumber, true, false)
	return
}
