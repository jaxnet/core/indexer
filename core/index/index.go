package index

import (
	"github.com/jackc/pgx/v4"
	jsoniter "github.com/json-iterator/go"
	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/core/indexer/core"
	"gitlab.com/jaxnet/core/indexer/core/blocks_cache"
	"gitlab.com/jaxnet/core/indexer/core/clickhouse"
	"gitlab.com/jaxnet/core/indexer/core/database"
	"gitlab.com/jaxnet/core/indexer/core/ec"
	"gitlab.com/jaxnet/core/indexer/core/index/interfaces"
	"gitlab.com/jaxnet/core/indexer/core/logger"
	"gitlab.com/jaxnet/core/indexer/core/metrics"
	"gitlab.com/jaxnet/core/indexer/core/settings"

	"time"
)

type Index struct {
	RPCClient    *core.AbstractRPCClient
	CacheHandler *blocks_cache.CacheHandler
}

func New(impl core.IndexImplementation) (index *Index, err error) {

	rcpClient, err := impl.InitRPCClient()
	if err != nil {
		return
	}
	return &Index{
		RPCClient:    rcpClient,
		CacheHandler: blocks_cache.New(),
	}, nil
}

// Run begins to fetch latest_blocks from node and to index them.
func (index *Index) Run(impl core.IndexImplementation) (err error) {
	err = database.Init()
	if err != nil {
		return
	}

	err = impl.PrepareDatabase()
	if err != nil {
		return
	}

	err = index.initMetrics()
	if err != nil {
		return
	}

	if settings.Conf.ClickHouse != nil {
		err = clickhouse.Init()
		if err != nil {
			return
		}
	}

	if settings.Conf.Mode == settings.ModeHttp || settings.Conf.Mode == settings.ModeIndexAndHttp {
		go func() {
			err = interfaces.HandleHttpInterface(impl)
			if err != nil {
				// This method by default must never return error.
				// All HTTP-related errors must be processed by the internal errors handler (written to the log).
				// Ideally, indexer should never be stopped, but for the case if error is already here -
				// the panic is present, because it is going to be serious.
				panic(err)
			}
		}()
	}

	if settings.Conf.Mode != settings.ModeIndex && settings.Conf.Mode != settings.ModeIndexAndHttp {
		_ = <-make(chan struct{}) // Hang goroutine.
	}

	if index.CacheHandler != nil {
		go index.CacheHandler.DeleteOutdatedCache()
	}

	for {
		//// There cases when network begins to generate huge amount of blocks one by one.
		//// It is possible during sync and/or during huge mining hash rate increasing.
		//// In this cases indexes are going wild, and dou to the huge data processing flow
		//// http interface is going to hang (potentially seriously).
		//// To prevent it - some minimal round delay added.
		//const minRoundDelay = time.Millisecond * 100
		//time.Sleep(minRoundDelay)

		var (
			tx                      *pgx.Tx
			block                   *core.AbstractMsgBlock
			blockNumber             int64
			prevBlockNumber         int64
			blockHeight             int64
			rebuildChainBlockNumber int64
		)

		reportErrAndWait := func(err error) {
			logger.Log.Err(err).Msg("One iteration error")
			err = database.Rollback(*tx)
			if err != nil {
				logger.Log.Err(err).Msg("Can't rollback transaction")
			}
			time.Sleep(time.Millisecond * 500)
			return
		}

		// Fetching block mechanism uses database too.
		// It is decided to create one common TX for fetching and storing mechanics.
		// Doing so allows to use less transactions and significantly speedup latest_blocks processing.
		tx, err = database.BeginTransaction()
		if err != nil {
			// `BeginTransaction` tries to handle errors internally.
			// If error is here then communication to the database can't be restored any more.
			return
		}

		blockNumber, forward, checkPrevBlock, err := database.GetNextBlockNumberForProcessing(*tx, impl)
		if err != nil {
			reportErrAndWait(err)
			continue
		}
		logger.Log.Debug().Int64("block-number", blockNumber).Bool("forward", forward).Msg("Try to get next block")

		// connecting
		if forward {

			block, prevBlockNumber, blockHeight, rebuildChainBlockNumber, err = index.waitUntilNextBlockIsAvailable(
				*tx, blockNumber, checkPrevBlock, impl)
			if err != nil {
				reportErrAndWait(err)
				continue
			}

			if blockHeight == -1 {
				logger.Log.Debug().Msg("Block is orphan")
				err = index.processOrphanBlock(tx, blockNumber, prevBlockNumber)
				if err != nil {
					reportErrAndWait(err)
					continue
				}
			} else {
				if block == nil {
					logger.Log.Debug().Int64("block-num", blockNumber).Int64(
						"block-height", blockHeight).Int64(
						"rebuild-block-num", rebuildChainBlockNumber).Msg(
						"Chain rebuilding")
					// chain was rebuilt
					err = index.rebuildChainSequence(*tx, blockNumber, rebuildChainBlockNumber)
					if err != nil {
						reportErrAndWait(err)
						continue
					}
				} else {
					logger.Log.Debug().Int64("block-num", blockNumber).Int64(
						"block-height", blockHeight).Int64(
						"rebuild-block-num", rebuildChainBlockNumber).Int("tx-cnt", block.TxCnt()).Msg(
						"Next block received")

					if prevBlockNumber <= impl.DefaultFirstBlockForProcessing() {
						tx, err = index.processNextBlockConnecting(tx, block, blockHeight, blockNumber, prevBlockNumber, impl)
						if err != nil {
							reportErrAndWait(err)
							continue
						}
					} else {

						prevBlockState, err := database.GetBlockConnectingState(*tx, prevBlockNumber)
						if err != nil {
							reportErrAndWait(err)
							continue
						}

						if prevBlockState == ec.BlockStateDisconnected {
							logger.Log.Debug().Int64("prev-block-num", prevBlockNumber).Msg(
								"Previous block marked as disconnected. Start rebuilding chain")

							err = index.rebuildChainSequenceFromDisconnected(*tx, blockNumber, prevBlockNumber)
							if err != nil {
								reportErrAndWait(err)
								continue
							}

						} else {
							tx, err = index.processNextBlockConnecting(tx, block, blockHeight, blockNumber, prevBlockNumber, impl)
							if err != nil {
								reportErrAndWait(err)
								continue
							}
						}
					}
				}
			}
			// disconnecting
		} else {
			logger.Log.Debug().Int64(
				"block-num", blockNumber).Msg(
				"Disconnecting block")

			state, err := database.GetBlockConnectingState(*tx, blockNumber)
			if err != nil {
				reportErrAndWait(err)
				continue
			}
			if state == ec.BlockStateDisconnected {
				logger.Log.Debug().Msg("Block already disconnected")
				err = database.RemoveNextBlockNumberForProcessing(*tx, blockNumber, false)
				if err != nil {
					reportErrAndWait(err)
					continue
				}
			} else {

				block, prevBlockNumber, blockHeight, rebuildChainBlockNumber, err = index.waitUntilNextBlockIsAvailable(
					*tx, blockNumber, checkPrevBlock, impl)
				if err != nil {
					reportErrAndWait(err)
					continue
				}
				logger.Log.Debug().Int64("block-num", blockNumber).Msg(
					"Block-candidate for disconnection received")

				processingStartTime := time.Now()
				{
					tx, err = index.processNextBlockDisconnecting(tx, block, blockHeight, blockNumber, impl)
					if err != nil {
						reportErrAndWait(err)
						continue
					}
				}
				processingEndTime := time.Now()
				timeDelta := processingEndTime.Sub(processingStartTime)
				logger.Log.Debug().Str("duration", timeDelta.String()).Msg("Block disconnecting time")
				logger.Log.Debug().Int64("block-num", blockNumber).Msg("Block disconnected")

				err = database.UpdateBlockConnectingState(*tx, blockNumber, ec.BlockStateDisconnected)
				if err != nil {
					reportErrAndWait(err)
					continue
				}
				metrics.GaugeSet(ec.MetricLastBlockDisconnected, float64(blockHeight))
				metrics.HistogramObserve(ec.MetricBlockDisconnectingTime, timeDelta.Seconds())
			}
		}

		err = database.Commit(*tx)
		if err != nil {
			panic(err)
		}

		if settings.Conf.BlockProcessingNotification != "" {
			NotifyAboutBlockProcessing(blockNumber)
		}

		// Note:
		// No need to call Commit() / Rollback() here.
		// `ProcessNextBlock()` handles this.
	}
}

// Stop stops the index and frees all the resources.
// It is expected that index's process would be stopped as well after calling this method.
func (index *Index) Stop() {
	database.Free()

	if settings.Conf.ClickHouse != nil {
		clickhouse.Free()
	}
}

func (index *Index) initMetrics() (err error) {
	var (
		blocksProcessingHistogramBuckets = []float64{0.25, 0.5, 0.75, 1, 1.5, 2, 5, 10, 20, 50, 100}
	)

	err = metrics.InitGauge(settings.Conf.Name, ec.MetricLastBlockConnected, "")
	if err != nil {
		return
	}

	err = metrics.InitGauge(settings.Conf.Name, ec.MetricLastBlockDisconnected, "")
	if err != nil {
		return
	}

	err = metrics.InitHistogram(settings.Conf.Name, ec.MetricBlockConnectingTime, "", blocksProcessingHistogramBuckets)
	if err != nil {
		return
	}

	err = metrics.InitHistogram(settings.Conf.Name, ec.MetricBlockDisconnectingTime, "", blocksProcessingHistogramBuckets)
	if err != nil {
		return
	}

	//
	// Other metrics goes here.
	//

	return
}

type NotificationRequestBody struct {
	ShardId     uint32 `json:"shardId"`
	BlockNumber int64  `json:"blockNumber"`
}

func NotifyAboutBlockProcessing(blockNumber int64) {

	req := fasthttp.AcquireRequest()
	req.SetRequestURI(settings.Conf.BlockProcessingNotification)
	req.Header.SetMethod("DELETE")
	notificationRequestBody := NotificationRequestBody{
		ShardId:     settings.Conf.Blockchain.ID,
		BlockNumber: blockNumber,
	}
	data, err := jsoniter.Marshal(notificationRequestBody)
	if err != nil {
		logger.Log.Debug().Err(err).Msg("can't marshal notification request body")
		return
	}
	req.SetBody(data)

	resp := fasthttp.AcquireResponse()
	client := &fasthttp.Client{}
	err = client.Do(req, resp)
	if err != nil {
		logger.Log.Info().Err(err).Msg("can't send notification")
		return
	}
	if resp.StatusCode() != 200 {
		logger.Log.Info().Int("status-code", resp.StatusCode()).Msg("invalid notification response")
	}
}
