package utils

import "time"

func YesterdayBegin() time.Time {
	y := time.Now().UTC().Add(-1 * time.Hour * 24)
	return time.Date(y.Year(), y.Month(), y.Day(), 0, 0, 1, 0, y.Location())
}

func TodayEnd() time.Time {
	y := time.Now().UTC()
	return time.Date(y.Year(), y.Month(), y.Day(), 23, 59, 59, 0, y.Location())
}

func ToIndexInternalTime(in time.Time) (out time.Time) {
	out = in.UTC()
	return
}
