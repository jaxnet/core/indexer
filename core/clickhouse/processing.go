package clickhouse

import "gitlab.com/jaxnet/core/indexer/core/settings"

func DataTableName() string {
	return settings.Conf.ClickHouse.TableName
}
