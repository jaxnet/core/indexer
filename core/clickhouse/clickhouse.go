package clickhouse

import (
	"database/sql"
	"gitlab.com/jaxnet/core/indexer/core/settings"
	"time"

	_ "github.com/ClickHouse/clickhouse-go"
)

var (
	CH *sql.DB
)

func Init() (err error) {
	CH, err = sql.Open("clickhouse", settings.Conf.ClickHouse.ConnectionCredentials())
	if err != nil {
		return
	}

	err = CH.Ping()
	if err != nil {
		return
	}

	err = EnsureSchema()
	return
}

func Free() {
	if CH != nil {
		_ = CH.Close()
	}
}

// BeginOperation is an analogue of BeginTransaction from database package.
// Click House does not supports transactions (in terms of classic ACID transactions of PostgreSQL),
// so the name of this method has been changed to not confuse developers.
//
// Clickhouse client tries to stay compliant with sql.DB interface, so it must define a Transaction's interface,
// but this transactions are not ACID.
func BeginOperation() (tx *sql.Tx, err error) {
	const maxAttempts = 10

	for attempt := 0; attempt < maxAttempts; attempt++ {
		tx, err = CH.Begin()
		if err == nil {
			return
		}

		// If error - sleep a bit and try once more.
		time.Sleep(time.Second * 1)
	}

	return
}
