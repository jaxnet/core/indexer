package clickhouse

import (
	"strings"
	"time"
)

func DropTZInfo(t time.Time) string {
	return strings.TrimSuffix(t.Format(time.RFC3339), "Z")
}
