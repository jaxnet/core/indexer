package core

import (
	"errors"
	btcclient "github.com/btcsuite/btcd/rpcclient"
	jaxclient "gitlab.com/jaxnet/jaxnetd/network/rpcclient"
)

type AbstractRPCClient struct {
	client interface{}
}

func Init(client interface{}) (abstractRPCClient *AbstractRPCClient) {

	//todo : check type of client
	abstractRPCClient = &AbstractRPCClient{
		client: client,
	}
	return
}

func (arc AbstractRPCClient) JaxRPCClient() (client *jaxclient.Client, err error) {
	client, ok := arc.client.(*jaxclient.Client)
	if !ok {
		err = errors.New("can't cast jax client")
		return
	}
	return
}

func (arc AbstractRPCClient) BTCRPCClient() (client *btcclient.Client, err error) {
	client, ok := arc.client.(*btcclient.Client)
	if !ok {
		err = errors.New("can't cast jax client")
		return
	}
	return
}
