package core

import (
	"errors"
	btcwire "github.com/btcsuite/btcd/wire"
	jaxwire "gitlab.com/jaxnet/jaxnetd/types/wire"
)

type AbstractMsgBlock struct {
	block interface{}
}

func InitBlock(block interface{}) (abstractMsgBlock *AbstractMsgBlock) {

	//todo : check type of client
	abstractMsgBlock = &AbstractMsgBlock{
		block: block,
	}
	return
}

func (amb AbstractMsgBlock) JaxMsgBlock() (block *jaxwire.MsgBlock, err error) {
	block, ok := amb.block.(*jaxwire.MsgBlock)
	if !ok {
		err = errors.New("Can't cast jax MsgBlock")
		return
	}
	return
}

func (amb AbstractMsgBlock) BTCMsgBlock() (block *btcwire.MsgBlock, err error) {
	block, ok := amb.block.(*btcwire.MsgBlock)
	if !ok {
		err = errors.New("Can't cast btc MsgBlock")
		return
	}
	return
}

func (amb AbstractMsgBlock) BlockHash() string {
	switch amb.block.(type) {
	case *jaxwire.MsgBlock:
		{
			return amb.block.(*jaxwire.MsgBlock).BlockHash().String()
		}
	case *btcwire.MsgBlock:
		{
			return amb.block.(*btcwire.MsgBlock).BlockHash().String()
		}
	// todo : correct parsing default
	default:
		return ""
	}
}

func (amb AbstractMsgBlock) TxCnt() int {
	switch amb.block.(type) {
	case *jaxwire.MsgBlock:
		{
			return len(amb.block.(*jaxwire.MsgBlock).Transactions)
		}
	case *btcwire.MsgBlock:
		{
			return len(amb.block.(*btcwire.MsgBlock).Transactions)
		}
	// todo : correct parsing default
	default:
		return 0
	}
}
