package core

import (
	"github.com/fasthttp/router"
	"github.com/jackc/pgx/v4"
	"gitlab.com/jaxnet/core/indexer/core/blocks_cache"
)

type IndexImplementation interface {
	InitRPCClient() (*AbstractRPCClient, error)

	//
	// Blocks processing
	//

	FetchNextBlockFromNode(offset int64, checkPrevBlock bool, rpcClient *AbstractRPCClient, cacheHandler *blocks_cache.CacheHandler) (
		*AbstractMsgBlock, int64, int64, int64, error)

	// ConnectBlock is called each time new block arrives to the indexer.
	ConnectBlock(tx pgx.Tx, block *AbstractMsgBlock, blockHeight int64, rpcClient *AbstractRPCClient) error

	// DisconnectBlock is called each time some block is disconnected from the main chain,
	// and must be reverted from the index also.
	DisconnectBlock(tx pgx.Tx, block *AbstractMsgBlock, blockHeight int64, rpcClient *AbstractRPCClient) error

	//
	// HTTP Interface
	//

	// SetHttpHandlers allows custom index to specify API handlers for it's own.
	SetHttpHandlers(group *router.Group)

	// DefaultFirstBlockForProcessing returns the serial ID of the first block from which indexing should be started.
	DefaultFirstBlockForProcessing() int64

	// PrepareDatabase makes some preliminary actions with the database before blocks processing begins,
	// e.g. initialises current block height table for UTXOs indexer in case of the first start.
	// (For the details, please see the docs for this method from the UTXOs index implementation)
	PrepareDatabase() error
}
