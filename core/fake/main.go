package fake

import (
	"fmt"
	"github.com/rs/zerolog/log"
	"gitlab.com/jaxnet/core/indexer/core"
	"gitlab.com/jaxnet/core/indexer/core/ec"
	"gitlab.com/jaxnet/core/indexer/core/index"
	"gitlab.com/jaxnet/core/indexer/core/logger"
	"gitlab.com/jaxnet/core/indexer/core/metrics"
	"gitlab.com/jaxnet/core/indexer/core/settings"
)

func Main(jaxImpl core.IndexImplementation, btcImpl core.IndexImplementation) {
	err := settings.Init()
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	logger.Init()
	metrics.Init()

	if settings.Conf.Blockchain.ID == ec.BTCShardID {
		err = runIndex(btcImpl)
	} else {
		err = runIndex(jaxImpl)
	}
	if err != nil {
		logger.Err(err)
	}
}

func runIndex(impl core.IndexImplementation) (err error) {
	idx, err := index.New(impl)
	if err != nil {
		return
	}

	err = idx.Run(impl)
	if err != nil {
		log.Err(err).Msg("Can't start index")
		idx.Stop()
	}
	return
}
