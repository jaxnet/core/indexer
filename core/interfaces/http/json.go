package http

import (
	jsoniter "github.com/json-iterator/go"
	"github.com/valyala/fasthttp"
)

func WriteJSONResponse(response interface{}, ctx *fasthttp.RequestCtx) (err error) {
	data, err := jsoniter.Marshal(response)
	if err != nil {
		return
	}

	_, err = ctx.Write(data)
	if err != nil {
		return
	}

	ctx.Response.Header.SetContentType("application/json")
	return
}
