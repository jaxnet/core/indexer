package http

import (
	"fmt"
	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/core/indexer/core/logger"
	"math/rand"
)

// DiscloseError is used for the cases when error can be shown to the caller.
func DiscloseError(err error, ctx *fasthttp.RequestCtx, status int) {
	errorID := rand.Int31()

	logger.Err(err).Int32(
		"error-id", errorID).Msg(
		"error occurred during processing http request")

	ctx.Error(
		fmt.Sprint(err.Error(), " (id=", errorID, ")"), status)
}

// HandleErrorAndHideDetails is used in the cases when error must be logged but no details must be shared with user.
func HandleErrorAndHideDetails(err error, ctx *fasthttp.RequestCtx) {
	errorID := rand.Int31()

	logger.Err(err).Int32(
		"error-id", errorID).Msg(
		"error occurred during processing http request")

	ctx.Error(
		fmt.Sprint("Unexpected error occurred (id=", errorID, ")"),
		fasthttp.StatusInternalServerError)
}

func EstablishHandlerWideErrorsProcessing(err *error, ctx *fasthttp.RequestCtx) {
	if *err != nil {
		HandleErrorAndHideDetails(*err, ctx)
	}
}

func EstablishUserVisibleErrorsProcessing(err *error, ctx *fasthttp.RequestCtx) {
	if *err != nil {
		DiscloseError(*err, ctx, fasthttp.StatusBadRequest)
	}
}
