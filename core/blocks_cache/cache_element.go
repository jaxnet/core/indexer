package blocks_cache

import (
	btcclient "github.com/btcsuite/btcd/rpcclient"
	jaxclient "gitlab.com/jaxnet/jaxnetd/network/rpcclient"
	"time"
)

type JaxCacheElement struct {
	BlockResult *jaxclient.BlockResult
	TimeAdded   time.Time
}

type BtcCacheElement struct {
	BlockResult *btcclient.BlockResult
	TimeAdded   time.Time
}
