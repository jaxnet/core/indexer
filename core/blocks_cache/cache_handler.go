package blocks_cache

import (
	"errors"
	btcclient "github.com/btcsuite/btcd/rpcclient"
	"github.com/rs/zerolog/log"
	"gitlab.com/jaxnet/core/indexer/core/logger"
	"gitlab.com/jaxnet/core/indexer/core/settings"
	jaxclient "gitlab.com/jaxnet/jaxnetd/network/rpcclient"
	"sync"
	"time"
)

type CacheHandler struct {
	jaxBlocksCache        map[int64]JaxCacheElement
	btcBlocksCache        map[int64]BtcCacheElement
	mutex                 sync.Mutex
	CountBlocksPerRequest int
	ttlElementInSec       int
	updatingPeriodInSec   int
}

func New() (handler *CacheHandler) {
	if settings.Conf.BlocksCache == nil {
		// Metrics config missed, disable metrics.
		log.Debug().Msg("Blocks cache config is missed, caching disabled")
		return
	}
	return &CacheHandler{
		jaxBlocksCache:        make(map[int64]JaxCacheElement),
		btcBlocksCache:        make(map[int64]BtcCacheElement),
		CountBlocksPerRequest: settings.Conf.BlocksCache.CountBlocksPerRequest,
		ttlElementInSec:       settings.Conf.BlocksCache.TTLInSeconds,
		updatingPeriodInSec:   settings.Conf.BlocksCache.UpdatingPeriodInSeconds,
	}
}

func (h *CacheHandler) AddJaxBlock(serialId int64, blockResult *jaxclient.BlockResult) {
	h.mutex.Lock()
	defer h.mutex.Unlock()
	h.jaxBlocksCache[serialId] = JaxCacheElement{
		BlockResult: blockResult,
		TimeAdded:   time.Now(),
	}
}

func (h *CacheHandler) GetJaxBlock(serialId int64) (blockResult *jaxclient.BlockResult, err error) {
	br, ok := h.jaxBlocksCache[serialId]
	if !ok {
		err = errors.New("there are no block with such id")
	}
	blockResult = br.BlockResult
	return
}

func (h *CacheHandler) AddBtcBlock(serialId int64, blockResult *btcclient.BlockResult) {
	h.mutex.Lock()
	defer h.mutex.Unlock()
	h.btcBlocksCache[serialId] = BtcCacheElement{
		BlockResult: blockResult,
		TimeAdded:   time.Now(),
	}
}

func (h *CacheHandler) GetBtcBlock(serialId int64) (blockResult *btcclient.BlockResult, err error) {
	br, ok := h.btcBlocksCache[serialId]
	if !ok {
		err = errors.New("there are no block with such id")
	}
	blockResult = br.BlockResult
	return
}

func (h *CacheHandler) DeleteOutdatedCache() {
	for {
		h.mutex.Lock()
		for key, value := range h.jaxBlocksCache {
			if value.TimeAdded.Add(time.Second * time.Duration(h.ttlElementInSec)).Before(time.Now()) {
				delete(h.jaxBlocksCache, key)
			}
		}

		for key, value := range h.btcBlocksCache {
			if value.TimeAdded.Add(time.Second * time.Duration(h.ttlElementInSec)).Before(time.Now()) {
				delete(h.btcBlocksCache, key)
			}
		}

		logger.Log.Debug().Int("size", len(h.jaxBlocksCache)).Msg("JAX cache")
		logger.Log.Debug().Int("size", len(h.btcBlocksCache)).Msg("BTC cache")
		h.mutex.Unlock()
		time.Sleep(time.Second * time.Duration(h.updatingPeriodInSec))
	}
}
