package ec

const (
	BTCShardID            = 4294967295
	BeaconShardID         = 0
	CrossShardTxVersion   = 4
	EmptyHash             = "0000000000000000000000000000000000000000000000000000000000000000"
	JaxBlockNotFoundError = "-5: Block not found - "
	JaxChainNotFoundError = "-32603: chain serial id is empty or invalid -"
	BtcBlockNotFoundError = "-32600: Invalid request: malformed"

	BlockStateConnected            = 0
	BlockStateDisconnected         = 1
	BlockConnectingStateRecordsCnt = 1000
)

// WARN: Metrics names must contain only underscores, and no dashes.
const (
	MetricLastBlockConnected     = "last_block_connected"
	MetricBlockConnectingTime    = "block_connecting_time"
	MetricLastBlockDisconnected  = "last_block_disconnected"
	MetricBlockDisconnectingTime = "block_disconnecting_time"
)
