package ec

import "errors"

var (
	ErrValidation       = errors.New("validation error")
	ErrNoTxCanBeCreated = errors.New("tx cant be created")

	//
	// Metrics
	//
	ErrMetricIsUndefined = errors.New("metric is undefined")
	ErrNoRecordsInDB     = errors.New("there are no records in table")
)
