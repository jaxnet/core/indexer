package settings

import (
	"flag"
	"gitlab.com/jaxnet/jaxnetd/types/chaincfg"
	"gopkg.in/yaml.v3"
	"io/ioutil"
)

const (
	ModeHttp         = "http"
	ModeIndex        = "index"
	ModeIndexAndHttp = "index-and-http"
)

type ShardConfig struct {
	NetworkName                 string    `yaml:"network-name"`
	ID                          uint32    `yaml:"shard-id"`
	RPC                         RPCConfig `yaml:",inline"`
	BlocksFetchingPeriodInSec   int       `yaml:"blocks-fetching-period-in-sec"`
	NewBlockFetchingPeriodInSec int       `yaml:"new-blocks-fetching-period-in-sec"`
}

type Settings struct {
	Debug                       bool              `yaml:"debug"`
	Mode                        string            `yaml:"mode"`
	WriteLogsIntoFile           bool              `yaml:"write-logs-into-file"`
	Name                        string            `yaml:"name"`
	Http                        HTTPConfig        `yaml:"http"`
	DB                          DBConfig          `yaml:"database"`
	ClickHouse                  *ClickHouseConfig `yaml:"click-house,omitempty"`
	Blockchain                  ShardConfig       `yaml:"blockchain"`
	CntRecordsSavedInDB         uint32            `yaml:"count-records-saved-in-db,omitempty"`
	CntRecordsInResponse        uint32            `yaml:"count-records-in-response,omitempty"`
	Metrics                     *MetricsConfig    `yaml:"metrics,omitempty"`
	BlockProcessingNotification string            `yaml:"block-processing-notification-url"`
	BlocksCache                 *CacheConfig      `yaml:"blocks-cache"`
}

var (
	Conf      *Settings
	NetParams *chaincfg.Params
)

func Init() (err error) {
	path := flag.String("config", "conf.yaml", "path to configuration file (must end with conf.yaml)")
	flag.Parse()

	data, err := ioutil.ReadFile(*path)
	if err != nil {
		return
	}

	Conf = &Settings{}
	err = yaml.Unmarshal(data, Conf)
	if err != nil {
		return
	}

	applyDefault(Conf)

	err = validate(Conf)
	if err != nil {
		return
	}

	if Conf.Blockchain.NetworkName == "fastnet" {
		NetParams = &chaincfg.FastNetParams
	} else if Conf.Blockchain.NetworkName == "testnet" {
		NetParams = &chaincfg.TestNet3Params
	} else if Conf.Blockchain.NetworkName == "testnet3" {
		NetParams = &chaincfg.TestNet3Params
	} else if Conf.Blockchain.NetworkName == "mainnet" {
		NetParams = &chaincfg.MainNetParams
	} else {
		panic("Hmm.. I know nothing about other test networks than 'testnet' and 'fastnet'(")
	}
	return
}
