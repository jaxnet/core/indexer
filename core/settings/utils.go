package settings

import "fmt"

type InterfaceConfig struct {
	Host string `yaml:"host"`
	Port uint16 `yaml:"port"`
}

func (i InterfaceConfig) Interface() string {
	return fmt.Sprint(i.Host, ":", i.Port)
}

type CredentialsConfig struct {
	User string `yaml:"user"`
	Pass string `yaml:"pass"`
}

type DBConfig struct {
	InterfaceConfig   `yaml:",inline"`
	CredentialsConfig `yaml:",inline"`
	DatabaseName      string `yaml:"database-name"`
	ValidateSchema    bool   `yaml:"validate-schema"`
}

func (c DBConfig) PSQLConnectionCredentials() string {
	return fmt.Sprint("postgres://", c.User, ":", c.Pass, "@", c.Host, ":", c.Port, "/", c.DatabaseName)
}

type HTTPConfig struct {
	InterfaceConfig `yaml:",inline"`
	ApiPrefix       string `yaml:"api-prefix"`
}

type RPCConfig struct {
	Net         InterfaceConfig   `yaml:"net"`
	Credentials CredentialsConfig `yaml:"credentials"`
}

type ClickHouseConfig struct {
	InterfaceConfig   `yaml:",inline"`
	CredentialsConfig `yaml:",inline"`
	TableName         string `yaml:"table-name"`
}

type CacheConfig struct {
	CountBlocksPerRequest   int `yaml:"count-blocks-per-request"`
	TTLInSeconds            int `yaml:"ttl-in-seconds"`
	UpdatingPeriodInSeconds int `yaml:"updating-period-in-seconds"`
}

func (c ClickHouseConfig) ConnectionCredentials() string {
	return fmt.Sprintf("tcp://%s:%d?username=%s&password=%s", c.Host, c.Port, c.User, c.Pass)
}

type MetricsConfig struct {
	InterfaceConfig `yaml:",inline"`
	Namespace       string
}
