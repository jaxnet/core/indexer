package settings

import (
	"fmt"
	"gitlab.com/jaxnet/core/indexer/core/ec"
)

// validate validates loaded settings.
func validate(settings *Settings) (err error) {
	if settings.Name == "" {
		return fmt.Errorf("index name could not be empty: %w", ec.ErrValidation)
	}

	if settings.Name[0] == ' ' ||
		settings.Name[0] == '0' ||
		settings.Name[0] == '1' ||
		settings.Name[0] == '2' ||
		settings.Name[0] == '3' ||
		settings.Name[0] == '4' ||
		settings.Name[0] == '5' ||
		settings.Name[0] == '6' ||
		settings.Name[0] == '7' ||
		settings.Name[0] == '8' ||
		settings.Name[0] == '9' {
		return fmt.Errorf("index name could not begin with space or digit: %w", ec.ErrValidation)
	}

	//
	// HTTP Interface
	err = validateInterface(settings.Http.InterfaceConfig)
	if err != nil {
		err = fmt.Errorf("invalid interface config set for htpp.*")
		return
	}
	if settings.Http.ApiPrefix == "" {
		return fmt.Errorf("http.api-prefix could not be empty: %w", ec.ErrValidation)
	}

	//
	// Database
	err = validateInterface(settings.DB.InterfaceConfig)
	if err != nil {
		err = fmt.Errorf("invalid interface config set for database.*")
		return
	}
	if settings.DB.User == "" {
		return fmt.Errorf("database.user could not be empty: %w", ec.ErrValidation)
	}

	//
	// Clickhouse
	if settings.ClickHouse != nil {
		if settings.ClickHouse.TableName == "" {
			return fmt.Errorf("clickhouse.table-name could not be empty")
		}
	}

	//
	// Execution mode
	if settings.Mode != ModeIndex &&
		settings.Mode != ModeHttp &&
		settings.Mode != ModeIndexAndHttp {

		err = fmt.Errorf(
			"invalid execution mode, available options are (`%s`, `%s` or `%s`): %w",
			ModeIndex, ModeHttp, ModeIndexAndHttp, ec.ErrValidation)
	}

	//
	// Clickhouse
	if settings.ClickHouse != nil {
		err = validateInterface(settings.ClickHouse.InterfaceConfig)
		if err != nil {
			err = fmt.Errorf("invalid interface config set for click-house.*")
			return
		}

		if settings.ClickHouse.TableName == "" {
			return fmt.Errorf("click-house.data-table could not be empty")
		}
	}

	//
	// Metrics
	if settings.Metrics != nil {
		err = validateInterface(settings.Metrics.InterfaceConfig)
		if err != nil {
			err = fmt.Errorf("invalid interface config set for metrics.*")
			return
		}

		if settings.Metrics.Namespace == "" {
			return fmt.Errorf("metrics.namespace could not be empty")
		}
	}

	return
}

func validateInterface(i InterfaceConfig) (err error) {
	if i.Host == "" {
		return fmt.Errorf("host could not be empty: %w", ec.ErrValidation)
	}

	if i.Port == 0 {
		return fmt.Errorf("port could not be empty: %w", ec.ErrValidation)
	}

	return
}
