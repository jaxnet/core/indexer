package settings

import (
	"fmt"
	"github.com/rs/zerolog/log"
)

func applyDefault(settings *Settings) {
	if settings.Mode == "" {
		settings.Mode = ModeIndexAndHttp
		log.Warn().Msg(
			fmt.Sprintf(
				"No value set for configuration parameter `mode`, `%s` applied by default.",
				ModeIndexAndHttp))
	}
}
