package metrics

import (
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/zerolog/log"
	"gitlab.com/jaxnet/core/indexer/core/ec"
	"gitlab.com/jaxnet/core/indexer/core/logger"
	"gitlab.com/jaxnet/core/indexer/core/settings"
	"net/http"
)

var (
	metricsEnabled = false
	gauges         = map[string]prometheus.Gauge{}
	histograms     = map[string]prometheus.Histogram{}
)

func Init() {
	if settings.Conf.Metrics == nil {
		// Metrics config missed, disable metrics.
		log.Warn().Msg("Metrics config is missed, metrics handling disabled")
		return
	}

	go func() {
		metricsEnabled = true
		host := settings.Conf.Metrics.Interface()

		// By default, prometheus expects to be used with the standard http  server provided by the golang library.
		// Custom server is used here to prevent potential collisions with another usages of standard http server.
		server := &http.Server{
			Addr:    host,
			Handler: promhttp.Handler(),
		}

		logger.Log.Info().Str(
			"host", host).Str(
			"endpoint", "/metrics").Msg(
			"Metrics server started")

		err := server.ListenAndServe()
		if err != nil {
			err = fmt.Errorf("metrics server failed: %w", err)
			panic(err)
		}
	}()
}

func InitGauge(subsystem, metricName, help string) (err error) {
	if metricsEnabled == false {
		return
	}

	_, isPresent := gauges[metricName]
	if isPresent {
		err = fmt.Errorf("can't init metric `%s` for subsystem `%s`: already initialised", metricName, subsystem)
		return
	}

	gauge := prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: settings.Conf.Metrics.Namespace,
		Subsystem: subsystem,
		Name:      metricName,
		Help:      help,
	})

	gauges[metricName] = gauge

	err = prometheus.Register(gauge)
	if err != nil {
		err = fmt.Errorf("can't init metric `%s` for subsystem `%s`: %w", metricName, subsystem, err)
		return
	}

	return
}

func GaugeSet(metricName string, value float64) {
	if metricsEnabled == false {
		return
	}

	gauge, isPresent := gauges[metricName]
	if !isPresent {
		log.Err(ec.ErrMetricIsUndefined).Msg("Attempt to write undefined metric")
		return
	}

	gauge.Set(value)
}

func InitHistogram(subsystem, metricName, help string, buckets []float64) (err error) {
	if metricsEnabled == false {
		return
	}

	_, isPresent := histograms[metricName]
	if isPresent {
		err = fmt.Errorf("can't init metric `%s` for subsystem `%s`: already initialised", metricName, subsystem)
		return
	}

	histogram := prometheus.NewHistogram(prometheus.HistogramOpts{
		Namespace: settings.Conf.Metrics.Namespace,
		Subsystem: subsystem,
		Name:      metricName,
		Help:      help,
		Buckets:   buckets,
	})

	histograms[metricName] = histogram

	err = prometheus.Register(histogram)
	if err != nil {
		err = fmt.Errorf("can't init metric `%s` for subsystem `%s`: %w", metricName, subsystem, err)
		panic(err)
	}

	return
}

func HistogramObserve(metricName string, value float64) {
	if metricsEnabled == false {
		return
	}

	histogram, isPresent := histograms[metricName]
	if !isPresent {
		log.Err(ec.ErrMetricIsUndefined).Msg("Attempt to write undefined metric")
		return
	}

	histogram.Observe(value)
}
