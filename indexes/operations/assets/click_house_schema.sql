CREATE TABLE IF NOT EXISTS {{ index_data_table }}
(
    hash FixedString(64),
    blockNumber UInt64,
    originTxHash FixedString(64),
    txIndex UInt32,
    shardID UInt32,
    address String,
    amount Int64,
    timestamp DateTime,
    isIncoming Bool, -- "false" == TX is outgoing
    isCrossShard Bool

    INDEX hash_bloom_index hash type bloom_filter(0.00001) granularity 4,
    INDEX multisig_address_bloom_index startsWith(address, '2') type bloom_filter(0.00001) granularity 4

) ENGINE = MergeTree()
    PRIMARY KEY (address, hash, txIndex, isIncoming)
    PARTITION BY shardID
    ORDER BY (address, hash, txIndex, isIncoming);

-- WARN:
-- Please, do not use buffer table here due to the limitations with DELETE operations.
-- Buffer engine does not support DELETE mutations, so there is no way to delete orphans from buffer.
-- The only way to do it is to flush buffer to the main table and then to delete required records from there,
-- but this operation is very inefficient and (more important) is not quarantined to be executed correctly
-- (click house could still flush buffer partially,
-- see: https://clickhouse.tech/docs/ru/sql-reference/statements/optimize/ for the details).
--
-- According to the docs, buffer would be totally flushed in case when it is going to be dropped,
-- but dropping and then re-creating of the buffer each time when orphan block is processed seems to be
-- ridiculously inefficient.
--
-- So that's why it is decided to keep operations without buffer before it.