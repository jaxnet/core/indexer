
-- WARN: This section contains tables that are common
-- (expected to be used by all indexes, that would share common database instance)
-- Because this tables are common, it is expected, that they would have the same schema in all indexes.
--
-- This tables is declared in the same file, that defines the schema for the particular index.
-- It is safe to write this declaration here: common tables would not be replaced if some of them is already present,
-- so no data corruption or loss is possible.
create table if not exists hashes
(
    id bigserial primary key,
    hash text unique
);

-- END OF COMMON TABLES SECTION

create table if not exists {{ index_blocks_for_processing_table }}
(
    id bigserial,
    block_number bigint,
    forward bool,
    check_prev_block bool
);

create table if not exists {{ index_processed_blocks_table }}
(
    hash bigint unique
    -- NOTE: No additional index is needed here.
    --       `unique` already instructs postgres to create one.
);

create table if not exists {{ index_blocks_connecting_state }} (
    serial_id bigint,
    prev_serial_id bigint,
    -- 0 - connected, 1 - disconnected
    state smallint
);