package interfaces

import (
	"errors"
	"fmt"
	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/core/indexer/core/clickhouse"
	"gitlab.com/jaxnet/core/indexer/core/ec"
	"gitlab.com/jaxnet/core/indexer/core/interfaces/http"
	"gitlab.com/jaxnet/core/indexer/core/logger"
	"gitlab.com/jaxnet/core/indexer/core/utils"
	"strconv"
	"time"
)

const (
	DirectionOutgoing = "out"
	DirectionIncoming = "in"
	DirectionLoop     = "loop"
)

type nearestDates struct {
	Next     *time.Time `json:"next"`
	Previous *time.Time `json:"previous"`
}

type historyData struct {
	Records      []interface{} `json:"records"`
	NearestDates nearestDates  `json:"nearestDates"`
}

type HistoryResponse struct {
	Data historyData `json:"data"`
}

type txHeaders struct {
	Hash      string     `json:"hash,omitempty"`      // Could be empty in cross shard TXs
	Timestamp *time.Time `json:"timestamp,omitempty"` // Could be empty in cross shard TXs
}

type regularTx struct {
	BlockNumber uint64 `json:"blockNumber"`
	Direction   string `json:"direction,omitempty"` // `incoming` or 'outgoing'. Could be empty in cross shard TXs
	ShardID     uint32 `json:"shardID"`
	Amount      int64  `json:"amount"`
	TxFee       int64  `json:"txFee"`
}

type CSTXLock struct {
	Hash      string   `json:"hash"`
	TxFee     int64    `json:"txFee"`
	Addresses []string `json:"addresses"`
}

type cstxShardRecord struct {
	txHeaders
	regularTx
	Lock      *CSTXLock `json:"lock"`
	Addresses []string  `json:"addresses"`
}

type TxRecord struct {
	txHeaders
	regularTx
	Addresses []string `json:"addresses"`
}

type CSTXRecord struct {
	txHeaders
	AgentFee         int64            `json:"agentFee"`
	SourceShard      *cstxShardRecord `json:"sourceShard"`
	DestinationShard *cstxShardRecord `json:"destinationShard"`
}

func handleHistoryRequest(ctx *fasthttp.RequestCtx) {
	var (
		err, reportedError error

		address   string
		from      = string(ctx.QueryArgs().Peek("from"))
		to        = string(ctx.QueryArgs().Peek("to"))
		shardsIDs = ctx.QueryArgs().PeekMulti("shard")
	)
	defer http.EstablishHandlerWideErrorsProcessing(&err, ctx)
	defer http.EstablishUserVisibleErrorsProcessing(&reportedError, ctx)

	if (from != "" && to == "") || (to != "" && from == "") {
		reportedError = errors.New("arguments `from` and `to` must be always specified together")
		return
	}

	if len(shardsIDs) == 0 {
		reportedError = errors.New("at least one shard must be specified")
		return
	}

	var (
		parsedShards = ""
		parsedShard  uint64
		firstRound   = true
	)

	for _, shardID := range shardsIDs {
		parsedShard, reportedError = strconv.ParseUint(string(shardID), 10, 32)
		if err != nil {
			reportedError = fmt.Errorf("can't parse shardID: %w", reportedError)
			return
		}

		{
			if firstRound {
				parsedShards = fmt.Sprint(parsedShard)
			} else {
				parsedShards = fmt.Sprint(parsedShards, ",", parsedShard)
			}
			firstRound = false
		}
	}

	// No need to validate this field.
	// In case of invalid data - the response would just be empty.
	address = string(ctx.QueryArgs().Peek("address"))

	handleHistoryFetching(address, from, to, parsedShards, ctx)
}

func handleHistoryFetching(address, from, to, shardsIDs string, ctx *fasthttp.RequestCtx) {
	var (
		err        error
		userErr    error
		timeFrom   time.Time
		timeTo     time.Time
		operations []RawOperation
		response   *HistoryResponse
	)
	defer http.EstablishHandlerWideErrorsProcessing(&err, ctx)
	defer http.EstablishUserVisibleErrorsProcessing(&userErr, ctx)

	timeFrom, userErr = time.Parse(time.RFC3339, from)
	if userErr != nil {
		return
	}
	timeFrom = utils.ToIndexInternalTime(timeFrom)

	timeTo, userErr = time.Parse(time.RFC3339, to)
	if userErr != nil {
		return
	}
	timeTo = utils.ToIndexInternalTime(timeTo)

	const maxDaysRange = time.Hour * 24 * 7
	if timeTo.Sub(timeFrom) > maxDaysRange {
		userErr = fmt.Errorf("no more than 7 days are allowed to be fetched in a time range")
		return
	}

	query := fmt.Sprintf(
		queryFromToRaw(),
		shardsIDs,
		address,
		address,
		shardsIDs,
		clickhouse.DropTZInfo(timeFrom.UTC()),
		clickhouse.DropTZInfo(timeTo.UTC()))

	operations, err = fetchRawOperations(query)
	if err != nil {
		return
	}

	response = reformatHistory(address, operations)
	response.Data.NearestDates.Next,
		response.Data.NearestDates.Previous, err = fetchNearestOperationsDates(timeFrom, timeTo, address, shardsIDs)
	if err != nil {
		return
	}

	err = http.WriteJSONResponse(response, ctx)
	return
}

func reformatHistory(anchorAddress string, operations []RawOperation) (response *HistoryResponse) {
	TXsOperations, CSTXsOperations, hashesOrder := groupOperationsIntoTXs(operations)

	lockTxs := mapLockWithCorrespondingCSTXs(anchorAddress, TXsOperations, CSTXsOperations)

	// Dropping locks from regular TXs set.
	// Locks would be merged with the CSTXs.
	for _, lockTXsSet := range lockTxs {
		for _, lockTX := range lockTXsSet {
			delete(TXsOperations, lockTX.Hash)
		}
	}

	TXsRecords, CSTXsRecords := buildHistoryRecordsFromOperations(
		anchorAddress, TXsOperations, CSTXsOperations, lockTxs)

	response = mergeRecordsAccordingToOriginalOrder(TXsRecords, CSTXsRecords, hashesOrder)
	return
}

func groupOperationsIntoTXs(ops []RawOperation) (TXs, CSTXs map[string][]RawOperation, hashesOrder []string) {
	// This list specifies the order into which the TX hashes occur into the response from the database (ops).
	// It is used to restore original sequence of structures into the response.
	hashesOrder = make([]string, 0, len(ops)/2)

	TXs = make(map[string][]RawOperation)
	CSTXs = make(map[string][]RawOperation)

	for _, op := range ops {
		// Addresses are supposed to be sorted on the database level.
		// So, there is no need to scan whole hashes set each time new one is added.
		if len(hashesOrder) == 0 {
			hashesOrder = append(hashesOrder, op.TxHash)
		} else {
			if hashesOrder[len(hashesOrder)-1] != op.TxHash {
				hashesOrder = append(hashesOrder, op.TxHash)
			}
		}

		if op.IsCrossShard {
			operations, cstxAlreadySeen := CSTXs[op.TxHash]
			if cstxAlreadySeen {
				operations = append(operations, op)
				CSTXs[op.TxHash] = operations

			} else {
				const defaultOperationsPerTx = 16
				operations = make([]RawOperation, 0, defaultOperationsPerTx)
				operations = append(operations, op)
				CSTXs[op.TxHash] = operations
			}

		} else {
			operations, txAlreadySeen := TXs[op.TxHash]
			if txAlreadySeen {
				operations = append(operations, op)
				TXs[op.TxHash] = operations

			} else {
				const defaultOperationsPerTx = 2
				operations = make([]RawOperation, 0, defaultOperationsPerTx)
				operations = append(operations, op)
				TXs[op.TxHash] = operations
			}
		}
	}

	return
}

func mapLockWithCorrespondingCSTXs(anchorAddress string, TXs, CSTXs map[string][]RawOperation) (lockTXs map[string][]CSTXLock) {
	lockTXs = make(map[string][]CSTXLock)

	var (
		err error
	)

	for _, cstxOperationsSet := range CSTXs {

		var (
			correspondingLockTxs []*TxRecord
			lockTxs              = make([]CSTXLock, 0, 2)
		)

		// Using operations set of the CSTX it is possible to identify lock TX that has populated this CSTX.
		// By looking for proper input and matching it with the Lock TX hash.
		for _, cstxOperation := range cstxOperationsSet {
			// Ignore outputs of the CSTX
			// (funds locking TX moves into the CSTX, so only inputs are interesting).
			if cstxOperation.IsOutput() {
				continue
			}

			if cstxOperation.AddressIsValidMultiSig() == false {
				continue
			}

			correspondingTxOperations, lockTXFound := TXs[cstxOperation.OriginTXHash]
			if lockTXFound {
				for _, lock := range lockTxs {
					for _, correspondingTx := range correspondingTxOperations {
						if lock.Hash == correspondingTx.TxHash {
							lockTXFound = false
							break
						}
					}

					if lockTXFound == false {
						break
					}
				}
			}

			if lockTXFound == false {
				continue
			}

			lockTxHash := cstxOperation.OriginTXHash

			// Lock TX is just a regular TX.
			// Operations of this TX must be properly processed to find TX amount and other required fields.
			correspondingLockTxs, err = buildRegularTxHistoryRecord(anchorAddress, correspondingTxOperations, true)
			if len(correspondingLockTxs) == 0 || err != nil {
				// Lock TX found byt could not be parsed.
				logger.Log.Warn().Str(
					"lock-tx-hash", lockTxHash).Msg(
					"Lock TX found, but could not be parsed")

				continue
			}

			// One lock TX could potentially move funds into more than CSTX,
			// end even contain some other funds moving operations.
			for _, fundsMove := range correspondingLockTxs {
				if fundsMove.Direction == DirectionOutgoing {
					lockTx := CSTXLock{
						Hash:      lockTxHash,
						TxFee:     fundsMove.TxFee,
						Addresses: fundsMove.Addresses,
					}
					lockTxs = append(lockTxs, lockTx)
					if len(lockTxs) >= 2 {
						goto bothLockCSTXFound
					}
				}
			}
		}

	bothLockCSTXFound:
		cstxHash := cstxOperationsSet[0].TxHash
		lockTXs[cstxHash] = lockTxs
	}

	return
}

func buildHistoryRecordsFromOperations(
	anchorAddress string,
	TXsOperations, CSTXsOperations map[string][]RawOperation,
	lockTXs map[string][]CSTXLock) (
	TXsRecords map[string][]*TxRecord, CSTXsRecords map[string]*CSTXRecord) {

	var err error
	TXsRecords = make(map[string][]*TxRecord)
	CSTXsRecords = make(map[string]*CSTXRecord)

	for _, txOperations := range TXsOperations {
		var records []*TxRecord
		records, err = buildRegularTxHistoryRecord(anchorAddress, txOperations, false)
		if err != nil {
			logger.Log.Err(err).Stack().Msg("")
			continue
		}

		TXsRecords[records[0].Hash] = records
	}

	for _, cstxOperations := range CSTXsOperations {
		var record *CSTXRecord
		record, err = buildCSTxHistoryRecord(anchorAddress, cstxOperations, lockTXs)
		if err != nil {
			logger.Log.Err(err).Stack().Msg("")
			continue
		}

		CSTXsRecords[record.Hash] = record
	}

	return
}

func buildRegularTxHistoryRecord(anchorAddress string, ops []RawOperation, isLockTX bool) (records []*TxRecord, err error) {
	if len(ops) == 0 {
		err = errors.New(
			"operations set could not be empty " +
				"(at least one operation must be specified to build the regular tx history record)")
		return
	}

	var (
		totalTXOutgoing      int64 = 0
		totalTXIncoming      int64 = 0
		totalAddressOutgoing int64 = 0
		totalAddressIncoming int64 = 0
		sourceAddresses            = make([]string, 0, len(ops))
		destinationAddresses       = make([]string, 0, len(ops))
	)

	for _, operation := range ops {
		if operation.Amount == 0 {
			// Index has collected some amount of invalid operations in the past.
			// Now these operations must be filtered away.
			continue
		}

		if operation.IsInput() {
			totalTXOutgoing += operation.Amount
			if operation.Address == anchorAddress {
				totalAddressOutgoing += operation.Amount
			}

			sourceAddresses = appendAddressIfNotPresent(sourceAddresses, operation.Address)

		} else {
			totalTXIncoming += operation.Amount
			if operation.Address == anchorAddress {
				totalAddressIncoming += operation.Amount
			}

			//if operation.Address != anchorAddress {
			destinationAddresses = appendAddressIfNotPresent(destinationAddresses, operation.Address)
			//}
		}
	}

	firstOperation := ops[0]
	header := txHeaders{
		Hash:      firstOperation.TxHash,
		Timestamp: &firstOperation.Timestamp,
	}

	txFee := totalTXOutgoing - totalTXIncoming
	if totalAddressOutgoing-txFee == totalAddressIncoming {
		body := regularTx{
			BlockNumber: firstOperation.BlockNumber,
			Direction:   DirectionLoop,
			ShardID:     firstOperation.ShardID,
			TxFee:       txFee,
			Amount:      totalAddressOutgoing - totalAddressIncoming,
		}
		record := &TxRecord{
			txHeaders: header,
			regularTx: body,
			Addresses: sourceAddresses,
		}

		records = append(records, record)

	} else {
		if totalAddressIncoming > totalAddressOutgoing {
			body := regularTx{
				BlockNumber: firstOperation.BlockNumber,
				Direction:   DirectionIncoming,
				ShardID:     firstOperation.ShardID,
				TxFee:       txFee,
				Amount:      totalAddressIncoming - totalAddressOutgoing,
			}
			record := &TxRecord{
				txHeaders: header,
				regularTx: body,
				Addresses: sourceAddresses,
			}

			records = append(records, record)

		} else {
			body := regularTx{
				BlockNumber: firstOperation.BlockNumber,
				Direction:   DirectionOutgoing,
				ShardID:     firstOperation.ShardID,
				TxFee:       txFee,
				Amount:      totalAddressOutgoing - totalAddressIncoming,
			}

			if totalAddressIncoming > 0 && !isLockTX {
				// The transaction is recognized as outgoing, but
				// there is a rest that is moved back to the anchor address.
				// In this case operations set would contain anchor address as a receiver,
				// but history record should show only the real receivers set.
				// (removing anchor address from outgoing addresses)
				for i, address := range destinationAddresses {
					if address == anchorAddress {
						destinationAddresses = remove(destinationAddresses, i)
						break
					}
				}
			}

			record := &TxRecord{
				txHeaders: header,
				regularTx: body,
				Addresses: destinationAddresses,
			}

			records = append(records, record)
		}
	}

	return
}

func buildCSTxHistoryRecord(
	anchorAddress string,
	ops []RawOperation,
	lockTXs map[string][]CSTXLock) (record *CSTXRecord, err error) {

	const (
		// Index adds to the database only those records of the CSTX that belongs to its shard ID.
		// By default, CSTX consist of 4 inputs and 4 outputs in total.
		// But there is a time window after publication when some index already scanned CSTX,
		// but paired index still not scanned the CSTX, so only the half of it is present in the index.
		halfCSTXInputsOutputsSet = 4
		fullCSTXInputsOutputsSet = halfCSTXInputsOutputsSet * 2

		undefinedShardID int64 = -1
	)

	parseShardTX := func(ops []RawOperation) (shardTX *cstxShardRecord, anchorAddressIsAPayee bool, err error) {
		if len(ops) != halfCSTXInputsOutputsSet {
			err = fmt.Errorf("operations set must contains %d records", halfCSTXInputsOutputsSet)
			return
		}

		const (
			maxOutgoingAddresses = 1
			maxIncomingAddresses = 1
		)

		var (
			determinedShardID = undefinedShardID
			outgoingAddresses = make([]string, 0, maxOutgoingAddresses)
			incomingAddresses = make([]string, 0, maxIncomingAddresses)

			totalTXIncoming int64 = 0
			totalTXOutgoing int64 = 0
			txAmount        int64 = 0
		)

		for _, operation := range ops {
			if operation.IsCrossShard == false {
				err = fmt.Errorf("expected cross-shard operation, got non-cross-shard one")
				return
			}

			if operation.ShardID == ec.BTCShardID {
				err = fmt.Errorf("BTC could not take a part in cross shard TXs")
				return
			}

			if operation.ShardID == ec.BeaconShardID {
				err = fmt.Errorf("beacon could not take a part in cross shard TXs")
				return
			}

			if determinedShardID == undefinedShardID {
				determinedShardID = int64(operation.ShardID)

			} else if determinedShardID != int64(operation.ShardID) {
				err = fmt.Errorf("operations set supposed to contains records from one shard only, " +
					"but this one seems to be incosistent (contains operations from different shards)")
			}

			if operation.IsInput() {
				totalTXOutgoing += operation.Amount

				if operation.AddressIsRegular() {
					outgoingAddresses = appendAddressIfNotPresent(outgoingAddresses, operation.Address)
				}

				if operation.AddressIsValidMultiSig() {
					txAmount = operation.Amount
				}

			} else {
				totalTXIncoming += operation.Amount

				if operation.AddressIsRegular() {
					// TX record should not include addresses that collects the TX rest.
					incomingAddresses = appendAddressIfNotPresent(incomingAddresses, operation.Address)
				}

				if operation.AddressIsValidMultiSig() {
					txAmount = operation.Amount
				}
			}
		}

		firstOp := ops[0]
		shardTX = &cstxShardRecord{
			txHeaders: txHeaders{
				Hash:      firstOp.TxHash,
				Timestamp: &firstOp.Timestamp,
			},
			regularTx: regularTx{
				BlockNumber: firstOp.BlockNumber,
				ShardID:     uint32(determinedShardID),
				Amount:      txAmount,
				TxFee:       totalTXOutgoing - totalTXIncoming,
			},
		}

		lockTXs, lockTXsAreFound := lockTXs[firstOp.TxHash]
		if !lockTXsAreFound {
			err = fmt.Errorf("can't found lock TXs of the CSTX")
			return
		}

		// Attaching lock TX info to the CSTX shard TX
		for _, lock := range lockTXs {
			for _, lockAddress := range lock.Addresses {
				for _, cstxOperation := range ops {
					if cstxOperation.IsOutput() {
						continue
					}

					if !cstxOperation.AddressIsValidMultiSig() {
						continue
					}

					if cstxOperation.Address == lockAddress {
						shardTX.Lock = &lock
						for _, address := range lock.Addresses {
							if address == anchorAddress {
								// Anchor address has populated the CSTX with the funds.
								// So it could be only Sender or EAD.
								// In both cases, this part of CSTX would be seen as a source shard by them.
								anchorAddressIsAPayee = true
								goto shardLockDetected
							}
						}

						goto shardLockDetected
					}
				}
			}
		}
	shardLockDetected:

		// Dropping EAD address from both shards addresses lists.
		for incomingOperationsI, incomingOpAddress := range incomingAddresses {
			for outgoingOperationsI, outgoingOpAddress := range outgoingAddresses {
				if incomingOpAddress == outgoingOpAddress {
					// EAD address found.
					incomingAddresses = remove(incomingAddresses, incomingOperationsI)
					outgoingAddresses = remove(outgoingAddresses, outgoingOperationsI)
					break
				}
			}
		}

		if !anchorAddressIsAPayee {
			shardTX.Addresses = incomingAddresses
		}

		return
	}

	var (
		txA, txB                   *cstxShardRecord
		anchorAddressIsAPayeeInTXA = false
		anchorAddressIsAPayeeInTXB = false
	)

	switch len(ops) {
	case halfCSTXInputsOutputsSet:
		// Only half of CSTX is present in index.
		// This is possible in case when only one index has indexed the TX so far.
		txA, anchorAddressIsAPayeeInTXA, err = parseShardTX(ops[0:4])
		if err != nil {
			return
		}

		record = &CSTXRecord{
			txHeaders: txHeaders{
				Hash: txA.Hash,
			},
		}
		if anchorAddressIsAPayeeInTXA {
			record.SourceShard = txA
			record.SourceShard.Hash = "" // prevent redundant data marshalling.
		} else {
			record.DestinationShard = txA
			record.SourceShard.Hash = "" // prevent redundant data marshalling.
		}

		return

	case fullCSTXInputsOutputsSet:
		txA, anchorAddressIsAPayeeInTXA, err = parseShardTX(ops[0:4])
		if err != nil {
			return
		}

		txB, anchorAddressIsAPayeeInTXB, err = parseShardTX(ops[4:8])
		if err != nil {
			return
		}

		if anchorAddressIsAPayeeInTXA == anchorAddressIsAPayeeInTXB && anchorAddressIsAPayeeInTXB == true {
			err = fmt.Errorf("could not determine payee of the TX")
			return
		}

		record = &CSTXRecord{
			txHeaders: txHeaders{
				Hash: txA.Hash,
			},
		}

		record.SourceShard = txA
		record.DestinationShard = txB
		record.AgentFee = record.SourceShard.Amount - record.DestinationShard.Amount

		// todo: fix me according to the convention "source shard records are the first 2 records of the CSTX".
		//		 At the moment, this convention is only accepted by core developers,
		//		 but still needs to be supported by developers of the EAD.
		//		 It also needs to be documented in public docs and index support needs to be done also.
		//		 For that - core should provide a guaranties about operations order fetched by GetBlockTxOperations.
		if record.AgentFee < 0 {
			record.SourceShard = txB
			record.DestinationShard = txA
			record.AgentFee = record.SourceShard.Amount - record.DestinationShard.Amount
		}

		record.SourceShard.Hash = ""      // prevent redundant data marshalling.
		record.DestinationShard.Hash = "" // prevent redundant data marshalling.

		// Dropping EAD address from both shards addresses lists.
		for sourceShardI, sourceShardAddress := range record.SourceShard.Addresses {
			for destShardI, destShardAddress := range record.DestinationShard.Addresses {
				if sourceShardAddress == destShardAddress {
					// EAD address found.
					// It is the same in both shards.
					record.SourceShard.Addresses = remove(record.SourceShard.Addresses, sourceShardI)
					record.DestinationShard.Addresses = remove(record.DestinationShard.Addresses, destShardI)
					break
				}
			}
		}

		// Adding real sender to source shard address
		for _, address := range record.SourceShard.Lock.Addresses {
			if address[0] != '2' {
				record.SourceShard.Addresses = append(record.SourceShard.Addresses, address)
				break
			}
		}

		return

	default:
		err = fmt.Errorf("CSTX operations set must contain %d or %d of records",
			halfCSTXInputsOutputsSet, fullCSTXInputsOutputsSet)
		return
	}
}

func mergeRecordsAccordingToOriginalOrder(
	TXs map[string][]*TxRecord, CSTXs map[string]*CSTXRecord, hashesOrder []string) (response *HistoryResponse) {

	totalRecordsCount := len(TXs) + len(CSTXs)
	response = &HistoryResponse{}
	response.Data = historyData{
		Records:      make([]interface{}, 0, totalRecordsCount),
		NearestDates: nearestDates{},
	}

	for _, hash := range hashesOrder {
		txRecords, txsArePresent := TXs[hash]
		if txsArePresent {
			for _, record := range txRecords {
				response.Data.Records = append(response.Data.Records, record)
			}

			// It is supposed no hash duplication is possible.
			continue
		}

		cstxRecord, cstxIsPresent := CSTXs[hash]
		if cstxIsPresent {
			response.Data.Records = append(response.Data.Records, cstxRecord)

			// It is supposed no hash duplication is possible.
			continue
		}
	}

	return
}

func appendAddressIfNotPresent(addresses []string, address string) []string {
	for _, a := range addresses {
		if a == address {
			return addresses
		}
	}

	return append(addresses, address)
}

func remove(s []string, i int) []string {
	s[i] = s[len(s)-1]
	return s[:len(s)-1]
}
