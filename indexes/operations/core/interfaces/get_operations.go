package interfaces

import (
	"database/sql"
	"errors"
	"fmt"
	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/core/indexer/core/clickhouse"
	"gitlab.com/jaxnet/core/indexer/core/database"
	"gitlab.com/jaxnet/core/indexer/core/interfaces/http"
	"gitlab.com/jaxnet/core/indexer/core/utils"
	"strconv"
	"strings"
	"time"
)

// Hard bound prevents users from fetching too wide range of dates using one request only.
const hardRequestBound = 10 * 1024

type operation struct {
	TxHash      string    `json:"txHash"`
	BlockNumber uint64    `json:"blockNumber"`
	ShardID     uint32    `json:"shardID"`
	Amount      int64     `json:"amount"`
	Address     string    `json:"address,omitempty"`
	Timestamp   time.Time `json:"timestamp"`
}

// DEPRECATED
type Operation struct {
	operation
	Direction string `json:"direction"`
}

type RawOperation struct {
	operation
	IsIncoming   bool   `json:"isIncoming"`
	IsCrossShard bool   `json:"isCrossShard"`
	OriginTXHash string `json:"originTxHash"`
}

func (o RawOperation) IsInput() bool {
	return o.IsIncoming == false
}

func (o RawOperation) IsOutput() bool {
	return !o.IsInput()
}

func (o RawOperation) AddressIsValidMultiSig() bool {
	return len(o.Address) > 0 && o.Address[0] == '2'
}

func (o RawOperation) AddressIsRegular() bool {
	return len(o.Address) > 0 && o.Address[0] != '2'
}

var (
	// DEPRECATED
	// This query fetches operations that belongs to the specified address in the specified set of shards
	// including all corresponding cross shard transactions, that relates to the specified address
	// (has been populated from it, or benefits from it).
	queryLimitOffsetTemplate = "SELECT hash, amount, address, timestamp, isIncoming, blockNumber, shardID " +
		"FROM {{ table }} " +
		"WHERE hash IN ( " +

		//   This statement allows reusing of the query results (potentially with caching)
		"    WITH cached_regular_txs AS ( " +
		"        SELECT hash " +
		"        FROM {{ table }} " +
		"        WHERE shardID IN (%s) " +
		"          AND address = '%s' " +
		"        ORDER BY timestamp DESC, hash, isIncoming, amount " +
		"    ) " +

		//   This SELECT drops hashes duplications that occur as a result of UNION ALL
		"    SELECT DISTINCT hash " +
		"    FROM ( " +
		"          SELECT hash " +
		"          FROM cached_regular_txs " +
		"          UNION ALL " +
		"          SELECT hash " +
		"          FROM {{ table }} " +
		"          WHERE address IN (" +

		//             Selects multi. sig addresses that are populated by the regular operations,
		//             that involves specified non-multi sig. address.
		"              SELECT address " +
		"              FROM {{ table }} " +
		"              WHERE hash in ( " +

		//                 Selects regular transactions that involves specified address.
		"                  SELECT DISTINCT hash " +
		"                  FROM cached_regular_txs " +
		"              ) " +
		"                AND isIncoming = 1 " +
		"                AND startsWith(address, '2') " +
		"              ORDER BY timestamp DESC, hash, isIncoming, amount " +
		"         ) " +
		"    ) " +
		") " +
		"ORDER BY timestamp DESC, hash, isIncoming, amount " +
		"LIMIT %d OFFSET %d "

	// DEPRECATED
	queryLimitOffsetCache *string = nil

	// DEPRECATED
	// This query fetches operations that belongs to the specified address in the specified set of shards
	// including all corresponding cross shard transactions (including corresponding locks),
	// that relates to the specified address (has been populated from it, or benefits from it).
	queryFromToTemplate =
	//   This statement allows reusing of the query results (potentially with caching)
	"WITH cached_regular_txs AS ( " +
		"SELECT hash " +
		"FROM {{ table }} " +
		"  WHERE " +
		"        shardID IN (%s) " +
		"    AND address = '%s' " +
		"	 AND timestamp >= toDateTime('%s') " + // NOTE: This line differs from previous script.
		"	 AND timestamp < toDateTime('%s') " + // NOTE: This line differs from previous script.
		"ORDER BY timestamp DESC, hash, isIncoming, amount " +
		") " +
		"SELECT hash, amount, address, timestamp, isIncoming, blockNumber, shardID " +
		"FROM {{ table }} " +
		"WHERE hash IN ( " +

		//   This SELECT drops hashes duplications that occur as a result of UNION ALL
		"    SELECT DISTINCT hash " +
		"    FROM ( " +
		"          SELECT hash " +
		"          FROM cached_regular_txs " +
		"          UNION ALL " +
		"          SELECT hash " +
		"          FROM {{ table }} " +
		"          WHERE address IN (" +
		//             Selects multi. sig addresses that are populated by the regular operations,
		//             that involves specified non-multi sig. address.
		"              SELECT address " +
		"              FROM {{ table }} " +
		"              WHERE hash in ( " +

		//                 Selects regular transactions that involves specified address.
		"                  SELECT DISTINCT hash " +
		"                  FROM cached_regular_txs " +
		"              ) " +
		"                AND isIncoming = 1 " +
		"                AND startsWith(address, '2') " +
		"              ORDER BY timestamp DESC, hash, isIncoming, amount " +
		"         ) " +
		"    ) " +
		") " +
		"ORDER BY timestamp DESC, hash, isIncoming, amount "

	// DEPRECATED
	queryFromToCache *string = nil

	// This query fetches raw operations that belongs to the specified address in the specified set of shards
	// including all corresponding cross shard transactions (but without corresponding locks)
	// that relates to the specified address (has been populated from it, or benefits from it).
	queryFromToRawTemplate =
	// This statement allows reusing of the query results (potentially with caching)
	`
		SELECT 
			hash, 
			originTxHash, 
			amount, 
			address, 
			timestamp, 
			isIncoming, 
			blockNumber, 
			shardID, 
			isCrossShard  
		FROM {{ table }}  
		WHERE hash IN (
			WITH cached_api_caller_txs AS (
			SELECT DISTINCT hash, address, isIncoming, originTxHash
			FROM operations
				WHERE 
					shardID IN (%s)  
			    	AND address = '%s'
			    	AND isCrossShard = 0  
			),  
			cached_api_caller_cstxs_by_reg_txs AS ( 
				SELECT hash, address, isIncoming, originTxHash
				FROM operations 
				WHERE address IN ( 
					SELECT address 
					FROM cached_api_caller_txs 
					WHERE 
						startsWith(address, '2')
						AND isIncoming = 1 
			    )
			),
			cached_api_caller_cstxs_by_address AS ( 
				SELECT hash 
				FROM operations  
				WHERE 
					address = '%s' 
					AND isCrossShard = 1 
					AND shardID IN (%s)
			),
			cached_cstxs_by_api_caller_txs AS (
				SELECT hash
				FROM operations
				WHERE originTxHash IN (SELECT originTxHash FROM cached_api_caller_txs)
				  AND isCrossShard = 1
			),
			cached_ctxss_locks_by_cstxs AS (
				SELECT hash
				FROM operations
				WHERE hash IN (
					SELECT originTxHash
					FROM operations
					WHERE hash IN (SELECT hash FROM cached_cstxs_by_api_caller_txs))
					AND isCrossShard = 0
			)
			SELECT DISTINCT hash
            FROM (
				SELECT hash
				FROM cached_api_caller_txs

				UNION ALL
				SELECT hash
				FROM cached_cstxs_by_api_caller_txs

				UNION ALL
				SELECT hash
				FROM cached_ctxss_locks_by_cstxs

				UNION ALL
				SELECT hash
				FROM cached_api_caller_cstxs_by_reg_txs
			
				UNION ALL
				SELECT hash
				FROM cached_api_caller_cstxs_by_address
			
				UNION ALL
				SELECT hash
				FROM operations
				WHERE hash IN (
                	SELECT originTxHash
                	FROM operations
					WHERE hash IN (
                		SELECT hash
                      	FROM cached_api_caller_cstxs_by_reg_txs
                    	)
					AND startsWith(address, '2')
				)
			
				UNION ALL
				SELECT hash
				FROM operations
					WHERE hash IN (
                    	SELECT originTxHash
                    	FROM operations
                    	WHERE hash IN (
							SELECT hash
                      		FROM cached_api_caller_cstxs_by_address
                      		)
                      	AND startsWith(address, '2')
                    )
                )
			)
			
			AND timestamp > toDateTime('%s')  
			AND timestamp <= toDateTime('%s')
			ORDER BY timestamp DESC, hash, shardID, isIncoming, amount;
		  `

	queryFromToRawCache *string = nil

	queryNearestNextDateTemplate = `
		SELECT timestamp
		FROM {{ table }}
		WHERE
			    address = '%s'
			and shardID IN (%s)
			and timestamp > toDateTime('%s')
		ORDER BY timestamp ASC
		LIMIT 1
		`

	queryNearestNextDateCache *string = nil

	queryNearestPreviousDateTemplate = `
		SELECT timestamp
		FROM {{ table }}
		WHERE
			    address = '%s'
			and shardID IN (%s)
			and timestamp < toDateTime('%s')
		ORDER BY timestamp DESC
		LIMIT 1
		`

	queryNearestPreviousDateCache *string = nil
)

// DEPRECATED
// queryLimitOffset caches query with the predefined table name.
// Makes query building procedure a bit faster,
// because table name is going to be set only once (when the first query occurs).
func queryLimitOffset() string {
	if queryLimitOffsetCache == nil {
		s := strings.ReplaceAll(queryLimitOffsetTemplate, "{{ table }}", clickhouse.DataTableName())
		queryLimitOffsetCache = &s
	}

	return *queryLimitOffsetCache
}

// DEPRECATED
// queryFromTo caches query with the predefined table name.
// Makes query building procedure a bit faster,
// because table name is going to be set only once (when the first query occurs).
func queryFromTo() string {
	if queryFromToCache == nil {
		s := strings.ReplaceAll(queryFromToTemplate, "{{ table }}", clickhouse.DataTableName())
		queryFromToCache = &s
	}

	return *queryFromToCache
}

// queryFromToRaw caches query with the predefined table name.
// Makes query building procedure a bit faster,
// because table name is going to be set only once (when the first query occurs).
func queryFromToRaw() string {
	if queryFromToRawCache == nil {
		query := strings.ReplaceAll(queryFromToRawTemplate, "{{ table }}", clickhouse.DataTableName())
		query = database.MinimizeSQLScript(query)
		queryFromToRawCache = &query
	}

	return *queryFromToRawCache
}

func queryNearestNextDate() string {
	if queryNearestNextDateCache == nil {
		query := strings.ReplaceAll(queryNearestNextDateTemplate, "{{ table }}", clickhouse.DataTableName())
		query = database.MinimizeSQLScript(query)
		queryNearestNextDateCache = &query
	}

	return *queryNearestNextDateCache
}

func queryNearestPreviousDate() string {
	if queryNearestPreviousDateCache == nil {
		query := strings.ReplaceAll(queryNearestPreviousDateTemplate, "{{ table }}", clickhouse.DataTableName())
		query = database.MinimizeSQLScript(query)
		queryNearestPreviousDateCache = &query
	}

	return *queryNearestPreviousDateCache
}

// DEPRECATED
func handleOperationsRequest(ctx *fasthttp.RequestCtx) {
	var (
		err, reportedError error

		address   string
		from      = string(ctx.QueryArgs().Peek("from"))
		to        = string(ctx.QueryArgs().Peek("to"))
		limit     = string(ctx.QueryArgs().Peek("limit"))
		offset    = string(ctx.QueryArgs().Peek("offset"))
		shardsIDs = ctx.QueryArgs().PeekMulti("shard")
	)
	defer http.EstablishHandlerWideErrorsProcessing(&err, ctx)
	defer http.EstablishUserVisibleErrorsProcessing(&reportedError, ctx)

	if (from != "" || to != "") && (limit != "" || offset != "") {
		reportedError = errors.New(
			"invalid combination of arguments occurred, " +
				"only one arguments pair is allowed: (from, to) or (limit, offset)")
		return
	}

	if (from != "" && to == "") || (to != "" && from == "") {
		reportedError = errors.New("arguments `from` and `to` must be always specified together")
		return
	}

	if (limit != "" && offset == "") || (offset != "" && limit == "") {
		reportedError = errors.New("arguments `limit` and `offset` must be always specified together")
		return
	}

	if len(shardsIDs) == 0 {
		reportedError = errors.New("at least one shard must be specified")
		return
	}

	var (
		parsedShards = ""
		parsedShard  uint64
		firstRound   = true
	)

	for _, shardID := range shardsIDs {
		parsedShard, reportedError = strconv.ParseUint(string(shardID), 10, 32)
		if err != nil {
			reportedError = fmt.Errorf("can't parse shardID: %w", reportedError)
			return
		}

		{
			if firstRound {
				parsedShards = fmt.Sprint(parsedShard)
			} else {
				parsedShards = fmt.Sprint(parsedShards, ",", parsedShard)
			}
			firstRound = false
		}
	}

	// No need to validate this field.
	// In case of invalid data - the response would just be empty.
	address = string(ctx.QueryArgs().Peek("address"))

	if from != "" {
		handleOperationsByFromAndTo(address, from, to, parsedShards, ctx)
	} else {
		handleOperationsByOffsetAndLimit(address, offset, limit, parsedShards, ctx)
	}
}

// DEPRECATED
func handleOperationsByFromAndTo(address, from, to, shardsIDs string, ctx *fasthttp.RequestCtx) {
	var (
		err      error
		userErr  error
		timeFrom time.Time
		timeTo   time.Time
		response []Operation
	)
	defer http.EstablishHandlerWideErrorsProcessing(&err, ctx)
	defer http.EstablishUserVisibleErrorsProcessing(&userErr, ctx)

	// No need to validate this field.
	// In case of invalid data - the response would just be empty.
	address = string(ctx.QueryArgs().Peek("address"))

	timeFrom, userErr = time.Parse(time.RFC3339, from)
	if userErr != nil {
		return
	}
	timeFrom = utils.ToIndexInternalTime(timeFrom)

	timeTo, userErr = time.Parse(time.RFC3339, to)
	if userErr != nil {
		return
	}
	timeTo = utils.ToIndexInternalTime(timeTo)

	query := fmt.Sprintf(
		queryFromTo(),
		shardsIDs,
		address,
		clickhouse.DropTZInfo(timeFrom.UTC()),
		clickhouse.DropTZInfo(timeTo.UTC()))

	response, err = fetchOperations(query)
	if err != nil {
		return
	}

	err = http.WriteJSONResponse(response, ctx)
}

// DEPRECATED
func handleOperationsByOffsetAndLimit(address, offset, limit, shardsIDs string, ctx *fasthttp.RequestCtx) {
	var (
		err       error
		userErr   error
		intOffset uint64
		intLimit  uint64
		response  []Operation
	)
	defer http.EstablishHandlerWideErrorsProcessing(&err, ctx)
	defer http.EstablishUserVisibleErrorsProcessing(&userErr, ctx)

	intOffset, userErr = strconv.ParseUint(offset, 10, 64)
	if userErr != nil {
		return
	}

	intLimit, userErr = strconv.ParseUint(limit, 10, 64)
	if userErr != nil {
		return
	}

	if intLimit > hardRequestBound {
		userErr = errors.New(fmt.Sprintf("limit can't be greater than %d", hardRequestBound))
		return
	}

	query := fmt.Sprintf(
		queryLimitOffset(),
		shardsIDs,
		address,
		intLimit,
		intOffset)

	response, err = fetchOperations(query)
	if err != nil {
		return
	}

	err = http.WriteJSONResponse(response, ctx)
}

// DEPRECATED
func fetchOperations(query string) (operations []Operation, err error) {
	tx, err := clickhouse.BeginOperation()
	if err != nil {
		return
	}
	defer func(tx *sql.Tx) {
		_ = tx.Rollback()
	}(tx)

	rows, err := tx.Query(query)
	if err != nil {
		return
	}
	defer rows.Close()

	// Estimated average amount of records per request.
	// In case if there would be more operations for specified time range -- it is OK.
	// The operations slice would be used queryLimitOffsetTemplate bit less efficient, but it would still work correctly.
	const estAvgOperationsPerRequest = 128

	op := Operation{}
	operations = make([]Operation, 0, estAvgOperationsPerRequest)
	for rows.Next() {

		// WARN: Fields order is important!
		// 		 See handleOperationsByOffsetAndLimit() and handleOperationsByFromAndTo
		//		 implementations for the details.
		var direction uint8
		err = rows.Scan(&op.TxHash, &op.Amount, &op.Address, &op.Timestamp, &direction, &op.BlockNumber, &op.ShardID)
		if err != nil {
			return
		}

		if direction == 0 {
			op.Direction = "out"

		} else {
			op.Direction = "in"
		}

		operations = append(operations, op)
	}

	return

}

func fetchRawOperations(query string) (operations []RawOperation, err error) {
	tx, err := clickhouse.BeginOperation()
	if err != nil {
		return
	}
	defer func(tx *sql.Tx) {
		_ = tx.Rollback()
	}(tx)

	rows, err := tx.Query(query)
	if err != nil {
		return
	}
	defer rows.Close()

	// Estimated average amount of records per request.
	// In case if there would be more operations for specified time range -- it is OK.
	// The operations slice would be used queryLimitOffsetTemplate bit less efficient, but it would still work correctly.
	const estAvgOperationsPerRequest = 128

	op := RawOperation{}
	operations = make([]RawOperation, 0, estAvgOperationsPerRequest)
	for rows.Next() {

		// WARN: Fields order is important!
		err = rows.Scan(
			&op.TxHash, &op.OriginTXHash, &op.Amount, &op.Address, &op.Timestamp,
			&op.IsIncoming, &op.BlockNumber, &op.ShardID, &op.IsCrossShard)
		if err != nil {
			return
		}

		operations = append(operations, op)
	}

	return
}

// fetchNearestOperationsDates returns a pair of the nearest dates to the `from` and `to` dates passed.
// This method is used to determine if there is still some history records to be fetched from the database.
// The dates returned are in the timezone of the clickhouse.
func fetchNearestOperationsDates(from, to time.Time, anchorAddress string, shardsSet string) (next, previous *time.Time, err error) {
	nextDateQuery := fmt.Sprintf(
		queryNearestNextDate(),
		anchorAddress, shardsSet, clickhouse.DropTZInfo(to.UTC()))
	next = fetchBorderDate(nextDateQuery)

	previousDateQuery := fmt.Sprintf(
		queryNearestPreviousDate(),
		anchorAddress, shardsSet, clickhouse.DropTZInfo(from.UTC()))
	previous = fetchBorderDate(previousDateQuery)
	return
}

func fetchBorderDate(query string) (result *time.Time) {
	tx, err := clickhouse.BeginOperation()
	if err != nil {
		return
	}
	defer func(tx *sql.Tx) {
		_ = tx.Rollback()
	}(tx)

	rows, err := tx.Query(query)
	if err != nil {
		return
	}
	defer rows.Close()

	var rawDate string
	for rows.Next() {
		err = rows.Scan(&rawDate)
		if err != nil {
			return
		}
	}

	if rawDate != "" {
		parsedDate, err := time.Parse(time.RFC3339, rawDate)
		if err != nil {
			return
		}

		result = &parsedDate
	}

	return
}
