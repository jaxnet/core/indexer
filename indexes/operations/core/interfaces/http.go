package interfaces

import (
	"github.com/fasthttp/router"
)

func InitAPIHandlers(root *router.Group) {
	root.GET("operations/", handleOperationsRequest)
	root.GET("history/", handleHistoryRequest)
}
