package implementation

import (
	"database/sql"
	"errors"
	"fmt"
	"github.com/btcsuite/btcd/btcjson"
	"github.com/btcsuite/btcd/rpcclient"
	"github.com/btcsuite/btcd/wire"
	"github.com/jackc/pgx/v4"
	"gitlab.com/jaxnet/core/indexer/core"
	"gitlab.com/jaxnet/core/indexer/core/blocks_cache"
	"gitlab.com/jaxnet/core/indexer/core/clickhouse"
	"gitlab.com/jaxnet/core/indexer/core/logger"
	"gitlab.com/jaxnet/core/indexer/core/settings"
)

type BtcTXsHistoryIndex struct {
	TXsHistoryIndex
}

func (i BtcTXsHistoryIndex) InitRPCClient() (*core.AbstractRPCClient, error) {
	return core.InitBTCRPCClient()
}

func (i BtcTXsHistoryIndex) FetchNextBlockFromNode(offset int64, checkPrevBlock bool,
	rpcClient *core.AbstractRPCClient, cacheHandler *blocks_cache.CacheHandler) (
	block *core.AbstractMsgBlock, prevBlockNumber, blockHeight int64, rebuildingChainBlockNumber int64, err error) {
	return core.FetchNextBlockFromBtcNode(offset, checkPrevBlock, rpcClient, cacheHandler)
}

// WARN: This code is duplicated into jax_implementation!
// In case of any change - please sync both implemenatations.
func (i BtcTXsHistoryIndex) ConnectBlock(_ pgx.Tx, block *core.AbstractMsgBlock, _ int64, rpcClient *core.AbstractRPCClient) (err error) {
	var (
		btcBlock      *wire.MsgBlock
		btcClient     *rpcclient.Client
		ch            *sql.Tx
		chStatement   *sql.Stmt
		operationsSet *btcjson.BlockTxOperations
	)

	defer func() {
		if ch != nil {
			_ = ch.Rollback()
		}
	}()

	defer func() {
		if chStatement != nil {
			_ = chStatement.Close()
		}
	}()

	btcBlock, err = block.BTCMsgBlock()
	if err != nil {
		// This is a critical error (index must be stopped with an error).
		return
	}

	btcClient, err = rpcClient.BTCRPCClient()
	if err != nil {
		// This is a critical error (index must be stopped with an error).
		return
	}

	logger.Log.Debug().Msg("Begin operations fetching")
	blockHash := btcBlock.BlockHash()
	operationsSet, err = btcClient.GetBlockTxOperations(&blockHash)
	if err != nil {
		// Semi-critical error.
		// No further processing is possible, but index should not be stopped.
		// Ignore full block and report error.
		logger.Log.Err(err).Str(
			"hash", blockHash.String()).Msg(
			"can't fetch operations of the block")
		err = nil
		return
	}

	logger.Log.Debug().Int("operations-count", len(operationsSet.Ops)).Msg("Begin operations processing")
	insertsLeft := 0
	for _, operation := range operationsSet.Ops {

		if operation.Coinbase {
			// Ignore coinbase TX
			continue
		}

		if len(operation.Addresses) != 1 {
			err = errors.New("too many addresses (potential multi. sig, which is not supported)")

			// Non-critical err.
			// Suppress: ignore one operation, but still report the error.
			logger.Log.Err(err).Str(
				"tx-hash", operation.TxHash).Uint32(
				"tx-index", operation.TxIndex).Msg("can't process operation")
			err = nil
			continue
		}

		if insertsLeft == 0 {
			ch, err = clickhouse.BeginOperation()
			if err != nil {
				// This is a critical error (index must be stopped with an error).
				return
			}

			format := "INSERT INTO %s (" +
				"hash, blockNumber, originTxHash, txIndex, shardID, address, amount, timestamp, isIncoming, isCrossShard) " +
				"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
			query := fmt.Sprintf(format, clickhouse.DataTableName())
			chStatement, err = ch.Prepare(query)
			if err != nil {
				// This is a critical error (index must be stopped with an error).
				return
			}
		}

		// Click House driver in current revision requires uint8 values to be passed instead of bool.
		// So, that's why this ugly hack is here.
		var isInputUint8 int8
		if !operation.Input || operation.Coinbase {
			isInputUint8 = 1
		}

		_, err = chStatement.Exec(
			operation.TxHash,
			operationsSet.BlockHeight,
			operation.OriginTxHash,
			operation.TxIndex,
			settings.Conf.Blockchain.ID,
			operation.Addresses[0],
			operation.Amount,
			btcBlock.Header.Timestamp.UTC(),
			isInputUint8,
			0)

		if err != nil {
			// Non-critical err.
			// Suppress: ignore one operation, but still report the error.
			logger.Log.Err(err).Str(
				"tx-hash", operation.TxHash).Uint32(
				"tx-index", operation.TxIndex).Msg("can't process operation")
			err = nil

			// It is possible that the error would occur when insertsLeft == 0,
			// and on the next iteration ch and chStatement would be initialized once again,
			// but previous instances should be closed properly.
			if insertsLeft == 0 {
				_ = ch.Rollback()
				_ = chStatement.Close()
			}

			continue
		}

		// Current implementation of CH and/or its driver allows no more than 100 inserts batched into prepare statement.
		// In the same it doesnt allow to execute raw queries like INSERT ... VALUES () () () ...
		// So there is no way except to try to insert by chunks of 99 rows.
		// todo: check it again when this driver would be improved a bit :(
		insertsLeft += 1
		if insertsLeft == 99 {
			err = ch.Commit()
			if err != nil {
				// This is a critical error (index must be stopped with an error).
				return
			}
			insertsLeft = 0
		}
	}

	if insertsLeft > 0 {
		// This is a potentially (!) critical error (index must be stopped with an error).
		err = ch.Commit()
	}

	logger.Log.Debug().Msg("Finished operations processing")
	return
}

// WARN: This code is duplicated into jax_implementation!
// In case of any change - please sync both implemenatations.
func (i BtcTXsHistoryIndex) DisconnectBlock(tx pgx.Tx, block *core.AbstractMsgBlock, _ int64, rpcClient *core.AbstractRPCClient) (err error) {
	var (
		btcBlock      *wire.MsgBlock
		btcClient     *rpcclient.Client
		ch            *sql.Tx
		operationsSet *btcjson.BlockTxOperations
	)

	btcBlock, err = block.BTCMsgBlock()
	if err != nil {
		// This is a critical error (index must be stopped with an error).
		return err
	}

	btcClient, err = rpcClient.BTCRPCClient()
	if err != nil {
		// This is a critical error (index must be stopped with an error).
		return err
	}

	blockHash := btcBlock.BlockHash()
	operationsSet, err = btcClient.GetBlockTxOperations(&blockHash)
	if err != nil {
		// This is a critical error (index must be stopped with an error).
		// Orphan blocks must be canceled no matter what.
		return
	}

	hashesSet := make(map[string]bool)
	for _, operation := range operationsSet.Ops {
		hashesSet[operation.TxHash] = true
	}

	hashes := ""
	for hash, _ := range hashesSet {
		hashes += fmt.Sprintf(",'%s'", hash)
	}
	hashes = hashes[1:] // drop first comma

	ch, err = clickhouse.BeginOperation()
	if err != nil {
		return
	}
	defer func() {
		_ = ch.Rollback()
	}()

	query := fmt.Sprintf("ALTER TABLE %s DELETE WHERE hash IN (%s)", clickhouse.DataTableName(), hashes)
	_, err = ch.Exec(query)
	if err != nil {
		// This is a critical error (index must be stopped with an error).
		// Orphan blocks must be canceled no matter what.
		return
	}

	// This is a critical error (index must be stopped with an error).
	err = ch.Commit()
	return
}
