package implementation

import (
	"github.com/fasthttp/router"
	"gitlab.com/jaxnet/core/indexer/indexes/operations/core/interfaces"
)

type TXsHistoryIndex struct{}

func (i TXsHistoryIndex) SetHttpHandlers(group *router.Group) {
	interfaces.InitAPIHandlers(group)
}

func (i TXsHistoryIndex) DefaultFirstBlockForProcessing() (blockNumber int64) {
	return 0
}

func (i TXsHistoryIndex) PrepareDatabase() error {
	return nil
}
