package main

import (
	"gitlab.com/jaxnet/core/indexer/core/fake"
	"gitlab.com/jaxnet/core/indexer/indexes/operations/implementation"
)

func main() {
	fake.Main(&implementation.JaxTXsHistoryIndex{}, &implementation.BtcTXsHistoryIndex{})
}
