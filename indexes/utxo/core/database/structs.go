package database

type UTXO struct {
	TxHash   string `json:"hash"`
	TxOutput uint16 `json:"output"`
	Amount   int64  `json:"amount"`
	PKScript string `json:"pkScript"`
}

type LockedUTXO struct {
	TxHash         string `json:"hash"`
	TxOutput       uint16 `json:"output"`
	Amount         int64  `json:"amount"`
	UnlockingBlock uint64 `json:"unlockingBlock"`
	Timestamp      int64  `json:"timestamp"`
}
