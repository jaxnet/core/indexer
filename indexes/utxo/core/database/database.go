package database

import (
	"encoding/binary"
	"fmt"
	"github.com/jackc/pgx/v4"
	"gitlab.com/jaxnet/core/indexer/core/database"
	"gitlab.com/jaxnet/core/indexer/indexes/utxo/core/ec"
	"strconv"
	"strings"
)

func SelectUTXOs(strategy, address string, offset, limit, maxAmount int64) (utxos []UTXO, err error) {
	const (
		defaultLimit = 100
		maxLimit     = defaultLimit * 3
	)

	var (
		totalAmountCollected int64 = 0
		sortOrder                  = "ASC"

		rows pgx.Rows
		utxo UTXO
	)

	if limit > maxLimit {
		limit = maxLimit
	}

	utxos = UTXOsPool.Get().([]UTXO)

	if strategy != "smallest-first" {
		sortOrder = "DESC"
	}

	query :=
		"SELECT b.hash, a.tx_output, a.amount, a.pkscript " +
			"FROM %s AS a " +
			"LEFT JOIN hashes AS b ON b.id = a.tx_hash " +
			"LEFT JOIN addresses AS c ON c.id = a.address " +
			"WHERE c.address = '%s' AND a.state = 0 " +
			"ORDER BY a.amount %s, a.tx_hash, a.tx_output LIMIT %d OFFSET %d"

	query = fmt.Sprintf(query, database.DataTableName(), address, sortOrder, limit, offset)
	rows, err = database.QueryTxFree(query)
	defer rows.Close()
	if err != nil {
		return
	}

	var recordsReceived int64 = 0
	for {
		if !rows.Next() {
			break
		}

		values := rows.RawValues()
		utxo.TxHash = string(values[0])
		utxo.TxOutput = uint16(binary.BigEndian.Uint32(values[1])) // postgres returns uint32
		utxo.Amount = int64(binary.BigEndian.Uint64(values[2]))    // postgres returns uin64
		utxo.PKScript = string(values[3])
		utxos = append(utxos, utxo)
		recordsReceived += 1

		totalAmountCollected += utxo.Amount
		if totalAmountCollected >= maxAmount {
			return
		}
	}

	return
}

func SelectOneUTXO(strategy, address, amount string) (utxo UTXO, err error) {
	sortOrder := "ASC"
	if strategy != "smallest-first" {
		sortOrder = "DESC"
	}

	sql :=
		"SELECT b.hash, a.tx_output, a.amount, a.pkscript " +
			"FROM %s AS a " +
			"LEFT JOIN hashes AS b ON b.id = a.tx_hash " +
			"LEFT JOIN addresses AS c ON c.id = a.address " +
			"WHERE c.address = '%s' AND a.state = 0 AND amount >= %s" +
			"ORDER BY a.amount %s LIMIT 1"

	query := fmt.Sprintf(sql, database.DataTableName(), address, amount, sortOrder)
	rows, err := database.QueryTxFree(query)
	defer rows.Close()
	if err != nil {
		return
	}

	if !rows.Next() {
		err = ec.ErrNoUTXOFound
		return
	}

	values := rows.RawValues()
	utxo.TxHash = string(values[0])
	utxo.TxOutput = uint16(binary.BigEndian.Uint32(values[1])) // postgres returns uint32
	utxo.Amount = int64(binary.BigEndian.Uint64(values[2]))    // postgres returns uin64
	utxo.PKScript = string(values[3])
	return
}

func SelectUTXOsByHashes(address string, txHashes []string) (utxos []UTXO, err error) {
	var (
		query string
		rows  pgx.Rows
		utxo  UTXO
	)

	hashesList := ""
	for idx, hash := range txHashes {
		hashesList = hashesList + fmt.Sprintf("'%s'", hash)
		if idx != len(txHashes)-1 {
			hashesList = hashesList + ", "
		}
	}

	if hashesList != "" {
		query = fmt.Sprintf("SELECT c.hash, a.tx_output, a.amount, a.pkscript FROM %s_data AS a "+
			"LEFT JOIN addresses AS b ON a.address = b.id "+
			"LEFT JOIN hashes AS c ON a.tx_hash = c.id "+
			"WHERE b.address = '%s' AND c.hash IN (%s)",
			database.IndexNamePrefix(), address, hashesList)
	} else {
		query = fmt.Sprintf("SELECT c.hash, a.tx_output, a.amount, a.pkscript FROM %s_data AS a "+
			"LEFT JOIN addresses AS b ON a.address = b.id "+
			"LEFT JOIN hashes AS c ON a.tx_hash = c.id "+
			"WHERE b.address = '%s' ",
			database.IndexNamePrefix(), address)
	}

	rows, err = database.QueryTxFree(query)
	defer rows.Close()
	if err != nil {
		return
	}

	utxos = UTXOsPool.Get().([]UTXO)
	for {
		if !rows.Next() {
			break
		}

		values := rows.RawValues()
		utxo.TxHash = string(values[0])
		utxo.TxOutput = uint16(binary.BigEndian.Uint32(values[1])) // postgres returns uint32
		utxo.Amount = int64(binary.BigEndian.Uint64(values[2]))    // postgres returns uin64
		utxo.PKScript = string(values[3])

		utxos = append(utxos, utxo)
	}

	return
}

func MarkUTXOAsUsed(utxos []UTXO, lockPeriod int64) (err error) {
	for _, utxo := range utxos {
		sql :=
			"UPDATE %s SET state = 1, lock_period = %d " +
				"WHERE tx_hash = (SELECT id AS hash FROM hashes WHERE hash = '%s')" +
				"AND tx_output = %s"

		query := fmt.Sprintf(sql, database.DataTableName(), lockPeriod,
			utxo.TxHash, strconv.FormatUint(uint64(utxo.TxOutput), 10))

		_, err = database.ExecTxFree(query)
		if err != nil {
			return
		}
	}
	return
}

func MarkUTXOAsDefected(utxos []UTXO) (err error) {
	for _, utxo := range utxos {
		sql :=
			"UPDATE %s SET state = -2 " +
				"WHERE tx_hash = (SELECT id AS hash FROM hashes WHERE hash = '%s')" +
				"AND tx_output = %s"

		query := fmt.Sprintf(sql,
			database.DataTableName(), utxo.TxHash, strconv.FormatUint(uint64(utxo.TxOutput), 10))

		_, err = database.ExecTxFree(query)
		if err != nil {
			return
		}
	}
	return
}

func GetCurrentBlockHeight() (blockHeight int64, err error) {
	query := fmt.Sprint(
		"SELECT current_block_height FROM {{ current_block_height_table }} LIMIT 1")

	query = strings.Replace(query, "{{ current_block_height_table }}",
		database.AdditionalDataTableName("block_height"), 1)

	var rows pgx.Rows
	rows, err = database.QueryTxFree(query)
	defer rows.Close()
	if err != nil {
		return
	}

	if !rows.Next() {
		err = ec.ErrNoRecordsInDB
		return
	}

	values, err := rows.Values()
	if err != nil {
		return
	}
	blockHeight, ok := values[0].(int64)
	if !ok {
		err = ec.ErrUnexpectedResponseFromDB
		return
	}
	return
}

func SetCurrentBlockHeight(blockHeight int64) (err error) {
	sql := "UPDATE %s SET current_block_height = %d "

	query := fmt.Sprintf(sql, database.AdditionalDataTableName("block_height"), blockHeight)

	_, err = database.ExecTxFree(query)
	return
}

func SelectUTXOsLessEqualThanAmount(address string, amount string) (utxos []UTXO, err error) {
	var (
		rows pgx.Rows
		utxo UTXO
	)

	utxos = UTXOsPool.Get().([]UTXO)

	query :=
		"SELECT b.hash, a.tx_output, a.amount, a.pkscript " +
			"FROM %s AS a " +
			"JOIN hashes AS b ON b.id = a.tx_hash " +
			"JOIN addresses AS c ON c.id = a.address " +
			"WHERE c.address = '%s' AND a.state = 0 AND a.amount <= %s"

	query = fmt.Sprintf(query, database.DataTableName(), address, amount)
	rows, err = database.QueryTxFree(query)
	defer rows.Close()
	if err != nil {
		return
	}

	var recordsReceived int64 = 0
	for {
		if !rows.Next() {
			break
		}

		values := rows.RawValues()
		utxo.TxHash = string(values[0])
		utxo.TxOutput = uint16(binary.BigEndian.Uint32(values[1])) // postgres returns uint32
		utxo.Amount = int64(binary.BigEndian.Uint64(values[2]))    // postgres returns uin64
		utxo.PKScript = string(values[3])
		utxos = append(utxos, utxo)
		recordsReceived += 1

		if recordsReceived >= 100 {
			return
		}
	}

	return
}

func SelectLockedUTXOs(address string, from, to int64) (utxos []LockedUTXO, err error) {
	var (
		rows pgx.Rows
		utxo LockedUTXO
	)

	utxos = make([]LockedUTXO, 0)

	query :=
		"SELECT b.hash, a.tx_output, a.amount, a.lock_period, a.timestamp " +
			"FROM %s AS a " +
			"JOIN hashes AS b ON b.id = a.tx_hash " +
			"JOIN addresses AS c ON c.id = a.address " +
			"WHERE c.address = '%s' AND a.state = 1 AND a.timestamp >= %d AND a.timestamp <= %d " +
			"ORDER BY a.lock_period ASC"

	query = fmt.Sprintf(query, database.DataTableName(), address, from, to)
	rows, err = database.QueryTxFree(query)
	defer rows.Close()
	if err != nil {
		return
	}

	for {
		if !rows.Next() {
			break
		}

		values := rows.RawValues()
		utxo.TxHash = string(values[0])
		utxo.TxOutput = uint16(binary.BigEndian.Uint32(values[1]))
		utxo.Amount = int64(binary.BigEndian.Uint64(values[2])) // postgres returns uin64
		utxo.UnlockingBlock = binary.BigEndian.Uint64(values[3])
		utxo.Timestamp = int64(binary.BigEndian.Uint64(values[4]))
		utxos = append(utxos, utxo)
	}

	return
}

func SelectNextDateLockedUTXO(address string, to int64) (nextDate int64, err error) {
	var (
		rows pgx.Rows
	)

	query :=
		"SELECT a.timestamp " +
			"FROM %s AS a " +
			"JOIN hashes AS b ON b.id = a.tx_hash " +
			"JOIN addresses AS c ON c.id = a.address " +
			"WHERE c.address = '%s' AND a.state = 1 AND a.timestamp > %d " +
			"ORDER BY a.lock_period ASC LIMIT 1"

	query = fmt.Sprintf(query, database.DataTableName(), address, to)
	rows, err = database.QueryTxFree(query)
	defer rows.Close()
	if err != nil {
		return
	}

	if !rows.Next() {
		nextDate = -1
		return
	}

	values := rows.RawValues()
	nextDate = int64(binary.BigEndian.Uint64(values[0]))

	return
}

func SelectPreviousDateLockedUTXO(address string, from int64) (previousDate int64, err error) {
	var (
		rows pgx.Rows
	)

	query :=
		"SELECT a.timestamp " +
			"FROM %s AS a " +
			"JOIN hashes AS b ON b.id = a.tx_hash " +
			"JOIN addresses AS c ON c.id = a.address " +
			"WHERE c.address = '%s' AND a.state = 1 AND a.timestamp < %d " +
			"ORDER BY a.lock_period DESC LIMIT 1"

	query = fmt.Sprintf(query, database.DataTableName(), address, from)
	rows, err = database.QueryTxFree(query)
	defer rows.Close()
	if err != nil {
		return
	}

	if !rows.Next() {
		previousDate = -1
		return
	}

	values := rows.RawValues()
	previousDate = int64(binary.BigEndian.Uint64(values[0]))

	return
}
