package database

import (
	"gitlab.com/jaxnet/core/indexer/indexes/utxo/core/ec"
	"sync"
)

var UTXOsPool = sync.Pool{
	New: func() interface{} { return make([]UTXO, 0, ec.AvgUTXOsCountPerOperation) },
}

func ReturnUTXOsSlotToPoolIfNonNil(utxos []UTXO) {
	if utxos == nil {
		return
	}

	if len(utxos) > ec.AvgUTXOsCountPerOperation {
		// In case if total amount of UTXOs is larger than `maxUTXOsBufferCap`,
		// then size of the underlying array is already greater than max expected value.
		// In this case, to keep memory footprint small - the buffer must be recreated,
		// so this one would be freed by the GC.
		// Otherwise, the process would keep holding very big segment of memory
		// that is not going to be utilized fully (sort of memory leak).
		utxos = make([]UTXO, 0, ec.AvgUTXOsCountPerOperation)

	} else {
		// Underlying array could be simply reset
		// (no memory reallocation would occur, so the GC pressure is minimal)
		utxos = utxos[:0]
	}

	// Return UTXOs placeholder back to the pool
	UTXOsPool.Put(utxos)
}
