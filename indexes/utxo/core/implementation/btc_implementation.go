package implementation

import (
	"encoding/hex"
	"errors"
	"github.com/btcsuite/btcd/blockchain"
	"github.com/btcsuite/btcd/rpcclient"
	"github.com/btcsuite/btcd/wire"
	"github.com/jackc/pgx/v4"
	"gitlab.com/jaxnet/core/indexer/core"
	"gitlab.com/jaxnet/core/indexer/core/blocks_cache"
	"gitlab.com/jaxnet/core/indexer/core/ec"
	"gitlab.com/jaxnet/core/indexer/core/logger"
	"gitlab.com/jaxnet/core/indexer/core/settings"
	db "gitlab.com/jaxnet/core/indexer/indexes/utxo/core/database"
	"gitlab.com/jaxnet/jaxnetd/jaxutil/txutils"
	"time"
)

type BtcNonSpentUTXOsIndex struct {
	NonSpentUTXOsIndex
}

func (i BtcNonSpentUTXOsIndex) InitRPCClient() (*core.AbstractRPCClient, error) {
	return core.InitBTCRPCClient()
}

func (i BtcNonSpentUTXOsIndex) FetchNextBlockFromNode(offset int64, checkPrevBlock bool,
	rpcClient *core.AbstractRPCClient, cacheHandler *blocks_cache.CacheHandler) (
	block *core.AbstractMsgBlock, prevBlockNumber, blockHeight int64, rebuildingChainBlockNumber int64, err error) {
	return core.FetchNextBlockFromBtcNode(offset, checkPrevBlock, rpcClient, cacheHandler)
}

func (i BtcNonSpentUTXOsIndex) ConnectBlock(tx pgx.Tx, block *core.AbstractMsgBlock, blockHeight int64, rpcClient *core.AbstractRPCClient) error {
	btcBlock, err := block.BTCMsgBlock()
	if err != nil {
		return err
	}
	for _, transaction := range btcBlock.Transactions {
		if blockchain.IsCoinBaseTx(transaction) {
			err := i.connectCoinbaseTx(tx, transaction, btcBlock.BlockHash().String(), blockHeight, btcBlock.Header.Timestamp)
			if err != nil {
				return err
			}
		} else {
			err := i.connectRegularTx(tx, transaction, btcBlock.BlockHash().String(), blockHeight, btcBlock.Header.Timestamp)
			if err != nil {
				return err
			}
		}
	}
	btcClient, err := rpcClient.BTCRPCClient()
	if err != nil {
		return err
	}
	err = i.updateStatusOfUsedUTXO(tx, btcClient, blockHeight)
	if err != nil {
		return err
	}
	// todo : uncomment me after testing
	//err = i.checkNotMarkedUtxosInMempool(tx, btcClient, blockHeight)
	//if err != nil {
	//	return err
	//}
	err = db.SetCurrentBlockHeight(blockHeight)
	if err != nil {
		return err
	}
	return nil
}

func (i BtcNonSpentUTXOsIndex) DisconnectBlock(tx pgx.Tx, block *core.AbstractMsgBlock, blockHeight int64, rpcClient *core.AbstractRPCClient) error {
	btcBlock, err := block.BTCMsgBlock()
	if err != nil {
		return err
	}
	btcClient, err := rpcClient.BTCRPCClient()
	if err != nil {
		return err
	}
	for _, transaction := range btcBlock.Transactions {
		err := i.disconnectRegularTx(tx, transaction, btcClient, btcBlock.Header.Timestamp)
		if err != nil {
			return err
		}
	}
	err = db.SetCurrentBlockHeight(blockHeight)
	if err != nil {
		return err
	}
	return nil
}

func (i BtcNonSpentUTXOsIndex) connectCoinbaseTx(tx pgx.Tx, transaction *wire.MsgTx, blockHash string,
	blockHeight int64, blockTimestamp time.Time) (err error) {
	for idx, txOut := range transaction.TxOut {
		if txOut.Value == 0 {
			continue
		}
		decodedOutput, err := txutils.DecodeScript(txOut.PkScript, settings.NetParams)
		if err != nil {
			return err
		}
		if len(decodedOutput.Addresses) == 0 {
			logger.Log.Err(errors.New("Invalid output PkScript "+hex.EncodeToString(txOut.PkScript))).Int64(
				"block-height", blockHeight).Str(
				"block-hash", blockHash).Str(
				"tx-hash", transaction.TxHash().String()).Int(
				"tx-out-idx", idx).Msg("Coinbase Tx")
			continue
		}
		err = i.addUTXOWithLock(tx, transaction.TxHash().String(), decodedOutput.Addresses[0], idx, txOut.Value,
			hex.EncodeToString(txOut.PkScript), blockHeight+int64(settings.NetParams.CoinbaseMaturity), blockTimestamp)
		if err != nil {
			return err
		}
	}
	return
}

func (i BtcNonSpentUTXOsIndex) connectRegularTx(tx pgx.Tx, transaction *wire.MsgTx, blockHash string,
	blockHeight int64, blockTimestamp time.Time) (err error) {
	for _, txIn := range transaction.TxIn {
		if txIn.PreviousOutPoint.Hash.String() == ec.EmptyHash {
			continue
		}
		err := i.removeUTXO(tx, txIn.PreviousOutPoint.Hash.String(), txIn.PreviousOutPoint.Index)
		if err != nil {
			return err
		}
	}
	for idx, txOut := range transaction.TxOut {
		if txOut.Value == 0 {
			continue
		}
		decodedOutput, err := txutils.DecodeScript(txOut.PkScript, settings.NetParams)
		if err != nil {
			return err
		}
		if len(decodedOutput.Addresses) == 0 {
			logger.Log.Err(errors.New("Invalid output PkScript "+hex.EncodeToString(txOut.PkScript))).Int64(
				"block-height", blockHeight).Str(
				"block-hash", blockHash).Str(
				"tx-hash", transaction.TxHash().String()).Int(
				"tx-out-idx", idx).Msg("Regular Tx")
			continue
		}
		err = i.addUTXO(tx, transaction.TxHash().String(), decodedOutput.Addresses[0], idx, txOut.Value,
			hex.EncodeToString(txOut.PkScript), blockTimestamp)
		if err != nil {
			return err
		}
	}
	return
}

func (i BtcNonSpentUTXOsIndex) disconnectRegularTx(tx pgx.Tx, transaction *wire.MsgTx, rpcClient *rpcclient.Client,
	blockTimestamp time.Time) (err error) {
	for idx, _ := range transaction.TxOut {
		err := i.removeUTXO(tx, transaction.TxHash().String(), uint32(idx))
		if err != nil {
			return err
		}
	}
	for _, txIn := range transaction.TxIn {
		if txIn.PreviousOutPoint.Hash.String() == ec.EmptyHash {
			continue
		}
		previousTransaction, err := rpcClient.GetTxDetails(&txIn.PreviousOutPoint.Hash, true)
		if err != nil {
			return err
		}

		relatedPevTxOut := previousTransaction.Vout[txIn.PreviousOutPoint.Index]
		if len(relatedPevTxOut.ScriptPubKey.Addresses) == 0 {
			return errors.New("Invalid output PkScript")
		}

		err = i.addUTXO(tx, txIn.PreviousOutPoint.Hash.String(), relatedPevTxOut.ScriptPubKey.Addresses[0],
			int(txIn.PreviousOutPoint.Index), relatedPevTxOut.PreciseValue, relatedPevTxOut.ScriptPubKey.Hex, blockTimestamp)
		if err != nil {
			return err
		}
	}
	return
}

func (i BtcNonSpentUTXOsIndex) updateStatusOfUsedUTXO(tx pgx.Tx, rpcClient *rpcclient.Client, blockHeight int64) (err error) {
	usedUTXOs, err := i.getUsedUTXO(tx, blockHeight)
	if err != nil {
		return
	}

	if len(usedUTXOs) == 0 {
		return
	}

	utxosInMemPool, err := rpcClient.GetMempoolUTXOs()
	if err != nil {
		return
	}

	for _, usedUTXO := range usedUTXOs {
		isUtxoInMemPool := false
		for _, utxoInMemPool := range utxosInMemPool {
			if usedUTXO.TxHash == utxoInMemPool.UTXOHash && usedUTXO.Index == utxoInMemPool.UTXOIndex {
				isUtxoInMemPool = true
				break
			}
		}
		if isUtxoInMemPool {
			err = i.removeUTXO(tx, usedUTXO.TxHash, usedUTXO.Index)
			if err != nil {
				return
			}
		} else {
			err = i.markUTXOAsNotUsed(tx, usedUTXO.TxHash, usedUTXO.Index)
			if err != nil {
				return
			}
		}
	}

	return
}

func (i BtcNonSpentUTXOsIndex) checkNotMarkedUtxosInMempool(tx pgx.Tx, rpcClient *rpcclient.Client, blockHeight int64) (err error) {
	utxosInMemPool, err := rpcClient.GetMempoolUTXOs()
	if err != nil {
		return
	}

	for _, utxoInMemPool := range utxosInMemPool {
		nonSpent, err := i.checkIfUtxoNonSpent(tx, utxoInMemPool.UTXOHash, int(utxoInMemPool.UTXOIndex))
		if err != nil {
			return err
		}

		if nonSpent {
			err = i.markUTXOAsUsed(tx, utxoInMemPool.UTXOHash, utxoInMemPool.UTXOIndex, blockHeight+1)
			if err != nil {
				return err
			}
		}
	}

	return
}
