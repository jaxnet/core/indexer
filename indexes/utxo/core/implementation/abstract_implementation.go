package implementation

import (
	"github.com/fasthttp/router"
	"github.com/jackc/pgx/v4"
	"gitlab.com/jaxnet/core/indexer/core/database"
	"gitlab.com/jaxnet/core/indexer/indexes/utxo/core/interfaces"
	"strconv"
	"strings"
	"time"
)

type NonSpentUTXOsIndex struct{}

func (i NonSpentUTXOsIndex) SetHttpHandlers(group *router.Group) {
	interfaces.InitAPIHandlers(group)
}

func (i NonSpentUTXOsIndex) DefaultFirstBlockForProcessing() (blockNumber int64) {
	return 0
}

// PrepareDatabase makes some preliminary actions with database before indexing begins.
// According to core development policies, UTXOs index must ignore UTXOs from genesis blocks,
// so blocks processing must begin from the block with the serial ID == 1.
func (i NonSpentUTXOsIndex) PrepareDatabase() error {
	query := "SELECT 1 FROM {{ block_height_table }} "
	query = strings.Replace(query, "{{ block_height_table }}", database.AdditionalDataTableName("block_height"), 1)
	rows, err := database.QueryTxFree(query)
	defer rows.Close()
	if err != nil {
		return err
	}

	if !rows.Next() {
		query = "INSERT INTO {{ block_height_table }} (current_block_height) VALUES ( {{ initial_block_height }} ) "
		query = strings.Replace(query, "{{ block_height_table }}", database.AdditionalDataTableName("block_height"), 1)
		query = strings.Replace(query, "{{ initial_block_height }}", strconv.FormatInt(i.DefaultFirstBlockForProcessing(), 10), 1)
		_, err = database.ExecTxFree(query)
		if err != nil {
			return err
		}
	}
	return nil
}

func (i NonSpentUTXOsIndex) removeUTXO(tx pgx.Tx, txHash string, txOutput uint32) (err error) {
	query := "DELETE FROM {{ not_spent_utxo_table }} WHERE tx_hash = " +
		"(SELECT id FROM hashes WHERE hash = '{{ hash }}') AND tx_output = {{ tx_output }}"
	query = strings.Replace(query, "{{ not_spent_utxo_table }}", database.DataTableName(), 1)
	query = strings.Replace(query, "{{ hash }}", txHash, 1)
	query = strings.Replace(query, "{{ tx_output }}", strconv.FormatUint(uint64(txOutput), 10), 1)
	_, err = database.Exec(tx, query)
	if err != nil {
		return
	}
	return
}

func (i NonSpentUTXOsIndex) addUTXO(tx pgx.Tx, txHash string, address string, outIdx int, amount int64,
	pkScript string, timestamp time.Time) (err error) {

	query := "INSERT INTO hashes (hash) VALUES ( '{{ hash }}' ) ON CONFLICT DO NOTHING"
	query = strings.Replace(query, "{{ hash }}", txHash, 1)
	_, err = database.Exec(tx, query)
	if err != nil {
		return
	}

	query = "INSERT INTO addresses (address) VALUES ( '{{ address }}' ) ON CONFLICT DO NOTHING"
	query = strings.Replace(query, "{{ address }}", address, 1)
	_, err = database.Exec(tx, query)
	if err != nil {
		return
	}

	query =
		"INSERT INTO {{ not_spent_utxo_table }} (address, tx_hash, amount, tx_output, pkscript, timestamp) VALUES(" +
			"(SELECT id AS address FROM addresses WHERE address = '{{ address }}'), " +
			"(SELECT id AS hash FROM hashes WHERE hash = '{{ hash }}'), " +
			strconv.FormatInt(amount, 10) + ", " + strconv.Itoa(outIdx) + ", " +
			"'{{ pkscript }}', '{{ timestamp }}')"

	query = strings.Replace(query, "{{ not_spent_utxo_table }}", database.DataTableName(), 1)
	query = strings.Replace(query, "{{ address }}", address, 1)
	query = strings.Replace(query, "{{ hash }}", txHash, 1)
	query = strings.Replace(query, "{{ pkscript }}", pkScript, 1)
	query = strings.Replace(query, "{{ timestamp }}", strconv.FormatInt(timestamp.Unix(), 10), 1)
	_, err = database.Exec(tx, query)
	if err != nil {
		return
	}
	return
}

func (i NonSpentUTXOsIndex) addUTXOWithLock(tx pgx.Tx, txHash string, address string, outIdx int,
	amount int64, pkScript string, lock int64, timestamp time.Time) (err error) {

	query := "INSERT INTO hashes (hash) VALUES ( '{{ hash }}' ) ON CONFLICT DO NOTHING"
	query = strings.Replace(query, "{{ hash }}", txHash, 1)
	_, err = database.Exec(tx, query)
	if err != nil {
		return
	}

	query = "INSERT INTO addresses (address) VALUES ( '{{ address }}' ) ON CONFLICT DO NOTHING"
	query = strings.Replace(query, "{{ address }}", address, 1)
	_, err = database.Exec(tx, query)
	if err != nil {
		return
	}

	query =
		"INSERT INTO {{ not_spent_utxo_table }} (address, tx_hash, amount, tx_output, pkscript, state, lock_period, timestamp) VALUES(" +
			"(SELECT id AS address FROM addresses WHERE address = '{{ address }}'), " +
			"(SELECT id AS hash FROM hashes WHERE hash = '{{ hash }}'), " +
			strconv.FormatInt(amount, 10) + ", " + strconv.Itoa(outIdx) + ", " +
			"'{{ pkscript }}', 1, " + strconv.FormatInt(lock, 10) + ", '{{ timestamp }}')"

	query = strings.Replace(query, "{{ not_spent_utxo_table }}", database.DataTableName(), 1)
	query = strings.Replace(query, "{{ address }}", address, 1)
	query = strings.Replace(query, "{{ hash }}", txHash, 1)
	query = strings.Replace(query, "{{ pkscript }}", pkScript, 1)
	query = strings.Replace(query, "{{ timestamp }}", strconv.FormatInt(timestamp.Unix(), 10), 1)
	_, err = database.Exec(tx, query)
	if err != nil {
		return
	}
	return
}

func (i NonSpentUTXOsIndex) checkIfUtxoPresent(tx pgx.Tx, txHash string, txOutNum int) (found bool, err error) {
	query := "SELECT 1 FROM {{ not_spent_utxo_table }} WHERE tx_hash = (SELECT id AS hash FROM hashes " +
		"WHERE hash = '{{ hash }}') AND tx_output = {{ tx_output }}"
	query = strings.Replace(query, "{{ not_spent_utxo_table }}", database.DataTableName(), 1)
	query = strings.Replace(query, "{{ hash }}", txHash, 1)
	query = strings.Replace(query, "{{ tx_output }}", strconv.Itoa(txOutNum), 1)
	rows, err := database.Query(tx, query)
	defer rows.Close()
	if err != nil {
		return
	}

	found = rows.Next()
	return
}

func (i NonSpentUTXOsIndex) checkIfUtxoNonSpent(tx pgx.Tx, txHash string, txOutNum int) (nonSpent bool, err error) {
	query := "SELECT state FROM {{ not_spent_utxo_table }} WHERE tx_hash = (SELECT id AS hash FROM hashes " +
		"WHERE hash = '{{ hash }}') AND tx_output = {{ tx_output }}"
	query = strings.Replace(query, "{{ not_spent_utxo_table }}", database.DataTableName(), 1)
	query = strings.Replace(query, "{{ hash }}", txHash, 1)
	query = strings.Replace(query, "{{ tx_output }}", strconv.Itoa(txOutNum), 1)
	rows, err := database.Query(tx, query)
	defer rows.Close()
	if err != nil {
		return
	}

	if !rows.Next() {
		return
	}
	values, err := rows.Values()
	if err != nil {
		return false, err
	}
	state, ok := values[1].(int32)
	if !ok {
		return false, err
	}
	nonSpent = state == 0
	return
}

func (i NonSpentUTXOsIndex) markUTXOAsNotUsed(tx pgx.Tx, hash string, outNum uint32) (err error) {
	query :=
		"UPDATE {{ not_spent_utxo_table }} SET state = 0 " +
			"WHERE tx_hash = (SELECT id AS hash FROM hashes WHERE hash = '{{ hash }}')" +
			"AND tx_output = {{ tx_output }}"

	query = strings.Replace(query, "{{ not_spent_utxo_table }}", database.DataTableName(), 1)
	query = strings.Replace(query, "{{ hash }}", hash, 1)
	query = strings.Replace(query, "{{ tx_output }}", strconv.FormatUint(uint64(outNum), 10), 1)
	_, err = database.Exec(tx, query)
	if err != nil {
		return
	}
	return
}

func (i NonSpentUTXOsIndex) markUTXOAsUsed(tx pgx.Tx, hash string, outNum uint32, lockPeriod int64) (err error) {
	query :=
		"UPDATE {{ not_spent_utxo_table }} SET state = 1, lock_period = {{lock_period}} " +
			"WHERE tx_hash = (SELECT id AS hash FROM hashes WHERE hash = '{{ hash }}')" +
			"AND tx_output = {{ tx_output }}"

	query = strings.Replace(query, "{{ not_spent_utxo_table }}", database.DataTableName(), 1)
	query = strings.Replace(query, "{{lock_period}}", strconv.FormatInt(lockPeriod, 10), 1)
	query = strings.Replace(query, "{{ hash }}", hash, 1)
	query = strings.Replace(query, "{{ tx_output }}", strconv.FormatUint(uint64(outNum), 10), 1)
	_, err = database.Exec(tx, query)
	if err != nil {
		return
	}
	return
}

type UTXO struct {
	TxHash string
	Index  uint32
}

func (i NonSpentUTXOsIndex) getUsedUTXO(tx pgx.Tx, currentBlockHeight int64) (usedUTXO []UTXO, err error) {
	query := "SELECT b.hash, a.tx_output FROM {{ not_spent_utxo_table }} AS a " +
		"JOIN hashes AS b ON b.id = a.tx_hash WHERE a.state = 1 AND a.lock_period < " +
		strconv.FormatInt(currentBlockHeight, 10)
	query = strings.Replace(query, "{{ not_spent_utxo_table }}", database.DataTableName(), 1)
	rows, err := database.Query(tx, query)
	defer rows.Close()
	if err != nil {
		return
	}

	for rows.Next() {
		values, err := rows.Values()
		if err != nil {
			return nil, err
		}
		hash, ok := values[0].(string)
		if !ok {
			return nil, err
		}
		outputNum, ok := values[1].(int32)
		if !ok {
			return nil, err
		}
		usedUTXO = append(usedUTXO, UTXO{TxHash: hash, Index: uint32(outputNum)})
	}
	return
}
