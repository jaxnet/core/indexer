package implementation

import (
	"encoding/hex"
	"errors"
	"github.com/jackc/pgx/v4"
	"gitlab.com/jaxnet/core/indexer/core"
	"gitlab.com/jaxnet/core/indexer/core/blocks_cache"
	"gitlab.com/jaxnet/core/indexer/core/ec"
	"gitlab.com/jaxnet/core/indexer/core/logger"
	"gitlab.com/jaxnet/core/indexer/core/settings"
	db "gitlab.com/jaxnet/core/indexer/indexes/utxo/core/database"
	"gitlab.com/jaxnet/jaxnetd/jaxutil/txutils"
	"gitlab.com/jaxnet/jaxnetd/network/rpcclient"
	"gitlab.com/jaxnet/jaxnetd/node/chaindata"
	"gitlab.com/jaxnet/jaxnetd/txscript"
	"gitlab.com/jaxnet/jaxnetd/types"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
	"time"
)

type JaxNonSpentUTXOsIndex struct {
	NonSpentUTXOsIndex
}

func (i JaxNonSpentUTXOsIndex) InitRPCClient() (*core.AbstractRPCClient, error) {
	return core.InitJaxRPCClient()
}

func (i JaxNonSpentUTXOsIndex) DefaultFirstBlockForProcessing() (blockNumber int64) {
	// in shards processing utxos starts from 1 st block
	if settings.Conf.Blockchain.ID == 0 {
		blockNumber = 0
	} else {
		blockNumber = 1
	}
	return
}

func (i JaxNonSpentUTXOsIndex) FetchNextBlockFromNode(offset int64, checkPrevBlock bool,
	rpcClient *core.AbstractRPCClient, cacheHandler *blocks_cache.CacheHandler) (
	block *core.AbstractMsgBlock, prevBlockNumber, blockHeight int64, rebuildingChainBlockNumber int64, err error) {
	return core.FetchNextBlockFromJaxNode(offset, checkPrevBlock, rpcClient, cacheHandler)
}

func (i JaxNonSpentUTXOsIndex) ConnectBlock(tx pgx.Tx, block *core.AbstractMsgBlock, blockHeight int64, rpcClient *core.AbstractRPCClient) error {
	jaxBlock, err := block.JaxMsgBlock()
	if err != nil {
		return err
	}
	for _, transaction := range jaxBlock.Transactions {
		if chaindata.IsCoinBaseTx(transaction) {
			err := i.connectCoinbaseTx(tx, transaction, jaxBlock.BlockHash().String(), blockHeight, jaxBlock.Header.Timestamp())
			if err != nil {
				return err
			}
		} else if transaction.Version == ec.CrossShardTxVersion {
			err := i.connectCrossShardTx(tx, transaction, jaxBlock.BlockHash().String(), blockHeight, jaxBlock.Header.Timestamp())
			if err != nil {
				return err
			}
		} else {
			err := i.connectRegularTx(tx, transaction, jaxBlock.BlockHash().String(), blockHeight, jaxBlock.Header.Timestamp())
			if err != nil {
				return err
			}
		}
	}
	jaxClient, err := rpcClient.JaxRPCClient()
	if err != nil {
		return err
	}
	err = i.updateStatusOfUsedUTXO(tx, jaxClient, blockHeight)
	if err != nil {
		return err
	}
	// todo : uncomment me after testing
	//err = i.checkNotMarkedUtxosInMempool(tx, jaxClient, blockHeight)
	//if err != nil {
	//	return err
	//}
	err = db.SetCurrentBlockHeight(blockHeight)
	if err != nil {
		return err
	}
	return nil
}

func (i JaxNonSpentUTXOsIndex) DisconnectBlock(tx pgx.Tx, block *core.AbstractMsgBlock, blockHeight int64, rpcClient *core.AbstractRPCClient) error {
	jaxBlock, err := block.JaxMsgBlock()
	if err != nil {
		return err
	}
	jaxClient, err := rpcClient.JaxRPCClient()
	if err != nil {
		return err
	}
	for _, transaction := range jaxBlock.Transactions {
		if transaction.Version == ec.CrossShardTxVersion {
			err := i.disconnectCrossShardTx(tx, transaction, jaxClient, jaxBlock.Header.Timestamp())
			if err != nil {
				return err
			}
		} else {
			err := i.disconnectRegularTx(tx, transaction, jaxClient, jaxBlock.Header.Timestamp())
			if err != nil {
				return err
			}
		}
	}
	err = db.SetCurrentBlockHeight(blockHeight)
	if err != nil {
		return err
	}
	return nil
}

func (i JaxNonSpentUTXOsIndex) connectCoinbaseTx(tx pgx.Tx, transaction *wire.MsgTx, blockHash string,
	blockHeight int64, blockTimestamp time.Time) (err error) {
	for idx, txOut := range transaction.TxOut {
		if txOut.Value == 0 {
			continue
		}
		decodedOutput, err := txutils.DecodeScript(txOut.PkScript, settings.NetParams)
		if err != nil {
			return err
		}
		if decodedOutput.Type == txscript.EADAddressTy.String() {
			continue
		}
		if decodedOutput.Type == types.JaxBurnAddrTy {
			continue
		}
		if len(decodedOutput.Addresses) == 0 {
			logger.Log.Err(errors.New("Invalid output PkScript "+hex.EncodeToString(txOut.PkScript))).Int64(
				"block-height", blockHeight).Str(
				"block-hash", blockHash).Str(
				"tx-hash", transaction.TxHash().String()).Int(
				"tx-out-idx", idx).Msg("Coinbase tx")
			continue
		}
		lockPeriod := int64(settings.NetParams.CoinbaseMaturity)
		if decodedOutput.Type == txscript.HTLCScriptTy.String() {
			lockTime, err := txscript.ExtractHTLCLockTime(txOut.PkScript)
			if err != nil {
				return err
			}
			lockPeriod = int64(lockTime)
		}
		err = i.addUTXOWithLock(tx, transaction.TxHash().String(), decodedOutput.Addresses[0], idx, txOut.Value,
			hex.EncodeToString(txOut.PkScript), blockHeight+lockPeriod, blockTimestamp)
		if err != nil {
			return err
		}
	}
	return
}

func (i JaxNonSpentUTXOsIndex) connectRegularTx(tx pgx.Tx, transaction *wire.MsgTx, blockHash string,
	blockHeight int64, blockTimestamp time.Time) (err error) {
	for _, txIn := range transaction.TxIn {
		err := i.removeUTXO(tx, txIn.PreviousOutPoint.Hash.String(), txIn.PreviousOutPoint.Index)
		if err != nil {
			return err
		}
	}
	for idx, txOut := range transaction.TxOut {
		if txOut.Value == 0 {
			continue
		}
		decodedOutput, err := txutils.DecodeScript(txOut.PkScript, settings.NetParams)
		if err != nil {
			return err
		}
		if decodedOutput.Type == txscript.EADAddressTy.String() {
			continue
		}
		if len(decodedOutput.Addresses) == 0 {
			logger.Log.Err(errors.New("Invalid output PkScript "+hex.EncodeToString(txOut.PkScript))).Int64(
				"block-height", blockHeight).Str(
				"block-hash", blockHash).Str(
				"tx-hash", transaction.TxHash().String()).Int(
				"tx-out-idx", idx).Msg("Regular tx")
			continue
		}
		if decodedOutput.Type == txscript.HTLCScriptTy.String() {
			lockTime, err := txscript.ExtractHTLCLockTime(txOut.PkScript)
			if err != nil {
				return err
			}
			err = i.addUTXOWithLock(tx, transaction.TxHash().String(), decodedOutput.Addresses[0], idx, txOut.Value,
				hex.EncodeToString(txOut.PkScript), blockHeight+int64(lockTime), blockTimestamp)
			if err != nil {
				return err
			}
		} else {
			err = i.addUTXO(tx, transaction.TxHash().String(), decodedOutput.Addresses[0], idx, txOut.Value,
				hex.EncodeToString(txOut.PkScript), blockTimestamp)
			if err != nil {
				return err
			}
		}
	}
	return
}

func (i JaxNonSpentUTXOsIndex) disconnectRegularTx(tx pgx.Tx, transaction *wire.MsgTx, rpcClient *rpcclient.Client,
	blockTimestamp time.Time) (err error) {
	for idx := range transaction.TxOut {
		err := i.removeUTXO(tx, transaction.TxHash().String(), uint32(idx))
		if err != nil {
			return err
		}
	}
	for _, txIn := range transaction.TxIn {
		if txIn.PreviousOutPoint.Hash.String() == ec.EmptyHash {
			continue
		}
		previousTransaction, err := rpcClient.GetTxDetails(&txIn.PreviousOutPoint.Hash, true)
		if err != nil {
			return err
		}

		relatedPevTxOut := previousTransaction.Vout[txIn.PreviousOutPoint.Index]
		if len(relatedPevTxOut.ScriptPubKey.Addresses) == 0 {
			return errors.New("invalid output PkScript")
		}

		err = i.addUTXO(tx, txIn.PreviousOutPoint.Hash.String(), relatedPevTxOut.ScriptPubKey.Addresses[0],
			int(txIn.PreviousOutPoint.Index), relatedPevTxOut.PreciseValue, relatedPevTxOut.ScriptPubKey.Hex, blockTimestamp)
		if err != nil {
			return err
		}
	}
	return
}

func (i JaxNonSpentUTXOsIndex) connectCrossShardTx(tx pgx.Tx, transaction *wire.MsgTx, blockHash string,
	blockHeight int64, blockTimestamp time.Time) (err error) {
	for idx, txIn := range transaction.TxIn {
		isUtxoFound, err := i.checkIfUtxoPresent(tx, txIn.PreviousOutPoint.Hash.String(), int(txIn.PreviousOutPoint.Index))
		if err != nil {
			return err
		}
		if isUtxoFound {
			err = i.removeUTXO(tx, txIn.PreviousOutPoint.Hash.String(), txIn.PreviousOutPoint.Index)
			if err != nil {
				return err
			}
			decodedOutput, err := txutils.DecodeScript(transaction.TxOut[idx].PkScript, settings.NetParams)
			if err != nil {
				return err
			}
			if len(decodedOutput.Addresses) == 0 {
				logger.Log.Err(errors.New("Invalid output PkScript "+hex.EncodeToString(transaction.TxOut[idx].PkScript))).Int64(
					"block-height", blockHeight).Str(
					"block-hash", blockHash).Str(
					"tx-hash", transaction.TxHash().String()).Int(
					"tx-out-idx", idx).Msg("Cross shard tx")
				continue
			}
			err = i.addUTXO(tx, transaction.TxHash().String(), decodedOutput.Addresses[0], idx,
				transaction.TxOut[idx].Value, hex.EncodeToString(transaction.TxOut[idx].PkScript), blockTimestamp)
			if err != nil {
				return err
			}
		}
	}
	return
}

func (i JaxNonSpentUTXOsIndex) disconnectCrossShardTx(tx pgx.Tx, transaction *wire.MsgTx, rpcClient *rpcclient.Client,
	blockTimestamp time.Time) (err error) {
	for idx := range transaction.TxOut {
		isUtxoFound, err := i.checkIfUtxoPresent(tx, transaction.TxHash().String(), idx)
		if err != nil {
			return err
		}
		if isUtxoFound {
			txIn := transaction.TxIn[idx]
			previousTransaction, err := rpcClient.GetTxDetails(&txIn.PreviousOutPoint.Hash, true)
			if err != nil {
				return err
			}

			relatedPevTxOut := previousTransaction.Vout[txIn.PreviousOutPoint.Index]
			if len(relatedPevTxOut.ScriptPubKey.Addresses) == 0 {
				return errors.New("invalid output PkScript")
			}

			err = i.removeUTXO(tx, transaction.TxHash().String(), uint32(idx))
			if err != nil {
				return err
			}

			err = i.addUTXO(tx, txIn.PreviousOutPoint.Hash.String(), relatedPevTxOut.ScriptPubKey.Addresses[0],
				int(txIn.PreviousOutPoint.Index), relatedPevTxOut.PreciseValue, relatedPevTxOut.ScriptPubKey.Hex, blockTimestamp)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (i JaxNonSpentUTXOsIndex) updateStatusOfUsedUTXO(tx pgx.Tx, rpcClient *rpcclient.Client, blockHeight int64) (err error) {
	usedUTXOs, err := i.getUsedUTXO(tx, blockHeight)
	if err != nil {
		return
	}

	if len(usedUTXOs) == 0 {
		return
	}

	utxosInMemPool, err := rpcClient.GetMempoolUTXOs()
	if err != nil {
		return
	}

	for _, usedUTXO := range usedUTXOs {
		isUtxoInMemPool := false
		for _, utxoInMemPool := range utxosInMemPool {
			if usedUTXO.TxHash == utxoInMemPool.UTXOHash && usedUTXO.Index == utxoInMemPool.UTXOIndex {
				isUtxoInMemPool = true
				break
			}
		}
		if isUtxoInMemPool {
			err = i.removeUTXO(tx, usedUTXO.TxHash, usedUTXO.Index)
			if err != nil {
				return
			}
		} else {
			err = i.markUTXOAsNotUsed(tx, usedUTXO.TxHash, usedUTXO.Index)
			if err != nil {
				return
			}
		}
	}

	return
}

func (i JaxNonSpentUTXOsIndex) checkNotMarkedUtxosInMempool(tx pgx.Tx, rpcClient *rpcclient.Client, blockHeight int64) (err error) {
	utxosInMemPool, err := rpcClient.GetMempoolUTXOs()
	if err != nil {
		return
	}

	for _, utxoInMemPool := range utxosInMemPool {
		nonSpent, err := i.checkIfUtxoNonSpent(tx, utxoInMemPool.UTXOHash, int(utxoInMemPool.UTXOIndex))
		if err != nil {
			return err
		}

		if nonSpent {
			err = i.markUTXOAsUsed(tx, utxoInMemPool.UTXOHash, utxoInMemPool.UTXOIndex, blockHeight+1)
			if err != nil {
				return err
			}
		}
	}

	return
}
