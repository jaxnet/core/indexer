package ec

import "errors"

var (
	ErrNoUTXOFound              = errors.New("no UTXO found")
	ErrNoRecordsInDB            = errors.New("there are no records")
	ErrUnexpectedResponseFromDB = errors.New("unexpected response from the database")
)
