package ec

const (
	// By default, index fetches UTXOs round-by-round and calculates running total.
	// DefaultRecordsLimit defines the size of one portion of data that is going to be fetched from the database.
	DefaultRecordsLimit = 32

	// AvgUTXOsCountPerOperation defines expected amount of UTXOs per operation.
	// This should not be used a strict limit, but as a tuning value for various internal memory operations.
	AvgUTXOsCountPerOperation = 8

	RegularUTXOLockPeriod = 3
)
