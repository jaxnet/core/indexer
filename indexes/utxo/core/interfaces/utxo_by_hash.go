package interfaces

import (
	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/core/indexer/core/interfaces/http"
	"gitlab.com/jaxnet/core/indexer/indexes/utxo/core/database"
	"gitlab.com/jaxnet/core/indexer/indexes/utxo/core/ec"
)

func handleGetUTXOsFilteredByHashRequest(ctx *fasthttp.RequestCtx) {
	address := string(ctx.QueryArgs().Peek("address"))
	err := validateAddressNotEmpty(address)
	if err != nil {
		http.DiscloseError(err, ctx, fasthttp.StatusBadRequest)
		return
	}

	hashesBytes := ctx.QueryArgs().PeekMulti("tx_hash")
	var hashes []string
	for _, hashB := range hashesBytes {
		hashes = append(hashes, string(hashB))
	}

	utxos, err := database.SelectUTXOsByHashes(address, hashes)
	if err != nil && err != ec.ErrNoUTXOFound {
		http.HandleErrorAndHideDetails(err, ctx)
		return
	}

	defer database.ReturnUTXOsSlotToPoolIfNonNil(utxos)
	err = writeUTXOs(ctx, utxos)
	if err != nil {
		http.HandleErrorAndHideDetails(err, ctx)
		return
	}
}
