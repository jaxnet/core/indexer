package interfaces

import (
	jsoniter "github.com/json-iterator/go"
	"github.com/valyala/fasthttp"
	database2 "gitlab.com/jaxnet/core/indexer/indexes/utxo/core/database"
)

func writeUTXOs(ctx *fasthttp.RequestCtx, utxos []database2.UTXO) (err error) {
	data, err := jsoniter.Marshal(utxos)
	if err != nil {
		return
	}

	_, err = ctx.Write(data)
	if err != nil {
		return
	}

	ctx.Response.Header.SetContentType("application/json")
	return
}

func writeLockedUTXOs(ctx *fasthttp.RequestCtx, utxos []database2.LockedUTXO, nextDate, previousDate int64) (err error) {
	type NearestDatesType struct {
		Next     int64 `json:"next"`
		Previous int64 `json:"previous"`
	}

	type Response struct {
		LockedUTXOs  []database2.LockedUTXO `json:"lockedUtxos"`
		NearestDates NearestDatesType       `json:"nearestDates"`
	}

	response := Response{
		LockedUTXOs: utxos,
		NearestDates: NearestDatesType{
			Next:     nextDate,
			Previous: previousDate,
		},
	}

	data, err := jsoniter.Marshal(response)
	if err != nil {
		return
	}

	_, err = ctx.Write(data)
	if err != nil {
		return
	}

	ctx.Response.Header.SetContentType("application/json")
	return
}
