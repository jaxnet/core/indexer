package interfaces

import (
	"errors"
	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/core/indexer/core/interfaces/http"
	"gitlab.com/jaxnet/core/indexer/core/utils"
	"gitlab.com/jaxnet/core/indexer/indexes/utxo/core/database"
	"time"
)

func handleGetLockedUTXOsRequest(ctx *fasthttp.RequestCtx) {
	address := string(ctx.QueryArgs().Peek("address"))
	err := validateAddressNotEmpty(address)
	if err != nil {
		http.DiscloseError(err, ctx, fasthttp.StatusBadRequest)
		return
	}

	from := string(ctx.QueryArgs().Peek("from"))
	to := string(ctx.QueryArgs().Peek("to"))
	if from == "" || to == "" {
		err = errors.New("arguments `from` and `to` must be always specified together")
		http.DiscloseError(err, ctx, fasthttp.StatusBadRequest)
		return
	}

	utxos, err := requestLockedUTXOs(address, from, to)
	if err != nil {
		http.HandleErrorAndHideDetails(err, ctx)
		return
	}

	nextDate, err := getNextDateUTXO(address, to)
	if err != nil {
		http.HandleErrorAndHideDetails(err, ctx)
		return
	}

	prevDate, err := getPreviousDateUTXO(address, from)
	if err != nil {
		http.HandleErrorAndHideDetails(err, ctx)
		return
	}

	err = writeLockedUTXOs(ctx, utxos, nextDate, prevDate)
	if err != nil {
		http.HandleErrorAndHideDetails(err, ctx)
		return
	}
}

func requestLockedUTXOs(address, from, to string) (utxos []database.LockedUTXO, err error) {
	// By default, (if there is no amount) - return all UTXOs available (for max. possible amount).
	var (
		timeFrom time.Time
		timeTo   time.Time
	)

	timeFrom, err = time.Parse(time.RFC3339, from)
	if err != nil {
		return
	}
	timeFrom = utils.ToIndexInternalTime(timeFrom)

	timeTo, err = time.Parse(time.RFC3339, to)
	if err != nil {
		return
	}
	timeTo = utils.ToIndexInternalTime(timeTo)

	return database.SelectLockedUTXOs(address, timeFrom.Unix(), timeTo.Unix())
}

func getNextDateUTXO(address, to string) (nextDate int64, err error) {
	var (
		timeTo time.Time
	)

	timeTo, err = time.Parse(time.RFC3339, to)
	if err != nil {
		return
	}
	timeTo = utils.ToIndexInternalTime(timeTo)

	return database.SelectNextDateLockedUTXO(address, timeTo.Unix())
}

func getPreviousDateUTXO(address, to string) (previousDate int64, err error) {
	var (
		timeFrom time.Time
	)

	timeFrom, err = time.Parse(time.RFC3339, to)
	if err != nil {
		return
	}
	timeFrom = utils.ToIndexInternalTime(timeFrom)

	return database.SelectPreviousDateLockedUTXO(address, timeFrom.Unix())
}
