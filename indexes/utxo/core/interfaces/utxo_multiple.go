package interfaces

import (
	"errors"
	"fmt"
	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/core/indexer/core/interfaces/http"
	"gitlab.com/jaxnet/core/indexer/indexes/utxo/core/database"
	"gitlab.com/jaxnet/core/indexer/indexes/utxo/core/ec"
	"math"
	"strconv"
)

func handleGetUTXOsRequest(ctx *fasthttp.RequestCtx) {
	address := string(ctx.QueryArgs().Peek("address"))
	err := validateAddressNotEmpty(address)
	if err != nil {
		http.DiscloseError(err, ctx, fasthttp.StatusBadRequest)
		return
	}

	strategy := string(ctx.QueryArgs().Peek("strategy"))

	// Notice: could be empty.
	// Would be replaced by the max(int64) by default.
	amount := string(ctx.QueryArgs().Peek("amount"))

	// Notice: could be empty.
	// Would be replaced by the max(int64) and 50 by default.
	limit := string(ctx.QueryArgs().Peek("limit"))

	// Notice: could be empty.
	// Would be replaced by the max(int64) and 50 by default.
	offset := string(ctx.QueryArgs().Peek("offset"))

	// Notice: could be empty.
	pubKeyStr := string(ctx.QueryArgs().Peek("pub_key"))

	// Notice: could be empty.
	signatureStr := string(ctx.QueryArgs().Peek("signature"))

	sig, pubKey, err := parsePubKeyAndSignature(pubKeyStr, signatureStr, address)
	if err != nil {
		http.DiscloseError(err, ctx, fasthttp.StatusBadRequest)
		return
	}
	returnedUTXOSShouldBeLocked := sig != nil && pubKey != nil

	write := func(strategy string) {
		utxos, err := requestUTXOs(strategy, address, amount, limit, offset)
		if err != nil {
			http.HandleErrorAndHideDetails(err, ctx)
			return
		}

		defer func() {
			if utxos == nil {
				return
			}

			if returnedUTXOSShouldBeLocked {
				blockHeight, err := database.GetCurrentBlockHeight()
				if err != nil {
					http.HandleErrorAndHideDetails(err, ctx)
					return
				}
				err = database.MarkUTXOAsUsed(utxos, blockHeight+ec.RegularUTXOLockPeriod)
				if err != nil {
					http.HandleErrorAndHideDetails(err, ctx)
					return
				}
			}

			database.ReturnUTXOsSlotToPoolIfNonNil(utxos)
		}()

		err = writeUTXOs(ctx, utxos)
		if err != nil {
			http.HandleErrorAndHideDetails(err, ctx)
			return
		}

		if returnedUTXOSShouldBeLocked {
			blockHeight, err := database.GetCurrentBlockHeight()
			if err != nil {
				http.HandleErrorAndHideDetails(err, ctx)
				return
			}
			err = database.MarkUTXOAsUsed(utxos, blockHeight+ec.RegularUTXOLockPeriod)
			if err != nil {
				http.HandleErrorAndHideDetails(err, ctx)
				return
			}
		}
	}

	switch strategy {
	case "smallest-first":
		write("smallest-first")

	case "biggest-first":
		write("biggest-first")

	default:
		msg := fmt.Sprint("Invalid strategy, available options: `smallest-first`, `biggest-first`")
		http.DiscloseError(errors.New(msg), ctx, fasthttp.StatusBadRequest)
		return
	}
}

func requestUTXOs(strategy, address, amount, limit, offset string) (utxos []database.UTXO, err error) {
	// By default, (if there is no amount) - return all UTXOs available (for max. possible amount).
	var (
		maxAmount int64 = math.MaxInt64
		iLimit    int64 = 100
		iOffset   int64 = 0
	)

	if amount != "" {
		maxAmount, err = strconv.ParseInt(amount, 10, 64)
		if err != nil {
			return
		}
	}

	if limit != "" {
		iLimit, err = strconv.ParseInt(limit, 10, 64)
		if err != nil {
			return
		}
	}

	if offset != "" {
		iOffset, err = strconv.ParseInt(offset, 10, 64)
		if err != nil {
			return
		}
	}

	if strategy == "smallest-first" {
		return database.SelectUTXOs("smallest-first", address, iOffset, iLimit, maxAmount)

	} else {
		return database.SelectUTXOs("biggest-first", address, iOffset, iLimit, maxAmount)
	}
}
