package interfaces

import (
	"encoding/binary"
	"errors"
	"fmt"
	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/core/indexer/core/database"
	"gitlab.com/jaxnet/core/indexer/core/interfaces/http"
	"gitlab.com/jaxnet/core/indexer/core/logger"
)

type BalanceResponse struct {
	Balance       int64 `json:"balance"`
	BalanceLocked int64 `json:"balanceLocked"`
}

func handleGetBalanceRequest(ctx *fasthttp.RequestCtx) {
	address := string(ctx.QueryArgs().Peek("address"))
	err := validateAddressNotEmpty(address)
	if err != nil {
		http.DiscloseError(err, ctx, fasthttp.StatusBadRequest)
		return
	}

	var response BalanceResponse

	logger.Log.Debug().Str("address", address).Msg("handleGetBalanceRequest")
	currentBlockNum, err := getCurrentBlockHeight()
	if err != nil {
		http.HandleErrorAndHideDetails(err, ctx)
		return
	}
	logger.Log.Debug().Int64("current-block-num", currentBlockNum).Msg("")

	balance, err := getBalance(address, currentBlockNum)
	if err != nil {
		http.HandleErrorAndHideDetails(err, ctx)
		return
	}

	lockedBalance, err := getLockedBalance(address, currentBlockNum)
	if err != nil {
		http.HandleErrorAndHideDetails(err, ctx)
		return
	}

	response.Balance = balance
	response.BalanceLocked = lockedBalance

	err = http.WriteJSONResponse(response, ctx)
	if err != nil {
		http.HandleErrorAndHideDetails(err, ctx)
		return
	}
}

func getCurrentBlockNum() (blockNum int64, err error) {
	sql := "SELECT block_number FROM %s LIMIT 1"
	queryBlockNum := fmt.Sprintf(sql, database.BlocksForProcessingTableName())
	rowsBlockNum, err := database.QueryTxFree(queryBlockNum)
	defer rowsBlockNum.Close()
	if err != nil {
		return
	}

	if !rowsBlockNum.Next() {
		err = errors.New("database has not returned the required data")
		return
	}
	values := rowsBlockNum.RawValues()
	blockNum = int64(binary.BigEndian.Uint64(values[0]))
	return
}

func getCurrentBlockHeight() (blockHeight int64, err error) {
	sql := "SELECT current_block_height FROM %s LIMIT 1"
	queryBlockNum := fmt.Sprintf(sql, database.AdditionalDataTableName("block_height"))
	rowsBlockNum, err := database.QueryTxFree(queryBlockNum)
	defer rowsBlockNum.Close()
	if err != nil {
		return
	}

	if !rowsBlockNum.Next() {
		err = errors.New("database has not returned the required data")
		return
	}
	values := rowsBlockNum.RawValues()
	blockHeight = int64(binary.BigEndian.Uint64(values[0]))
	return
}

func getBalance(address string, currentBlockNum int64) (balance int64, err error) {
	sql :=
		"SELECT coalesce(sum(amount), 0)::bigint as balance " +
			"FROM %s AS a " +
			"JOIN addresses AS c ON c.id = a.address " +
			"WHERE c.address = '%s' AND state >= -1 AND lock_period < %d"
	queryBalance := fmt.Sprintf(sql, database.DataTableName(), address, currentBlockNum)
	rowsBalance, err := database.QueryTxFree(queryBalance)
	defer rowsBalance.Close()
	if err != nil {
		return
	}
	if !rowsBalance.Next() {
		err = errors.New("database has not returned the required data")
		return
	}
	values := rowsBalance.RawValues()
	balance = int64(binary.BigEndian.Uint64(values[0]))
	return
}

func getLockedBalance(address string, currentBlockNum int64) (lockedBalance int64, err error) {
	sql :=
		"SELECT coalesce(sum(amount), 0)::bigint as balance " +
			"FROM %s AS a " +
			"JOIN addresses AS c ON c.id = a.address " +
			"WHERE c.address = '%s' AND a.state >= 0 AND a.lock_period >= %d"
	queryBalanceLock := fmt.Sprintf(sql, database.DataTableName(), address, currentBlockNum)
	rowsBalanceLock, err := database.QueryTxFree(queryBalanceLock)
	defer rowsBalanceLock.Close()
	if err != nil {
		return
	}
	if !rowsBalanceLock.Next() {
		err = errors.New("database has not returned the required data")
		return
	}
	values := rowsBalanceLock.RawValues()
	lockedBalance = int64(binary.BigEndian.Uint64(values[0]))
	return
}
