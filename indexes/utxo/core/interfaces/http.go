package interfaces

import (
	"github.com/fasthttp/router"
)

func InitAPIHandlers(root *router.Group) {
	group := root.Group("utxos/")
	group.GET("non-spent/", handleGetUTXOsRequest)
	group.GET("balance/", handleGetBalanceRequest)
	group.GET("non-spent/count/", handleGetCountUTXOsRequest)
	group.GET("non-spent/one/", handleGetUTXOsOneRequest)
	group.GET("non-spent/by-hash/", handleGetUTXOsFilteredByHashRequest)
	group.POST("deactivate/", handleDeactivateNonSpentUTXORequest)
	group.POST("lock/", handleLockNonSpentUTXORequest)
	group.GET("non-spent/less-equal/", handleGetUTXOsLessEqualThanAmount)
	group.GET("locked/", handleGetLockedUTXOsRequest)
}
