package interfaces

import (
	"encoding/json"
	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/core/indexer/core/interfaces/http"
	"gitlab.com/jaxnet/core/indexer/indexes/utxo/core/database"
)

func handleDeactivateNonSpentUTXORequest(ctx *fasthttp.RequestCtx) {
	var requestData []database.UTXO

	err := json.Unmarshal(ctx.Request.Body(), &requestData)
	if err != nil {
		http.DiscloseError(err, ctx, fasthttp.StatusBadRequest)
		return
	}

	err = database.MarkUTXOAsDefected(requestData)
	if err != nil {
		http.HandleErrorAndHideDetails(err, ctx)
	}
}
