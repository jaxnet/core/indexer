package interfaces

import (
	"encoding/hex"
	"fmt"
	"gitlab.com/jaxnet/core/indexer/core/ec"
	"gitlab.com/jaxnet/core/indexer/core/settings"
	"gitlab.com/jaxnet/jaxnetd/btcec"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
)

func validateAddressNotEmpty(address string) (err error) {
	if address == "" {
		return fmt.Errorf("parameter `address` can't be empty: %w", ec.ErrValidation)
	}

	return
}

func parsePubKey(pubKey string) (pubKeyAddr *jaxutil.AddressPubKey, err error) {
	pubKeyBytes, err := hex.DecodeString(pubKey)
	if err != nil {
		return
	}
	pubKeyAddr, err = jaxutil.NewAddressPubKey(pubKeyBytes, settings.NetParams)
	return
}

func parseSignature(signature string) (sig *btcec.Signature, err error) {
	signatureBytes, err := hex.DecodeString(signature)
	if err != nil {
		return
	}
	sig, err = btcec.ParseSignature(signatureBytes, btcec.S256())
	return
}
