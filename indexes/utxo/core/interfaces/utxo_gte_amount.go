package interfaces

import (
	"errors"
	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/core/indexer/core/interfaces/http"
	"gitlab.com/jaxnet/core/indexer/indexes/utxo/core/database"
	"gitlab.com/jaxnet/core/indexer/indexes/utxo/core/ec"
	"strconv"
)

func handleGetUTXOsOneRequest(ctx *fasthttp.RequestCtx) {
	address := string(ctx.QueryArgs().Peek("address"))
	err := validateAddressNotEmpty(address)
	if err != nil {
		http.DiscloseError(err, ctx, fasthttp.StatusBadRequest)
		return
	}

	strategy := string(ctx.QueryArgs().Peek("strategy"))
	if strategy != "biggest-first" {
		strategy = "smallest-first"
	}

	amount := string(ctx.QueryArgs().Peek("amount"))
	if amount != "" {
		_, err = strconv.ParseInt(amount, 10, 64)
		if err != nil {
			return
		}
	} else {
		err = errors.New("amount couldn't be empty")
		return
	}

	// Notice: could be empty.
	pubKeyStr := string(ctx.QueryArgs().Peek("pub_key"))

	// Notice: could be empty.
	signatureStr := string(ctx.QueryArgs().Peek("signature"))

	sig, pubKey, err := parsePubKeyAndSignature(pubKeyStr, signatureStr, address)
	if err != nil {
		http.DiscloseError(err, ctx, fasthttp.StatusBadRequest)
		return
	}
	returnedUTXOSShouldBeLocked := sig != nil && pubKey != nil

	utxo, err := database.SelectOneUTXO(strategy, address, amount)
	if err == ec.ErrNoUTXOFound {
		http.DiscloseError(err, ctx, fasthttp.StatusNotFound)
		return

	} else if err != nil {
		http.HandleErrorAndHideDetails(err, ctx)
		return
	}

	err = writeUTXOs(ctx, []database.UTXO{utxo})
	if err != nil {
		http.HandleErrorAndHideDetails(err, ctx)
		return
	}

	if returnedUTXOSShouldBeLocked {
		blockHeight, err := database.GetCurrentBlockHeight()
		if err != nil {
			http.HandleErrorAndHideDetails(err, ctx)
			return
		}
		err = database.MarkUTXOAsUsed([]database.UTXO{utxo}, blockHeight+ec.RegularUTXOLockPeriod)
		if err != nil {
			http.HandleErrorAndHideDetails(err, ctx)
			return
		}
	}
}
