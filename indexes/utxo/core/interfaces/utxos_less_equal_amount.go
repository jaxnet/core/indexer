package interfaces

import (
	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/core/indexer/core/interfaces/http"
	"gitlab.com/jaxnet/core/indexer/indexes/utxo/core/database"
	"strconv"
)

func handleGetUTXOsLessEqualThanAmount(ctx *fasthttp.RequestCtx) {
	address := string(ctx.QueryArgs().Peek("address"))
	err := validateAddressNotEmpty(address)
	if err != nil {
		http.DiscloseError(err, ctx, fasthttp.StatusBadRequest)
		return
	}

	amount := string(ctx.QueryArgs().Peek("amount"))
	_, err = strconv.Atoi(amount)
	if err != nil {
		http.DiscloseError(err, ctx, fasthttp.StatusBadRequest)
		return
	}

	utxos, err := database.SelectUTXOsLessEqualThanAmount(address, amount)
	if err != nil {
		http.HandleErrorAndHideDetails(err, ctx)
		return
	}

	err = writeUTXOs(ctx, utxos)
	if err != nil {
		http.HandleErrorAndHideDetails(err, ctx)
		return
	}
	return
}
