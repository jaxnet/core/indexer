package interfaces

import (
	"encoding/binary"
	"errors"
	"fmt"
	"github.com/jackc/pgx/v4"
	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/core/indexer/core/database"
	"gitlab.com/jaxnet/core/indexer/core/interfaces/http"
)

type CountResponse struct {
	Count uint64 `json:"count"`
}

func handleGetCountUTXOsRequest(ctx *fasthttp.RequestCtx) {
	address := string(ctx.QueryArgs().Peek("address"))
	err := validateAddressNotEmpty(address)
	if err != nil {
		http.DiscloseError(err, ctx, fasthttp.StatusBadRequest)
		return
	}

	var (
		query    string
		rows     pgx.Rows
		response CountResponse
	)

	sql :=
		"SELECT coalesce(count(amount), 0)::bigint as count_utxos " +
			"FROM %s AS a " +
			"JOIN addresses AS c ON c.id = a.address " +
			"WHERE c.address = '%s' AND a.state = 0"

	query = fmt.Sprintf(sql, database.DataTableName(), address)
	rows, err = database.QueryTxFree(query)
	defer rows.Close()
	if err != nil {
		return
	}

	if !rows.Next() {
		err = errors.New("database has not returned the required data")
		http.HandleErrorAndHideDetails(err, ctx)
		return
	}

	values := rows.RawValues()
	response.Count = binary.BigEndian.Uint64(values[0])
	err = http.WriteJSONResponse(response, ctx)
	if err != nil {
		http.HandleErrorAndHideDetails(err, ctx)
		return
	}
}
