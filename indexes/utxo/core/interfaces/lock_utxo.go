package interfaces

import (
	"encoding/json"
	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/core/indexer/core/interfaces/http"
	"gitlab.com/jaxnet/core/indexer/indexes/utxo/core/database"
	"gitlab.com/jaxnet/core/indexer/indexes/utxo/core/ec"
)

func handleLockNonSpentUTXORequest(ctx *fasthttp.RequestCtx) {
	var requestData []database.UTXO

	err := json.Unmarshal(ctx.Request.Body(), &requestData)
	if err != nil {
		http.DiscloseError(err, ctx, fasthttp.StatusBadRequest)
		return
	}

	blockHeight, err := database.GetCurrentBlockHeight()
	if err != nil {
		http.HandleErrorAndHideDetails(err, ctx)
		return
	}

	err = database.MarkUTXOAsUsed(requestData, blockHeight+ec.RegularUTXOLockPeriod)
	if err != nil {
		http.HandleErrorAndHideDetails(err, ctx)
	}
}
