package interfaces

import (
	"errors"
	"gitlab.com/jaxnet/jaxnetd/btcec"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
)

func parsePubKeyAndSignature(pubKeyHex, sigHex, address string) (sig *btcec.Signature, pubKey *btcec.PublicKey, err error) {
	var (
		pubKeyAddress *jaxutil.AddressPubKey
	)

	if pubKeyHex != "" && sigHex != "" {
		pubKeyAddress, err = parsePubKey(pubKeyHex)
		if err != nil {
			return
		}
		pubKey = pubKeyAddress.PubKey()

		if pubKeyAddress.EncodeAddress() != address {
			err = errors.New("public key does not match address")
			return
		}

		sig, err = parseSignature(sigHex)
		if err != nil {
			return
		}

		hash := chainhash.DoubleHashB([]byte(address))
		if !sig.Verify(hash, pubKey) {
			err = errors.New("signature is invalid")
			return
		}
	}

	return
}
