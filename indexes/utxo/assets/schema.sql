
-- WARN: This section contains tables that are common
-- (expected to be used by all indexes, that would share common database instance)
-- Because this tables are common, it is expected, that they would have the same schema in all indexes.
--
-- This tables is declared in the same file, that defines the schema for the particular index.
-- It is safe to write this declaration here: common tables would not be replaced if some of them is already present,
-- so no data corruption or loss is possible.
create table if not exists hashes (
    id bigserial primary key,
    hash text unique
);

-- NOTE: No additional index is needed here.
--       `unique` already instructs postgres to create one.

create table if not exists addresses (
    id bigserial primary key,
    address text unique
);

-- END OF COMMON TABLES SECTION

-- todo: describe the placeholder here.
create table if not exists {{ index_data_table }} (
    address bigint,
    tx_hash bigint,
    tx_output int,
    amount bigint,
    pkscript text,
    -- 0 - normal, 1 - used, -1 - defected
    state smallint default 0,
    lock_period bigint default 0,
    timestamp bigint
);

-- `address` is used in join operation, so it must be covered by an index.
create index if not exists {{ index_data_table }}_address_index on {{ index_data_table }} (address);

-- `amount` is used in sorting operations, so it should be covered by index as well.
create index if not exists {{ index_data_table }}_amount_index on {{ index_data_table }} (amount);

-- `tx_hash` is used in UTXO delete operations, so it should be covered by index as well.
create index if not exists {{ index_data_table }}_tx_hash_index on {{ index_data_table }} (tx_hash);

-- `tx_output` is used in UTXO delete operations, so it should be covered by index as well.
create index if not exists {{ index_data_table }}_tx_output_index on {{ index_data_table }} (tx_output);

create table if not exists {{ index_data_table }}_block_height (
    current_block_height bigint
);

create table if not exists {{ index_blocks_for_processing_table }} (
    id bigserial,
    block_number bigint,
    forward bool,
    check_prev_block bool
);

create table if not exists {{ index_processed_blocks_table }} (
    hash bigint unique
    -- NOTE: No additional index is needed here.
    --       `unique` already instructs postgres to create one.
);

create table if not exists {{ index_blocks_connecting_state }} (
    serial_id bigint,
    prev_serial_id bigint,
    -- 0 - connected, 1 - disconnected
    state smallint
);
