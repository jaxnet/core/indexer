package main

import (
	"gitlab.com/jaxnet/core/indexer/core/fake"
	"gitlab.com/jaxnet/core/indexer/indexes/latest_blocks/core/implementation"
)

// todo: Indexing of the one block must take no more than 7 seconds.
// todo: Ensure node is capable to return block sequentially and using long polling.
//

func main() {
	fake.Main(&implementation.JaxLatestBlocksIndex{}, &implementation.BtcLatestBlocksIndex{})
}
