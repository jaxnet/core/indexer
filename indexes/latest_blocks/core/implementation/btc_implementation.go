package implementation

import (
	"github.com/jackc/pgx/v4"
	"gitlab.com/jaxnet/core/indexer/core"
	"gitlab.com/jaxnet/core/indexer/core/blocks_cache"
)

type BtcLatestBlocksIndex struct {
	LatestBlocksIndex
}

func (i BtcLatestBlocksIndex) InitRPCClient() (*core.AbstractRPCClient, error) {
	return core.InitBTCRPCClient()
}

func (i BtcLatestBlocksIndex) FetchNextBlockFromNode(offset int64, checkPrevBlock bool,
	rpcClient *core.AbstractRPCClient, cacheHandler *blocks_cache.CacheHandler) (
	block *core.AbstractMsgBlock, prevBlockNum, blockHeight int64, rebuildingChainBlockNumber int64, err error) {
	return core.FetchNextBlockFromBtcNode(offset, checkPrevBlock, rpcClient, cacheHandler)
}

func (i BtcLatestBlocksIndex) ConnectBlock(tx pgx.Tx, block *core.AbstractMsgBlock, blockHeight int64, _ *core.AbstractRPCClient) error {
	btcBlock, err := block.BTCMsgBlock()
	if err != nil {
		return err
	}
	err = i.addBlockToIndex(tx, btcBlock.BlockHash().String(), btcBlock.Header.Timestamp, btcBlock.SerializeSize(), blockHeight)
	if err != nil {
		return err
	}

	err = i.removeOutdatedBlocks(tx)
	if err != nil {
		return err
	}
	return nil
}

func (i BtcLatestBlocksIndex) DisconnectBlock(tx pgx.Tx, block *core.AbstractMsgBlock, _ int64, _ *core.AbstractRPCClient) error {
	btcBlock, err := block.BTCMsgBlock()
	if err != nil {
		return err
	}
	err = i.removeBlockFromIndex(tx, btcBlock.BlockHash().String())
	if err != nil {
		return err
	}
	return nil
}
