package implementation

import (
	"github.com/jackc/pgx/v4"
	"gitlab.com/jaxnet/core/indexer/core"
	"gitlab.com/jaxnet/core/indexer/core/blocks_cache"
)

type JaxLatestBlocksIndex struct {
	LatestBlocksIndex
}

func (i JaxLatestBlocksIndex) InitRPCClient() (*core.AbstractRPCClient, error) {
	return core.InitJaxRPCClient()
}

func (i JaxLatestBlocksIndex) FetchNextBlockFromNode(offset int64, checkPrevBlock bool,
	rpcClient *core.AbstractRPCClient, cacheHandler *blocks_cache.CacheHandler) (
	block *core.AbstractMsgBlock, prevBlockNumber, blockHeight int64, rebuildingChainBlockNumber int64, err error) {
	return core.FetchNextBlockFromJaxNode(offset, checkPrevBlock, rpcClient, cacheHandler)
}

func (i JaxLatestBlocksIndex) ConnectBlock(tx pgx.Tx, block *core.AbstractMsgBlock, blockHeight int64, _ *core.AbstractRPCClient) error {
	jaxBlock, err := block.JaxMsgBlock()
	if err != nil {
		return err
	}
	err = i.addBlockToIndex(tx, jaxBlock.BlockHash().String(), jaxBlock.Header.Timestamp(), jaxBlock.SerializeSize(), blockHeight)
	if err != nil {
		return err
	}

	err = i.removeOutdatedBlocks(tx)
	if err != nil {
		return err
	}
	return nil
}

func (i JaxLatestBlocksIndex) DisconnectBlock(tx pgx.Tx, block *core.AbstractMsgBlock, _ int64, _ *core.AbstractRPCClient) error {
	jaxBlock, err := block.JaxMsgBlock()
	if err != nil {
		return err
	}
	err = i.removeBlockFromIndex(tx, jaxBlock.BlockHash().String())
	if err != nil {
		return err
	}
	return nil
}
