package implementation

import (
	"github.com/fasthttp/router"
	"github.com/jackc/pgx/v4"
	"gitlab.com/jaxnet/core/indexer/core/database"
	"gitlab.com/jaxnet/core/indexer/core/settings"
	"gitlab.com/jaxnet/core/indexer/indexes/latest_blocks/core/interfaces"
	"strconv"
	"strings"
	"time"
)

type LatestBlocksIndex struct{}

func (i LatestBlocksIndex) SetHttpHandlers(group *router.Group) {
	interfaces.InitAPIHandlers(group)
}

func (i LatestBlocksIndex) DefaultFirstBlockForProcessing() (blockNumber int64) {
	return 0
}

func (i LatestBlocksIndex) PrepareDatabase() error {
	return nil
}

func (i LatestBlocksIndex) addBlockToIndex(tx pgx.Tx, blockHash string, blockTimestamp time.Time, blockSize int, height int64) (err error) {
	query := "INSERT INTO hashes (hash) VALUES ( '{{ hash }}' ) ON CONFLICT DO NOTHING"
	query = strings.Replace(query, "{{ hash }}", blockHash, 1)
	_, err = database.Exec(tx, query)
	if err != nil {
		return
	}

	query =
		"INSERT INTO {{ latest_blocks_table }} (height, hash, mined, size) VALUES(" +
			"{{ height }}, " +
			"(SELECT id AS hash FROM hashes WHERE hash = '{{ hash }}'), " +
			"'{{ mined }}', {{ size }})"
	query = strings.Replace(query, "{{ latest_blocks_table }}", database.DataTableName(), 1)
	query = strings.Replace(query, "{{ height }}", strconv.FormatInt(height, 10), 1)
	query = strings.Replace(query, "{{ hash }}", blockHash, 1)
	query = strings.Replace(query, "{{ mined }}", blockTimestamp.Format(time.RFC3339), 1)
	query = strings.Replace(query, "{{ size }}", strconv.Itoa(blockSize), 1)
	_, err = database.Exec(tx, query)
	if err != nil {
		return
	}
	return
}

func (i LatestBlocksIndex) removeBlockFromIndex(tx pgx.Tx, blockHash string) (err error) {
	query :=
		"DELETE FROM {{ latest_blocks_table }} WHERE hash = " +
			"(SELECT id AS hash FROM hashes WHERE hash = '{{ hash }}')"

	query = strings.Replace(query, "{{ latest_blocks_table }}", database.DataTableName(), 1)
	query = strings.Replace(query, "{{ hash }}", blockHash, 1)
	_, err = database.Exec(tx, query)
	if err != nil {
		return
	}
	return
}

func (i LatestBlocksIndex) removeOutdatedBlocks(tx pgx.Tx) (err error) {
	query := "DELETE FROM {{ latest_blocks_table }} WHERE id < " +
		"(SELECT id FROM {{ latest_blocks_table }} ORDER BY ID DESC OFFSET {{ blocks_limit }} LIMIT 1)"
	query = strings.Replace(query, "{{ latest_blocks_table }}", database.DataTableName(), 2)
	query = strings.Replace(query, "{{ blocks_limit }}",
		strconv.FormatUint(uint64(settings.Conf.CntRecordsSavedInDB), 10), 1)
	_, err = database.Exec(tx, query)
	if err != nil {
		return
	}
	return
}
