package interfaces

import (
	jsoniter "github.com/json-iterator/go"
	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/core/indexer/core/database"
	"gitlab.com/jaxnet/core/indexer/core/settings"
	"strconv"
	"strings"
	"time"
)

func handleLatestBlocksRequest(ctx *fasthttp.RequestCtx) {

	query := "SELECT h.hash, lb.height, lb.mined, lb.size FROM {{ latest_blocks_table }} AS lb " +
		"LEFT JOIN hashes AS h ON lb.hash = h.id ORDER BY lb.id DESC LIMIT {{ blocks_count }}"
	query = strings.Replace(query, "{{ latest_blocks_table }}", database.DataTableName(), 1)
	query = strings.Replace(query, "{{ blocks_count }}",
		strconv.FormatUint(uint64(settings.Conf.CntRecordsInResponse), 10), 1)
	rows, err := database.QueryTxFree(query)
	defer rows.Close()
	if err != nil {
		reportError(err, ctx)
		return
	}

	var latestBlocks []LatestBlock
	for rows.Next() {

		values, err := rows.Values()
		if err != nil {
			reportError(err, ctx)
			return
		}
		hash, ok := values[0].(string)
		if !ok {
			reportError(err, ctx)
			return
		}
		height, ok := values[1].(int64)
		if !ok {
			reportError(err, ctx)
			return
		}
		mined, ok := values[2].(time.Time)
		if !ok {
			reportError(err, ctx)
			return
		}
		size, ok := values[3].(int64)
		if !ok {
			reportError(err, ctx)
			return
		}
		latestBlocks = append(latestBlocks, LatestBlock{Hash: hash, Height: int(height), Mined: mined, Size: int(size)})
	}

	err = writeLatestBlocks(ctx, latestBlocks)
	if err != nil {
		handleError(err, ctx)
		return
	}
}

func writeLatestBlocks(ctx *fasthttp.RequestCtx, latestBlocks []LatestBlock) (err error) {
	data, err := jsoniter.Marshal(latestBlocks)
	if err != nil {
		return
	}

	_, err = ctx.Write(data)
	if err != nil {
		return
	}

	ctx.Response.Header.SetContentType("application/json")
	return
}
