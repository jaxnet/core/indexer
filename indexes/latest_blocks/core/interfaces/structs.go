package interfaces

import "time"

type LatestBlock struct {
	Hash   string    `json:"hash"`
	Height int       `json:"height"`
	Mined  time.Time `json:"mined"`
	Size   int       `json:"size"`
}
