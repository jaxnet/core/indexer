
-- WARN: This section contains tables that are common
-- (expected to be used by all indexes, that would share common database instance)
-- Because this tables are common, it is expected, that they would have the same schema in all indexes.
--
-- This tables is declared in the same file, that defines the schema for the particular index.
-- It is safe to write this declaration here: common tables would not be replaced if some of them is already present,
-- so no data corruption or loss is possible.
create table if not exists hashes (
    id bigserial primary key,
    hash text unique
);

create table if not exists addresses (
    id bigserial primary key,
    address text unique
);

-- END OF COMMON TABLES SECTION

-- todo: describe the placeholder here.
create table if not exists {{ index_data_table }} (
    address bigint,
    tx_hash bigint,
    direction bool, -- true = incoming; false = outgoing
    output_number int
);

create index if not exists {{ index_data_table }}_address_index on {{ index_data_table }} (address);
create index if not exists {{ index_data_table }}_direction_index on {{ index_data_table }} (direction);
create index if not exists {{ index_data_table }}_tx_hash_index on {{ index_data_table }} (tx_hash);
create index if not exists {{ index_data_table }}_output_number on {{ index_data_table }} (output_number);

create table if not exists {{ index_data_table }}_txs (
    tx_hash bigint,
    input_tx_hash bigint,
    input_tx_idx int
);

create index if not exists {{ index_data_table }}_txs_input_idx_index on {{ index_data_table }}_txs (input_tx_idx);
create index if not exists {{ index_data_table }}_txs_hashes_index on {{ index_data_table }}_txs (tx_hash);

create table if not exists {{ index_blocks_for_processing_table }} (
    id bigserial,
    block_number bigint,
    forward bool,
    check_prev_block bool
);

create table if not exists {{ index_processed_blocks_table }} (
    hash bigint unique
    -- NOTE: No additional index is needed here.
    --       `unique` already instructs postgres to create one.
);

create table if not exists {{ index_blocks_connecting_state }} (
    serial_id bigint,
    prev_serial_id bigint,
    -- 0 - connected, 1 - disconnected
    state smallint
);