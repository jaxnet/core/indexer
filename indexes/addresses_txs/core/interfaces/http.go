package interfaces

import (
	"github.com/fasthttp/router"
	"github.com/valyala/fasthttp"
)

func InitAPIHandlers(root *router.Group) {
	group := root.Group("addresses/transactions")
	group.GET("/", HandleTransactionsOfAddressRequests)
	group.GET("/with-input/", HandleTransactionsOfAddressWithInputRequests)
}

func HandleTransactionsOfAddressRequests(ctx *fasthttp.RequestCtx) {
	switch string(ctx.Method()) {
	case "GET":
		handleGetTransactionsRequest(ctx)
		return

	default:
		ctx.SetStatusCode(fasthttp.StatusMethodNotAllowed)
		return
	}
}

func HandleTransactionsOfAddressWithInputRequests(ctx *fasthttp.RequestCtx) {
	switch string(ctx.Method()) {
	case "GET":
		handleGetTransactionsWithInputRequest(ctx)
		return

	default:
		ctx.SetStatusCode(fasthttp.StatusMethodNotAllowed)
		return
	}
}
