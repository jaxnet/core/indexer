package interfaces

import (
	"fmt"
	"gitlab.com/jaxnet/core/indexer/core/ec"
)

func validateAddressNotEmpty(address string) (err error) {
	if address == "" {
		return fmt.Errorf("parameter `address` can't be empty: %w", ec.ErrValidation)
	}

	return
}

func validateTxHashNotEmpty(txHash string) (err error) {
	if txHash == "" {
		return fmt.Errorf("parameter `tx-input` can't be empty: %w", ec.ErrValidation)
	}

	return
}

func validateTxInputIdxNotEmpty(txInputIdx string) (err error) {
	if txInputIdx == "" {
		return fmt.Errorf("parameter `tx-input-idx` can't be empty: %w", ec.ErrValidation)
	}

	return
}

func validateDirection(direction string) (err error) {
	if direction != "incoming" && direction != "outgoing" {
		err = fmt.Errorf("invalid direction occured, allowed values are: `incoming`, `outgoing`")
		return
	}

	return
}
