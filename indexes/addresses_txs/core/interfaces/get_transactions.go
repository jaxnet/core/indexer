package interfaces

import (
	"fmt"
	"github.com/jackc/pgx/v4"
	jsoniter "github.com/json-iterator/go"
	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/core/indexer/core/database"
	"gitlab.com/jaxnet/core/indexer/core/interfaces/http"
	"strconv"
	"strings"
)

func handleGetTransactionsRequest(ctx *fasthttp.RequestCtx) {
	address := string(ctx.QueryArgs().Peek("address"))
	err := validateAddressNotEmpty(address)
	if err != nil {
		http.DiscloseError(err, ctx, fasthttp.StatusBadRequest)
		return
	}

	direction := string(ctx.QueryArgs().Peek("direction"))
	err = validateDirection(direction)
	if err != nil {
		http.DiscloseError(err, ctx, fasthttp.StatusBadRequest)
		return
	}

	// Notice: could be empty.
	// Would be replaced by the max(int64) and 0 by default.
	limit := string(ctx.QueryArgs().Peek("limit"))

	// Notice: could be empty.
	// Would be replaced by the max(int64) and 50 by default.
	offset := string(ctx.QueryArgs().Peek("offset"))

	txs, err := fetchTransactions(address, direction, limit, offset)
	if err != nil {
		http.HandleErrorAndHideDetails(err, ctx)
		return
	}

	err = writeTXs(ctx, txs)
	if err != nil {
		http.HandleErrorAndHideDetails(err, ctx)
		return
	}
}

func fetchTransactions(address, direction, limit, offset string) (transactions []string, err error) {
	queryArgDirection := "true"
	if direction == "outgoing" {
		queryArgDirection = "false"
	}

	const (
		defaultLimit = 100
		maxLimit     = defaultLimit * 3
	)

	var (
		iLimit  uint64 = 100
		iOffset uint64 = 0
	)

	if limit != "" {
		iLimit, err = strconv.ParseUint(limit, 10, 64)
		if err != nil {
			return
		}
	}
	if iLimit > maxLimit {
		iLimit = maxLimit
	}

	if offset != "" {
		iOffset, err = strconv.ParseUint(offset, 10, 64)
		if err != nil {
			return
		}
	}

	query := fmt.Sprint(
		"SELECT DISTINCT b.hash, b.id FROM {{ index_data_table }} AS a ",
		"LEFT JOIN hashes AS b ON b.id = a.tx_hash ",
		"LEFT JOIN addresses AS c ON c.id = a.address ",
		"WHERE c.address = '", address, "' ",
		"AND direction = ", queryArgDirection,
		" ORDER BY b.id DESC "+
			" LIMIT "+strconv.FormatUint(iLimit, 10),
		" OFFSET "+strconv.FormatUint(iOffset, 10))

	query = strings.Replace(query, "{{ index_data_table }}", database.DataTableName(), 1)

	var rows pgx.Rows
	rows, err = database.QueryTxFree(query)
	defer rows.Close()
	if err != nil {
		return
	}

	transactions = make([]string, 0, 256)
	for {
		if !rows.Next() {
			break
		}

		values := rows.RawValues()
		transactions = append(transactions, string(values[0]))
	}

	return
}

func writeTXs(ctx *fasthttp.RequestCtx, txs []string) (err error) {
	data, err := jsoniter.Marshal(txs)
	if err != nil {
		return
	}

	_, err = ctx.Write(data)
	if err != nil {
		return
	}

	ctx.Response.Header.SetContentType("application/json")
	return
}

func handleGetTransactionsWithInputRequest(ctx *fasthttp.RequestCtx) {
	address := string(ctx.QueryArgs().Peek("address"))
	err := validateAddressNotEmpty(address)
	if err != nil {
		http.DiscloseError(err, ctx, fasthttp.StatusBadRequest)
		return
	}

	txInput := string(ctx.QueryArgs().Peek("tx_input"))
	err = validateTxHashNotEmpty(txInput)
	if err != nil {
		http.DiscloseError(err, ctx, fasthttp.StatusBadRequest)
		return
	}

	txInputIdx := string(ctx.QueryArgs().Peek("tx_input_idx"))
	err = validateTxInputIdxNotEmpty(txInputIdx)
	if err != nil {
		http.DiscloseError(err, ctx, fasthttp.StatusBadRequest)
		return
	}

	txs, err := fetchTransactionWithInput(address, txInput, txInputIdx)
	if err != nil {
		http.HandleErrorAndHideDetails(err, ctx)
		return
	}

	err = writeTXs(ctx, txs)
	if err != nil {
		http.HandleErrorAndHideDetails(err, ctx)
		return
	}
}

func fetchTransactionWithInput(address, txInput, txInputIdx string) (transactions []string, err error) {

	query := fmt.Sprint(
		"SELECT b.hash FROM {{ index_data_table }} AS a ",
		"LEFT JOIN hashes AS b ON b.id = a.tx_hash ",
		"LEFT JOIN addresses AS c ON c.id = a.address ",
		"WHERE c.address = '{{ address }}' ",
		"AND a.direction = false ",
		"AND a.tx_hash IN (SELECT d.tx_hash FROM {{ index_data_table_txs }} AS d ",
		"LEFT JOIN hashes AS e ON e.id = d.input_tx_hash ",
		"WHERE e.hash = '{{ tx_input }}' AND d.input_tx_idx = {{ tx_input_idx }})")

	query = strings.Replace(query, "{{ index_data_table }}", database.DataTableName(), 1)
	query = strings.Replace(query, "{{ index_data_table_txs }}", database.AdditionalDataTableName("txs"), 1)
	query = strings.Replace(query, "{{ address }}", address, 1)
	query = strings.Replace(query, "{{ tx_input }}", txInput, 1)
	query = strings.Replace(query, "{{ tx_input_idx }}", txInputIdx, 1)

	var rows pgx.Rows
	rows, err = database.QueryTxFree(query)
	defer rows.Close()
	if err != nil {
		return
	}

	transactions = make([]string, 0, 256)
	for {
		if !rows.Next() {
			break
		}

		values := rows.RawValues()
		transactions = append(transactions, string(values[0]))
	}

	return
}
