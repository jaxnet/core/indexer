package implementation

import (
	"errors"
	"github.com/fasthttp/router"
	"github.com/jackc/pgx/v4"
	"gitlab.com/jaxnet/core/indexer/core/database"
	"gitlab.com/jaxnet/core/indexer/indexes/addresses_txs/core/interfaces"
	"strconv"
	"strings"
)

type TransactionsIndex struct{}

func (i TransactionsIndex) SetHttpHandlers(group *router.Group) {
	interfaces.InitAPIHandlers(group)
}

func (i TransactionsIndex) DefaultFirstBlockForProcessing() (blockNumber int64) {
	return 0
}

func (i TransactionsIndex) PrepareDatabase() error {
	return nil
}

func (i TransactionsIndex) addInputTransaction(tx pgx.Tx, txHash string, address string, outputNumber int) (err error) {

	query := "INSERT INTO hashes (hash) VALUES ( '{{ hash }}' ) ON CONFLICT DO NOTHING"
	query = strings.Replace(query, "{{ hash }}", txHash, 1)
	_, err = database.Exec(tx, query)
	if err != nil {
		return
	}

	query = "INSERT INTO addresses (address) VALUES ( '{{ address }}' ) ON CONFLICT DO NOTHING"
	query = strings.Replace(query, "{{ address }}", address, 1)
	_, err = database.Exec(tx, query)
	if err != nil {
		return
	}

	query =
		"INSERT INTO {{ transactions_table }} (address, tx_hash, direction, output_number) VALUES(" +
			"(SELECT id AS address FROM addresses WHERE address = '{{ address }}'), " +
			"(SELECT id AS hash FROM hashes WHERE hash = '{{ hash }}'), true, '{{ output_number }}')"

	query = strings.Replace(query, "{{ transactions_table }}", database.DataTableName(), 1)
	query = strings.Replace(query, "{{ address }}", address, 1)
	query = strings.Replace(query, "{{ hash }}", txHash, 1)
	query = strings.Replace(query, "{{ output_number }}", strconv.Itoa(outputNumber), 1)
	_, err = database.Exec(tx, query)
	if err != nil {
		return
	}
	return
}

func (i TransactionsIndex) addOutputTransaction(tx pgx.Tx, txHash string, addressID int64) (err error) {
	query := "INSERT INTO hashes (hash) VALUES ( '{{ hash }}' ) ON CONFLICT DO NOTHING"
	query = strings.Replace(query, "{{ hash }}", txHash, 1)
	_, err = database.Exec(tx, query)
	if err != nil {
		return
	}

	query =
		"INSERT INTO {{ transactions_table }} (address, tx_hash, direction) VALUES(" +
			"'{{ address }}', (SELECT id AS hash FROM hashes WHERE hash = '{{ hash }}'), false)"

	query = strings.Replace(query, "{{ transactions_table }}", database.DataTableName(), 1)
	query = strings.Replace(query, "{{ address }}", strconv.FormatInt(addressID, 10), 1)
	query = strings.Replace(query, "{{ hash }}", txHash, 1)
	_, err = database.Exec(tx, query)
	if err != nil {
		return
	}
	return
}

func (i TransactionsIndex) checkIfTxPresent(tx pgx.Tx, txInPrevOutPointHash string, txInPrevOutPointIdx uint32) (
	found bool, addressID int64, err error) {
	query := "SELECT address FROM {{ transactions_table }} WHERE tx_hash = (SELECT id AS hash FROM hashes " +
		"WHERE hash = '{{ hash }}') AND direction = true AND output_number = '{{ output_number }}'"
	query = strings.Replace(query, "{{ transactions_table }}", database.DataTableName(), 1)
	query = strings.Replace(query, "{{ hash }}", txInPrevOutPointHash, 1)
	query = strings.Replace(query, "{{ output_number }}", strconv.FormatUint(uint64(txInPrevOutPointIdx), 10), 1)
	rows, err := database.Query(tx, query)
	defer rows.Close()
	if err != nil {
		return
	}

	found = rows.Next()
	if !found {
		return
	}
	values, err := rows.Values()
	if err != nil {
		return
	}
	addressID, ok := values[0].(int64)
	if !ok {
		err = errors.New("unexpected response from the database")
		return
	}
	return
}

func (i TransactionsIndex) addTransactionWithInput(tx pgx.Tx, txHash, txInputHash string, txInputNum uint32) (err error) {
	query := "INSERT INTO hashes (hash) VALUES ( '{{ hash }}' ), ( '{{ input_hash }}' ) ON CONFLICT DO NOTHING"
	query = strings.Replace(query, "{{ hash }}", txHash, 1)
	query = strings.Replace(query, "{{ input_hash }}", txInputHash, 1)
	_, err = database.Exec(tx, query)
	if err != nil {
		return
	}

	query =
		"INSERT INTO {{ transactions_table_txs }} (tx_hash, input_tx_hash, input_tx_idx) VALUES(" +
			"(SELECT id AS hash FROM hashes WHERE hash = '{{ tx_hash }}'), " +
			"(SELECT id AS hash FROM hashes WHERE hash = '{{ tx_input_hash }}'), {{ tx_input_num }})"

	query = strings.Replace(query, "{{ transactions_table_txs }}", database.AdditionalDataTableName("txs"), 1)
	query = strings.Replace(query, "{{ tx_hash }}", txHash, 1)
	query = strings.Replace(query, "{{ tx_input_hash }}", txInputHash, 1)
	query = strings.Replace(query, "{{ tx_input_num }}", strconv.FormatUint(uint64(txInputNum), 10), 1)
	_, err = database.Exec(tx, query)
	if err != nil {
		return
	}
	return
}

func (i TransactionsIndex) removeTransactionFromIndex(tx pgx.Tx, txHash string) (err error) {
	query :=
		"DELETE FROM {{ transactions_table }} WHERE tx_hash = " +
			"(SELECT id AS hash FROM hashes WHERE hash = '{{ hash }}')"

	query = strings.Replace(query, "{{ transactions_table }}", database.DataTableName(), 1)
	query = strings.Replace(query, "{{ hash }}", txHash, 1)
	_, err = database.Exec(tx, query)
	if err != nil {
		return
	}

	query =
		"DELETE FROM {{ transactions_table_txs }} WHERE tx_hash = " +
			"(SELECT id AS hash FROM hashes WHERE hash = '{{ hash }}')"

	query = strings.Replace(query, "{{ transactions_table_txs }}", database.AdditionalDataTableName("txs"), 1)
	query = strings.Replace(query, "{{ hash }}", txHash, 1)
	_, err = database.Exec(tx, query)
	if err != nil {
		return
	}
	return
}
