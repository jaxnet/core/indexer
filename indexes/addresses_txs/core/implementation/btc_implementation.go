package implementation

import (
	"encoding/hex"
	"errors"
	"github.com/btcsuite/btcd/wire"
	"github.com/jackc/pgx/v4"
	"gitlab.com/jaxnet/core/indexer/core"
	"gitlab.com/jaxnet/core/indexer/core/blocks_cache"
	"gitlab.com/jaxnet/core/indexer/core/ec"
	"gitlab.com/jaxnet/core/indexer/core/logger"
	"gitlab.com/jaxnet/core/indexer/core/settings"
	"gitlab.com/jaxnet/jaxnetd/jaxutil/txutils"
)

type BtcTransactionsIndex struct {
	TransactionsIndex
}

func (i BtcTransactionsIndex) InitRPCClient() (*core.AbstractRPCClient, error) {
	return core.InitBTCRPCClient()
}

func (i BtcTransactionsIndex) FetchNextBlockFromNode(offset int64, checkPrevBlock bool,
	rpcClient *core.AbstractRPCClient, cacheHandler *blocks_cache.CacheHandler) (
	block *core.AbstractMsgBlock, prevBlockNumber, blockHeight int64, rebuildingChainBlockNumber int64, err error) {
	return core.FetchNextBlockFromBtcNode(offset, checkPrevBlock, rpcClient, cacheHandler)
}

func (i BtcTransactionsIndex) ConnectBlock(tx pgx.Tx, block *core.AbstractMsgBlock, blockHeight int64, rpcClient *core.AbstractRPCClient) error {
	btcBlock, err := block.BTCMsgBlock()
	if err != nil {
		return err
	}
	for _, transaction := range btcBlock.Transactions {
		err := i.connectRegularTx(tx, transaction, btcBlock.BlockHash().String(), blockHeight)
		if err != nil {
			return err
		}
	}
	return nil
}

func (i BtcTransactionsIndex) DisconnectBlock(tx pgx.Tx, block *core.AbstractMsgBlock, _ int64, rpcClient *core.AbstractRPCClient) error {
	btcBlock, err := block.BTCMsgBlock()
	if err != nil {
		return err
	}
	for _, transaction := range btcBlock.Transactions {
		err := i.removeTransactionFromIndex(tx, transaction.TxHash().String())
		if err != nil {
			return err
		}
	}
	return nil
}

func (i BtcTransactionsIndex) connectRegularTx(tx pgx.Tx, transaction *wire.MsgTx, blockHash string, blockHeight int64) (err error) {
	for _, txIn := range transaction.TxIn {
		if txIn.PreviousOutPoint.Hash.String() == ec.EmptyHash {
			continue
		}
		isTxFound, addressID, err := i.checkIfTxPresent(tx, txIn.PreviousOutPoint.Hash.String(), txIn.PreviousOutPoint.Index)
		if err != nil {
			return err
		}
		if !isTxFound {
			continue
		}
		err = i.addOutputTransaction(tx, transaction.TxHash().String(), addressID)
		if err != nil {
			return err
		}

		err = i.addTransactionWithInput(tx, transaction.TxHash().String(),
			txIn.PreviousOutPoint.Hash.String(), txIn.PreviousOutPoint.Index)
		if err != nil {
			return err
		}
	}
	for idx, txOut := range transaction.TxOut {
		decodedOutput, err := txutils.DecodeScript(txOut.PkScript, settings.NetParams)
		if err != nil {
			return err
		}
		if len(decodedOutput.Addresses) == 0 {
			logger.Log.Err(errors.New("Invalid output PkScript "+hex.EncodeToString(txOut.PkScript))).Int64(
				"block-height", blockHeight).Str(
				"block-hash", blockHash).Str(
				"tx-hash", transaction.TxHash().String()).Int(
				"tx-out-idx", idx).Msg("")
			continue
		}
		err = i.addInputTransaction(tx, transaction.TxHash().String(), decodedOutput.Addresses[0], idx)
		if err != nil {
			return err
		}
	}
	return
}
