package implementation

import (
	"encoding/hex"
	"errors"
	"github.com/jackc/pgx/v4"
	"gitlab.com/jaxnet/core/indexer/core"
	"gitlab.com/jaxnet/core/indexer/core/blocks_cache"
	"gitlab.com/jaxnet/core/indexer/core/ec"
	"gitlab.com/jaxnet/core/indexer/core/logger"
	"gitlab.com/jaxnet/core/indexer/core/settings"
	"gitlab.com/jaxnet/jaxnetd/jaxutil/txutils"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

type JaxTransactionsIndex struct {
	TransactionsIndex
}

func (i JaxTransactionsIndex) InitRPCClient() (*core.AbstractRPCClient, error) {
	return core.InitJaxRPCClient()
}

func (i JaxTransactionsIndex) FetchNextBlockFromNode(offset int64, checkPrevBlock bool,
	rpcClient *core.AbstractRPCClient, cacheHandler *blocks_cache.CacheHandler) (
	block *core.AbstractMsgBlock, prevBlockNumber, blockHeight int64, rebuildingChainBlockNumber int64, err error) {
	return core.FetchNextBlockFromJaxNode(offset, checkPrevBlock, rpcClient, cacheHandler)
}

func (i JaxTransactionsIndex) ConnectBlock(tx pgx.Tx, block *core.AbstractMsgBlock, blockHeight int64, rpcClient *core.AbstractRPCClient) error {
	jaxBlock, err := block.JaxMsgBlock()
	if err != nil {
		return err
	}
	for _, transaction := range jaxBlock.Transactions {
		if transaction.Version == ec.CrossShardTxVersion {
			err := i.connectCrossShardTx(tx, transaction, jaxBlock.BlockHash().String(), blockHeight)
			if err != nil {
				return err
			}
		} else {
			err := i.connectRegularTx(tx, transaction, jaxBlock.BlockHash().String(), blockHeight)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (i JaxTransactionsIndex) DisconnectBlock(tx pgx.Tx, block *core.AbstractMsgBlock, _ int64, rpcClient *core.AbstractRPCClient) error {
	jaxBlock, err := block.JaxMsgBlock()
	if err != nil {
		return err
	}
	for _, transaction := range jaxBlock.Transactions {
		err := i.removeTransactionFromIndex(tx, transaction.TxHash().String())
		if err != nil {
			return err
		}
	}
	return nil
}

func (i JaxTransactionsIndex) connectCrossShardTx(tx pgx.Tx, transaction *wire.MsgTx, blockHash string, blockHeight int64) (err error) {
	for idx, txIn := range transaction.TxIn {
		isTxFound, addressID, err := i.checkIfTxPresent(tx, txIn.PreviousOutPoint.Hash.String(), txIn.PreviousOutPoint.Index)
		if err != nil {
			return err
		}
		if !isTxFound {
			continue
		}
		err = i.addOutputTransaction(tx, transaction.TxHash().String(), addressID)
		if err != nil {
			return err
		}
		decodedOutput, err := txutils.DecodeScript(transaction.TxOut[idx].PkScript, settings.NetParams)
		if err != nil {
			return err
		}
		if len(decodedOutput.Addresses) == 0 {
			logger.Log.Err(errors.New("Invalid output PkScript "+hex.EncodeToString(transaction.TxOut[idx].PkScript))).Int64(
				"block-height", blockHeight).Str(
				"block-hash", blockHash).Str(
				"tx-hash", transaction.TxHash().String()).Int(
				"tx-out-idx", idx).Msg("")
			continue
		}
		err = i.addInputTransaction(tx, transaction.TxHash().String(), decodedOutput.Addresses[0], idx)
		if err != nil {
			return err
		}

		err = i.addTransactionWithInput(tx, transaction.TxHash().String(),
			txIn.PreviousOutPoint.Hash.String(), txIn.PreviousOutPoint.Index)
		if err != nil {
			return err
		}
	}
	return
}

func (i JaxTransactionsIndex) connectRegularTx(tx pgx.Tx, transaction *wire.MsgTx, blockHash string, blockHeight int64) (err error) {
	for _, txIn := range transaction.TxIn {
		if txIn.PreviousOutPoint.Hash.String() == ec.EmptyHash {
			continue
		}
		isTxFound, addressID, err := i.checkIfTxPresent(tx, txIn.PreviousOutPoint.Hash.String(), txIn.PreviousOutPoint.Index)
		if err != nil {
			return err
		}
		if !isTxFound {
			continue
		}
		err = i.addOutputTransaction(tx, transaction.TxHash().String(), addressID)
		if err != nil {
			return err
		}

		err = i.addTransactionWithInput(tx, transaction.TxHash().String(),
			txIn.PreviousOutPoint.Hash.String(), txIn.PreviousOutPoint.Index)
		if err != nil {
			return err
		}
	}
	for idx, txOut := range transaction.TxOut {
		decodedOutput, err := txutils.DecodeScript(txOut.PkScript, settings.NetParams)
		if err != nil {
			return err
		}
		if len(decodedOutput.Addresses) == 0 {
			logger.Log.Err(errors.New("Invalid output PkScript "+hex.EncodeToString(txOut.PkScript))).Int64(
				"block-height", blockHeight).Str(
				"block-hash", blockHash).Str(
				"tx-hash", transaction.TxHash().String()).Int(
				"tx-out-idx", idx).Msg("")
			continue
		}
		err = i.addInputTransaction(tx, transaction.TxHash().String(), decodedOutput.Addresses[0], idx)
		if err != nil {
			return err
		}
	}
	return
}
