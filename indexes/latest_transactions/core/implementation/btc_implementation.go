package implementation

import (
	"github.com/jackc/pgx/v4"
	"gitlab.com/jaxnet/core/indexer/core"
	"gitlab.com/jaxnet/core/indexer/core/blocks_cache"
)

type BtcLatestTransactionsIndex struct {
	LatestTransactionsIndex
}

func (i BtcLatestTransactionsIndex) InitRPCClient() (*core.AbstractRPCClient, error) {
	return core.InitBTCRPCClient()
}

func (i BtcLatestTransactionsIndex) FetchNextBlockFromNode(offset int64, checkPrevBlock bool,
	rpcClient *core.AbstractRPCClient, cacheHandler *blocks_cache.CacheHandler) (
	block *core.AbstractMsgBlock, prevBlockNumber, blockHeight int64, rebuildingChainBlockNumber int64, err error) {
	return core.FetchNextBlockFromBtcNode(offset, checkPrevBlock, rpcClient, cacheHandler)
}

func (i BtcLatestTransactionsIndex) ConnectBlock(tx pgx.Tx, block *core.AbstractMsgBlock, blockHeight int64, rpcClient *core.AbstractRPCClient) error {
	btcBlock, err := block.BTCMsgBlock()
	if err != nil {
		return err
	}
	btcClient, err := rpcClient.BTCRPCClient()
	if err != nil {
		return err
	}
	for _, transaction := range btcBlock.Transactions {
		txHash := transaction.TxHash()
		var amount int64
		transactionDetails, err := btcClient.GetTxDetails(&txHash, true)
		if err != nil {
			return err
		}
		if transactionDetails.CoinbaseTx {
			amount = transactionDetails.OutAmount
		} else {
			amount = transactionDetails.InAmount
		}
		err = i.addTransactionToIndex(tx, transaction.TxHash().String(), blockHeight, amount)
		if err != nil {
			return err
		}
	}

	err = i.removeOutdatedTransactions(tx)
	if err != nil {
		return err
	}
	return nil
}

func (i BtcLatestTransactionsIndex) DisconnectBlock(tx pgx.Tx, block *core.AbstractMsgBlock, _ int64, _ *core.AbstractRPCClient) error {
	btcBlock, err := block.BTCMsgBlock()
	if err != nil {
		return err
	}
	for _, transaction := range btcBlock.Transactions {
		err := i.removeTransactionFromIndex(tx, transaction.TxHash().String())
		if err != nil {
			return err
		}
	}
	return nil
}
