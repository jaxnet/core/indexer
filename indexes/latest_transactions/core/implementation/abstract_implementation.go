package implementation

import (
	"github.com/fasthttp/router"
	"github.com/jackc/pgx/v4"
	"gitlab.com/jaxnet/core/indexer/core/database"
	"gitlab.com/jaxnet/core/indexer/core/settings"
	"gitlab.com/jaxnet/core/indexer/indexes/latest_transactions/core/interfaces"
	"strconv"
	"strings"
)

type LatestTransactionsIndex struct{}

func (i LatestTransactionsIndex) SetHttpHandlers(group *router.Group) {
	interfaces.InitAPIHandlers(group)
}

func (i LatestTransactionsIndex) DefaultFirstBlockForProcessing() (blockNumber int64) {
	return 0
}

func (i LatestTransactionsIndex) PrepareDatabase() error {
	return nil
}

func (i LatestTransactionsIndex) addTransactionToIndex(tx pgx.Tx, txHash string,
	blockHeight int64, amount int64) (err error) {
	query := "INSERT INTO hashes (hash) VALUES ( '{{ hash }}' ) ON CONFLICT DO NOTHING"
	query = strings.Replace(query, "{{ hash }}", txHash, 1)
	_, err = database.Exec(tx, query)
	if err != nil {
		return
	}

	query =
		"INSERT INTO {{ latest_transactions_table }} (block_num, hash, amount) VALUES(" +
			"{{ block_num }}, " +
			"(SELECT id AS hash FROM hashes WHERE hash = '{{ hash }}'), {{ amount }})"
	query = strings.Replace(query, "{{ latest_transactions_table }}", database.DataTableName(), 1)
	query = strings.Replace(query, "{{ block_num }}", strconv.FormatInt(blockHeight, 10), 1)
	query = strings.Replace(query, "{{ hash }}", txHash, 1)
	query = strings.Replace(query, "{{ amount }}", strconv.FormatInt(amount, 10), 1)
	_, err = database.Exec(tx, query)
	if err != nil {
		return
	}
	return
}

func (i LatestTransactionsIndex) removeTransactionFromIndex(tx pgx.Tx, txHash string) (err error) {
	query :=
		"DELETE FROM {{ latest_transactions_table }} WHERE hash = " +
			"(SELECT id AS hash FROM hashes WHERE hash = '{{ hash }}')"

	query = strings.Replace(query, "{{ latest_transactions_table }}", database.DataTableName(), 1)
	query = strings.Replace(query, "{{ hash }}", txHash, 1)
	_, err = database.Exec(tx, query)
	if err != nil {
		return
	}
	return
}

func (i LatestTransactionsIndex) removeOutdatedTransactions(tx pgx.Tx) (err error) {
	query := "DELETE FROM {{ latest_transactions_table }} WHERE id < " +
		"(SELECT id FROM {{ latest_transactions_table }} ORDER BY ID DESC OFFSET {{ transactions_limit }} LIMIT 1)"
	query = strings.Replace(query, "{{ latest_transactions_table }}", database.DataTableName(), 2)
	query = strings.Replace(query, "{{ transactions_limit }}",
		strconv.FormatUint(uint64(settings.Conf.CntRecordsSavedInDB), 10), 1)
	_, err = database.Exec(tx, query)
	if err != nil {
		return
	}
	return
}
