package interfaces

import (
	jsoniter "github.com/json-iterator/go"
	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/core/indexer/core/database"
	"gitlab.com/jaxnet/core/indexer/core/settings"
	"strconv"
	"strings"
)

func handleLatestTransactionsRequest(ctx *fasthttp.RequestCtx) {

	query := "SELECT h.hash, lt.block_num, lt.amount FROM {{ latest_transactions_table }} AS lt " +
		"LEFT JOIN hashes AS h ON lt.hash = h.id ORDER BY lt.id DESC LIMIT {{ transactions_count }}"
	query = strings.Replace(query, "{{ latest_transactions_table }}", database.DataTableName(), 1)
	query = strings.Replace(query, "{{ transactions_count }}",
		strconv.FormatUint(uint64(settings.Conf.CntRecordsInResponse), 10), 1)
	rows, err := database.QueryTxFree(query)
	defer rows.Close()
	if err != nil {
		reportError(err, ctx)
		return
	}

	var latestTransactions []LatestTransaction
	for rows.Next() {

		values, err := rows.Values()
		if err != nil {
			reportError(err, ctx)
			return
		}
		hash, ok := values[0].(string)
		if !ok {
			reportError(err, ctx)
			return
		}
		blockNum, ok := values[1].(int64)
		if !ok {
			reportError(err, ctx)
			return
		}
		amount, ok := values[2].(int64)
		if !ok {
			reportError(err, ctx)
			return
		}
		latestTransactions = append(latestTransactions, LatestTransaction{
			Hash:     hash,
			BlockNum: int(blockNum),
			Amount:   int(amount)})
	}

	err = writeLatestBlocks(ctx, latestTransactions)
	if err != nil {
		handleError(err, ctx)
		return
	}
}

func writeLatestBlocks(ctx *fasthttp.RequestCtx, latestBlocks []LatestTransaction) (err error) {
	data, err := jsoniter.Marshal(latestBlocks)
	if err != nil {
		return
	}

	_, err = ctx.Write(data)
	if err != nil {
		return
	}

	ctx.Response.Header.SetContentType("application/json")
	return
}
