package interfaces

type LatestTransaction struct {
	Hash     string `json:"hash"`
	BlockNum int    `json:"blockNumber"`
	Amount   int    `json:"amount"`
}
