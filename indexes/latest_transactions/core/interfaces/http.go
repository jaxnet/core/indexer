package interfaces

import (
	"fmt"
	"github.com/fasthttp/router"
	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/core/indexer/core/logger"
	"math/rand"
)

func InitAPIHandlers(root *router.Group) {
	group := root.Group("latest-transactions")
	group.GET("/", HandleLatestTransactions)
}

func HandleLatestTransactions(ctx *fasthttp.RequestCtx) {
	switch string(ctx.Method()) {
	case "GET":
		handleLatestTransactionsRequest(ctx)
		return

	default:
		ctx.SetStatusCode(fasthttp.StatusMethodNotAllowed)
		return
	}
}

// reportError is used for the cases when error can be shown to the caller.
func reportError(err error, ctx *fasthttp.RequestCtx) {
	errorID := rand.Int31()

	logger.Err(err).Int32(
		"error-id", errorID).Msg(
		"error occurred during processing http request")

	ctx.Error(
		fmt.Sprint(err.Error(), " (id=", errorID, ")"),
		fasthttp.StatusInternalServerError)
}

// handleError is used in the cases when error must be logged but no details must be shared with user.
func handleError(err error, ctx *fasthttp.RequestCtx) {
	errorID := rand.Int31()

	logger.Err(err).Int32(
		"error-id", errorID).Msg(
		"error occurred during processing http request")

	ctx.Error(
		fmt.Sprint("Unexpected error occurred (id=", errorID, ")"),
		fasthttp.StatusInternalServerError)
}
