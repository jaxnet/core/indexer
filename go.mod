module gitlab.com/jaxnet/core/indexer

go 1.16

require (
	github.com/ClickHouse/clickhouse-go v1.5.3
	github.com/btcsuite/btcd v0.22.0-beta
	github.com/fasthttp/router v1.3.10
	github.com/jackc/pgconn v1.8.0
	github.com/jackc/pgx/v4 v4.10.1
	github.com/json-iterator/go v1.1.12
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.11.0
	github.com/rs/zerolog v1.25.0
	github.com/valyala/fasthttp v1.22.0
	gitlab.com/jaxnet/jaxnetd v0.4.5
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)

replace github.com/btcsuite/btcd v0.22.0-beta => gitlab.com/jaxnet/btcd v0.22.7
